# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #
import argparse
import json
import logging
import sys
import time
from functools import partial
import queue
from multiprocessing import Process, Queue
from threading import Timer, Thread, Semaphore

import pika
from influxdb import InfluxDBClient

from optisim.utils import ConfigurationFitter, ResultsHandler, setup_logger
from optisim.utils.rabbitmq.rabbit_sim_producer import RabbitSimulationProducer

str_formatter = '%(asctime)-15s::%(levelname)s::%(process)d::%(name)s::%(funcName)s::%(message)s'

# --------------------------------------------------------------------------- #
# Constants
# --------------------------------------------------------------------------- #
WORK_QUEUE_NAME = 'output_results'
OUTPUT_EXCHANGE = 'output_exchange'
WATCHDOG_TIMEOUT = 5.0
HEARTBEAT_PERIOD = 1.0
FLUSH_TIMEOUT = 10.0
RABBIT_HEARTBEAT = 10


# --------------------------------------------------------------------------- #
# Functions
# --------------------------------------------------------------------------- #
class ChronQ:
    def __init__(self, ids_to_watch, timeout, heartbeat_queue, respawn_queue, logger):
        self.respawn_queue = respawn_queue
        self.heartbeat_queue = heartbeat_queue
        self.timeout = timeout
        self.logger = logger
        self.timers = {i: Timer(self.timeout, self.flag, args=[i, ]) for i in ids_to_watch}

    def _start(self):
        for t in self.timers.values():
            t.daemon = True
            t.start()

    def flag(self, nid):
        self.logger.warning('worker {} timed out'.format(nid))
        self.respawn_queue.put(nid)

    def reset_timer(self, nid):
        self.timers[nid].cancel()
        self.timers[nid] = Timer(self.timeout, self.flag, args=[nid, ])
        self.timers[nid].daemon = True
        self.timers[nid].start()

    def _watch(self):
        self._start()
        while True:
            niddi = self.heartbeat_queue.get()
            self.logger.debug('received heartbeat from {}'.format(niddi))
            self.reset_timer(niddi)

    def watch(self):
        wt = Thread(target=self._watch)
        wt.daemon = True
        wt.start()


class WorkerSpawner:
    def __init__(self, ids, timeout, logger, worker_params: list):
        self.logger = logger
        self.data_queues = [Queue() for i in range(int((len(ids)-1)/2))]
        self.spawn_queue = Queue()
        self.hb_queue = Queue()
        self.ids = ids
        self.targets_queues = list(zip(
            [run_influx_uploader] * int((len(ids) - 1) / 2) + [run_worker] * int((len(ids) - 1) / 2) + [run_supervisor],
            self.data_queues + self.data_queues + [None]))
        config, topic, idb_client, log_file = worker_params
        self.spawn_fn = partial(self.spawn_worker, config, topic, idb_client, self.hb_queue, log_file)
        self.cq = ChronQ(ids, timeout, self.hb_queue, self.spawn_queue, logger)
        self.ck_thread = Thread(target=self.check_and_respawn)
        self.ck_thread.daemon = True

    @staticmethod
    def spawn_worker(config, topic, idb_client, heartbeat_queue, log_file, id, target_queue):
        target = target_queue[0]
        if target == run_worker:
            data_queue = target_queue[1]
            w = Process(target=target, args=(config, topic, id, data_queue, heartbeat_queue, log_file))
        elif target == run_influx_uploader:
            data_queue = target_queue[1]
            w = Process(target=target, args=(config, id, idb_client, data_queue, heartbeat_queue, log_file))
        elif target == run_supervisor:
            w = Process(target=target, args=(config, id, heartbeat_queue, log_file))
        else:
            raise
        return w

    def spawn_id(self, wid):
        w = self.spawn_fn(wid, self.targets_queues[wid-1])
        w.daemon = True
        w.start()

    def run(self):
        self.logger.info('Spawning the workers (ids: {})'.format(self.ids))
        for wid in self.ids:
            self.spawn_id(wid)

        time.sleep(10)
        self.logger.info('Initializing watchdog timer checker...')
        self.cq.watch()
        self.logger.info('Initializing respawning thread...')
        self.ck_thread.start()
        # monitor the status of the queues
        while True:
            time.sleep(60)
            self.logger.info('Queues length:' + ','.join(
                [" queue {}: {}".format(i + 1, q.qsize()) for i, q in enumerate(self.data_queues)]))

    def check_and_respawn(self):
        while True:
            respawn_id = self.spawn_queue.get()
            self.logger.warning('Worker #{} heartbeat stopped. Respawning...'.format(respawn_id))
            self.spawn_id(respawn_id)
            self.logger.warning('Worker #{} respawned.'.format(respawn_id))


def run_supervisor(config, worker_id, heartbeat_queue, log_file_name):
    """ Function that runs a producer instance (for ping)
    :param config: configuration dictionary
    :type dict
    :param g: messages queue
    :type Queue
    :param log_file_name: logger file
    :type string
    """

    def heartbeat():
        while True:
            heartbeat_queue.put(worker_id)
            time.sleep(HEARTBEAT_PERIOD)
    heartbeat_thread = Thread(target=heartbeat)
    heartbeat_thread.daemon = True
    heartbeat_thread.start()

    # rabbit_logger = setup_logger(name='rabbit', filename=log_file_name, level=logging.INFO, entry_format=str_formatter)
    logger = setup_logger(name='output_data_manager', filename=log_file_name, level=logging.INFO,
                          entry_format=str_formatter)
    logger.info('Spawned supervisor, id {}'.format(worker_id))
    producer = RabbitSimulationProducer(host=config['rabbit']['host'],
                                        port=config['rabbit']['port'],
                                        user=config['rabbit']['user'],
                                        password=config['rabbit']['password'],
                                        virtual_host=config['rabbit']['virtual_host'],
                                        logger=rabbit_logger)

    ping_queue = next((l for l in config['input_output'] if l['type'] == 'odmping'), None)['topic']
    pong_queue = next((l for l in config['input_output'] if l['type'] == 'odmpong'), None)['topic']

    def on_message(_ch, _method, _properties, body):
        recv_data = json.loads(body.decode('utf-8'))
        assert recv_data['case'] == 'ctrl'
        assert recv_data['type'] == 'ping'
        rabbit_logger.info('Send pong command')
        producer.send_dict(OUTPUT_EXCHANGE,
                           pong_queue,  # the supervisor answers on the pong queue
                           dict(payload_dict={"msg": 'ok'},
                                simulation_dblhour=0,
                                sync=True),
                           case='ctrl')
        _ch.basic_ack(delivery_tag=_method.delivery_tag)

    cred = pika.PlainCredentials(username=config['rabbit']['user'], password=config['rabbit']['password'])
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=config['rabbit']['host'],
                                  port=config['rabbit']['port'],
                                  credentials=cred,
                                  virtual_host=config['rabbit']['virtual_host'],
                                  heartbeat=RABBIT_HEARTBEAT))
    channel = connection.channel()
    etype = 'direct'
    channel.exchange_declare(exchange=OUTPUT_EXCHANGE, exchange_type=etype, auto_delete=False)
    rabbit_logger.debug('Output data manager PONGER declared exchange {} ({})'.format(ping_queue, etype))

    _result = channel.queue_declare(ping_queue, exclusive=False)
    channel.queue_bind(exchange=OUTPUT_EXCHANGE, queue=ping_queue)
    rabbit_logger.debug('Output data manager PONGER bound to queue {}'.format(ping_queue))

    channel.basic_consume(
        queue=ping_queue, on_message_callback=on_message, auto_ack=False)
    try:
        channel.start_consuming()
    except:
        channel.stop_consuming()


def run_worker(config, topic, worker_id, data_queue, heartbeat_queue, log_file_name):
    """ Function that runs a consumer instance
    :param config: configuration dictionary
    :type dict
    :param topic: name of the topic
    :type str
    :param worker_id: identifier
    :type int
    :param idb_client: InfluxDB client
    :type InfluxDBClient
    :param g: messages queue
    :type Queue
    :param log_file_name: logger file
    :type string
    """

    # starting heartbeat
    def heartbeat():
        while True:
            heartbeat_queue.put(worker_id)
            time.sleep(HEARTBEAT_PERIOD)
    heartbeat_thread = Thread(target=heartbeat)
    heartbeat_thread.daemon = True
    heartbeat_thread.start()

    # create the consumer
    # rabbit_logger = setup_logger(name='rabbit', filename=log_file_name, level=logging.INFO, entry_format=str_formatter)
    logger = setup_logger(name='output_data_manager', filename=log_file_name, level=logging.INFO, entry_format=str_formatter)
    logger.info('Spawned worker, id {}'.format(worker_id))
    error_logger = setup_logger(name='output_data_manager_error',
                                filename=log_file_name + '_error' if type(log_file_name) == 'str' else None, level=logging.INFO,
                                entry_format=str_formatter)

    # setup connection
    cred = pika.PlainCredentials(username=config['rabbit']['user'], password=config['rabbit']['password'])
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=config['rabbit']['host'],
                                  port=config['rabbit']['port'],
                                  credentials=cred,
                                  virtual_host=config['rabbit']['virtual_host'],
                                  heartbeat=RABBIT_HEARTBEAT))
    channel = connection.channel()
    etype = 'direct'
    channel.exchange_declare(exchange=OUTPUT_EXCHANGE, exchange_type=etype)
    logger.debug('Output data manager worker #{} declared exchange {} ({})'.format(worker_id, topic, etype))

    _result = channel.queue_declare(WORK_QUEUE_NAME, exclusive=False)
    channel.queue_bind(exchange=OUTPUT_EXCHANGE, queue=WORK_QUEUE_NAME)
    logger.debug('Output data manager worker #{} bound to queue {}'.format(worker_id, WORK_QUEUE_NAME))

    # logger.info('continuous consumer process configured')

    # callback on message
    def on_message(_ch, _method, _properties, body):
        # decode data received by broker
        recv_data = json.loads(body.decode('utf-8'))
        data_queue.put(recv_data)
        _ch.basic_ack(delivery_tag=_method.delivery_tag)
        while data_queue.qsize() > config['influxdbResults']['maxDataInQueue']:
            time.sleep(0.01)
    if 'prefetchCount' in config['influxdbResults'].keys():
        channel.basic_qos(prefetch_count=config['influxdbResults']['prefetchCount'])
    channel.basic_consume(queue=WORK_QUEUE_NAME, on_message_callback=on_message, auto_ack=False)
    try:
        channel.start_consuming()
    except:
        channel.stop_consuming()


def run_influx_uploader(config, worker_id, idb_client, data_queue, heartbeat_queue, log_file_name):
    """ Function that runs a consumer instance
    :param config: configuration dictionary
    :type dict
    :param topic: name of the topic
    :type str
    :param worker_id: identifier
    :type int
    :param idb_client: InfluxDB client
    :type InfluxDBClient
    :param g: messages queue
    :type Queue
    :param log_file_name: logger file
    :type string
    """

    # starting heartbeat
    def heartbeat():
        while True:
            heartbeat_queue.put(worker_id)
            time.sleep(HEARTBEAT_PERIOD)
    heartbeat_thread = Thread(target=heartbeat)
    heartbeat_thread.daemon = True
    heartbeat_thread.start()

    logger = setup_logger(name='output_data_manager', filename=log_file_name, level=logging.INFO,
                          entry_format=str_formatter)
    logger.info('Spawned influx_uploader, id {}'.format(worker_id))
    error_logger = setup_logger(name='output_data_manager_error',
                                filename=log_file_name + '_error' if type(log_file_name) == 'str' else None,
                                level=logging.INFO,
                                entry_format=str_formatter)

    # define the ResultHandler object
    rh = ResultsHandler(config=config, influx_client=idb_client, logger=logger, id=worker_id)
    used_measurements = dict()
    data_points = []

    def flush(data, logger):
        logger.info('Autoflush triggered for worker #{}'.format(worker_id))
        try:
            return rh.send_data_to_influxdb(points_to_send=data.copy(), max_data_length=1)
        except Exception as e:
            error_logger.error(e)
            return data

    while True:
        try:
            recv_data = data_queue.get(timeout=FLUSH_TIMEOUT)
            if recv_data is not []:
                try:
                    _data_points = data_points.copy()
                    if recv_data is not None and 'case' in recv_data:

                        # forecast data
                        if recv_data['case'] == 'forecast':
                            used_measurements[recv_data['measurement']] = True

                            # insert models values
                            tags = dict(meter_id=str(recv_data['meter_id']), ag_name=str(recv_data['name']))
                            _data_points = rh.insert_forecast_values(str_data=recv_data['data'],
                                                                                   current_points=_data_points,
                                                                                   measurement=recv_data['measurement'],
                                                                                   tags=tags,
                                                                                   used_measurements=used_measurements)

                            # agent internal data
                        elif recv_data['case'] == 'internal':
                            used_measurements[recv_data['measurement']] = True

                            # # insert models values
                            tags = dict(meter_id=str(recv_data['meter_id']), ag_name=str(recv_data['name']))
                            _data_points = rh.insert_agent_internal_values(str_data=recv_data['data'],
                                                                           current_points=_data_points,
                                                                           measurement=recv_data['measurement'],
                                                                           tags=tags,
                                                                           used_measurements=used_measurements)

                        # model data
                        elif recv_data['case'] == 'model':
                            used_measurements[recv_data['measurement']] = True

                            # insert models values
                            tags = dict(meter_id=str(recv_data['meter_id']), bus_id='bus_%s' % str(recv_data['bus_id']),
                                        component_name=str(recv_data['name']), component_type=str(recv_data['type']))
                            _data_points = rh.insert_models_values(str_data=recv_data['data'],
                                                                                 current_points=_data_points,
                                                                                 measurement=recv_data['measurement'],
                                                                                 tags=tags,
                                                                                 used_measurements=used_measurements)

                        # simulation data
                        elif recv_data['case'] == 'simulation':
                            used_measurements[recv_data['measurement']] = True

                            # insert simulation values
                            _data_points = rh.insert_simulation_values(data=recv_data['payload'],
                                                                                 current_points=_data_points,
                                                                                 measurement=recv_data['measurement'],
                                                                                 timestamp=int(
                                                                                     recv_data['unix_simulation_time']),
                                                                                 index_dict=recv_data[
                                                                                     'indicization_dict'])

                        # disaggregation data
                        elif recv_data['case'] == 'algo_disaggregation':
                            used_measurements[recv_data['measurement']] = True

                            # insert disaggregation values
                            _data_points = rh.insert_disaggregation_values(data=recv_data,
                                                                                     current_points=_data_points,
                                                                                     measurement=recv_data[
                                                                                         'measurement'])

                        # meter_net_power data
                        elif recv_data['case'] == 'algo_meter_net_power':
                            used_measurements[recv_data['measurement']] = True

                            # insert meter_net_power values
                            _data_points = rh.insert_meter_net_power_values(data=recv_data,
                                                                                      current_points=_data_points,
                                                                                      measurement=recv_data[
                                                                                          'measurement'])

                        # meter_pv_profile data
                        elif recv_data['case'] == 'algo_meter_pv_profile':
                            used_measurements[recv_data['measurement']] = True

                            # insert meter_pv_profile values
                            _data_points = rh.insert_meter_pv_profile_values(data=recv_data,
                                                                                       current_points=_data_points,
                                                                                       measurement=recv_data[
                                                                                           'measurement'])
                        else:
                            logger.warning('Case \'%s\' not available' % recv_data['case'])

                    else:
                        logger.warning('Message not considered (message None or \'case\' key not available')

                    # upload to influx
                    _data_points = rh.send_data_to_influxdb(points_to_send=_data_points,
                                                            max_data_length=config['influxdb']['maxLinesPerInsert'])
                    data_points = _data_points
                except Exception as e:
                    logger.error('Failed to process message %s\n%s' % (recv_data, str(e)))
                    error_logger.error('Failed to process message %s\n%s' % (recv_data, str(e)))

        except queue.Empty:
            data_points = flush(data_points, logger)


# --------------------------------------------------------------------------- #
# Main
# --------------------------------------------------------------------------- #
if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c', help='connection configuration file')
    arg_parser.add_argument('-l', help='log file')

    # --------------------------------------------------------------------------- #
    # Set configuration
    # --------------------------------------------------------------------------- #
    args = arg_parser.parse_args()
    cf = ConfigurationFitter(config_file=args.c)
    config = cf.fitted_config

    OUTPUT_EXCHANGE = config['rabbit']['output_exchange_name']
    WORK_QUEUE_NAME = [c['topic'] for c in config['input_output'] if 'topic' in c.keys() and c['name']=='models_2_db'][0]
    # --------------------------------------------------------------------------- #
    # Set logging object
    # --------------------------------------------------------------------------- #
    if not args.l:
        log_file = None
    else:
        log_file = args.l

    logger = setup_logger(name='output_data_manager_main', filename=log_file, level=logging.INFO, entry_format=str_formatter)

    # --------------------------------------------------------------------------- #
    # InfluxDB connection
    # --------------------------------------------------------------------------- #
    try:
        # check if a specific InfluxDB has to be used to save the datasets
        if 'influxdbResults' in config.keys():
            cfg_influx = config['influxdbResults']
        else:
            cfg_influx = config['influxdb']

        logger.info('Connection to InfluxDB server on [%s:%i]...' % (cfg_influx['host'], cfg_influx['port']))
        idb_client = InfluxDBClient(host=cfg_influx['host'], port=int(cfg_influx['port']), username=cfg_influx['user'],
                                    password=cfg_influx['password'], database=cfg_influx['dbname'])
    except Exception as e:
        logger.error('EXCEPTION: %s' % str(e))
        sys.exit(-1)
    logger.info('Connection estabilished')

    # --------------------------------------------------------------------------- #
    # Starting program
    # --------------------------------------------------------------------------- #
    # define the topic
    topic = None
    # todo what happens if the first external_to_db has a 'topic' field different from the possibly present other
    # external_to_db outputs?
    for io in config['input_output']:
        if io['type'] == 'external_to_db':
            topic = io['topic']
            break

    #  check if a topic is available
    if topic is None:
        logger.error('No topic available, exit program')
        sys.exit(-2)


    # create the consumers workers and 1 for the supervisor
    wids = [i+1 for i in range(1+2*int(config['rabbit']['consumers']))]

    # spawn workers and supervisor
    rabbit_logger = setup_logger(name='rabbit', filename=log_file, level=logging.INFO, entry_format=str_formatter)
    ws = WorkerSpawner(wids, WATCHDOG_TIMEOUT, rabbit_logger, [config, topic, idb_client, log_file])
    ws.run()
