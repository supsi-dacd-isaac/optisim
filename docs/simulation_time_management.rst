.. _simulation-time-management:

Simulation time management
===================================

*Optisim* manages the temporal aspects of a simulation taking into account the following three parameters,
configurable in the :code:`simulation` section of main configuration file (see section :ref:`configuration-file-structure` for details):

* :code:`startDateTime`: it indicates the initial timestamp of the simulation in format :code:`YYYY-mm-DD HH:MM`.
* :code:`endDateTime`: it specifies the final timestamp of the simulation in format :code:`YYYY-mm-DD HH:MM`.
* :code:`step`: it defines the duration (seconds) of each simulation step (e.g. :code:`step=300`, then there is a step every 5 minutes).

It is important to take care that, before launching a simulation, all the required input data sets are already stored in the InfluxDB
database (see sections :ref:`external-interfaces` and :ref:`inputs-data` for details) for the interval :code:`[startDateTime-startDateTime]`.

Regarding the simulation time, it is complicated to be able to give reasonable answer. Under a general point of view,
the time strongly depends on the following aspects:

* The number of meters.
* The maximum number of components grouped by meters.
* The type of the simulated components, i.e. some components are described by models much more complicated than others.
* The hardware resources of the machine where *Optisim* runs.
