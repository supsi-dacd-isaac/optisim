.. _external-interfaces:

External interfaces
===================================

*Optisim* requires the following external services to operate correctly:

* `InfluxDB <https://www.influxdata.com/get-influxdb>`_
* `Rabbit MQ <https://www.rabbitmq.com>`_
* `OutputDataManager <https://gitlab.com/supsi-dacd-isaac/optisim/-/blob/master/output_data_manager.py>`_

The services have to be installed locally or in remote hosts from the machine where Optisim runs.
The following picture shows the interactions among the applications. Note how OutputDataManager does not interact effectively with Optisim.
However, this section describes it as an important part of the simulator, which interacts both with RabbitMQ and InfluxDB.

.. figure:: images/external_interfaces_schema.svg
  :width: 600
  :align: center

  Interaction of *Optisim* with external applications

InfluxDB
**************

InfluxDB is a powerful time-series database able to store and maintain significant amounts of data sets.
As regards *Optisim*, it uses the InfluxDB service to download all the data required by the simulation (e.g. the profile of the external temperature).
The data downloading, shown by the orange arrow in figure above, is actuated by *Optisim* at the beginning of the simulation,
to load all the needed data before starting the simulation.

RabbitMQ
**************

RabbitMQ is a message broking system, basically used to exchange data between different applications.
*Optisim* uses it for two aims. The former, already described in subsection :ref:`loop_with_external_controller`, is to utilize
the broker system to send, at every step, the simulation state to an external controller and then to receive from it the related commands
to actuate in the next step. Regarding the latter, Optisim uses RabbitMQ to send the simulation results configured to be stored in InfluxDB.
The effective saving in the database is performed by *OutputDataManager*, as described
in the following subsection.

*OutputDataManager*
**************************************

Unlike InfluxDB and RabbitMQ, *OutputDataManager* is a part of *Optisim* project. It acts with parallel and independent processes,
one for each CPU, and is demanded to save the simulation results as shown in the figure above.
The usage of this external script significantly improves the simulation velocity, leaving *Optisim* to
run it without having to spend time to save data in InfluxDB.

The following BASH commands show how to launch *OutputDataManager* installed in a generic :code:`$ROOT/optisim` folder
(see section :ref:`installation-procedure` for details).

.. code::

    cd $ROOT
    source optisim/venv/bin/activate
    (venv) python optisim/output_data_manager.py -c $CONNECTIONS_CONFIGURATION_FILE

Section (:ref:`configuration-file-structure`) describes the main parts of :code:`$CONNECTIONS_CONFIGURATION_FILE` file.

