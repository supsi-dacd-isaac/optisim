.. Optisim documentation master file, created by
   sphinx-quickstart on Wed Aug 18 16:25:43 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Optisim's documentation!
===================================

Optisim is a software able to simulate of the energy consumption and production of a group of households.

.. toctree::
    :maxdepth: 2

    general_overview
    installation
    configuration_file_structure
    simulation_time_management
    internal_structure
    external_interfaces
    inputs
    outputs
    components_models



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
