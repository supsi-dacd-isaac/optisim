.. _internal-structure:

Internal Structure
===================================

The internal structure of *Optisim* is based on a significant usage of `parallelism <https://en.wikipedia.org/wiki/Parallel_computing>`_
in order to maximize the velocity of the simulation.
As explained in :ref:`general_overview`, *Optisim* has three operation levels (in the following called *MeterManager*, *Meter* and *Component* levels),
which corresponds to the tree structure shown in the figure below.

.. figure:: images/tree_meters_components.svg
  :width: 600
  :align: center

  Tree structure of *Optisim*


The first two levels are run in parallel. First, a unique process corresponds to each meter. In addition, another process
manages the *MeterManager* level. The MeterManager process activates the meters for any simulation step,
which executes the related components' models sequentially.
Once all the meters processes end to simulate the components, the power flow among the meters is performed via Krangpower (if needed) at
the *MeterManager* level. Then, the returned data obtained by both models and the power flow are collected by *MeterManager*
to be (eventually) sent to a controller.
The communication between meters and MeterManager processes is managed by a set of queues used by MeterManager to activate
the meters and return the results, as reported in the following figures.

.. figure:: images/schema_optisim_no_controller.svg
  :width: 400
  :align: center

  Processes and queues interactions in *Optisim* when no control is actuated

The activation of the meters is performed at any simulation step by *MeterManager* writing the proper commands on the :code:`q_forceoff[.]` queues,
as shown in the figure above. Each meter has its own queue, and thanks to it, it receives the commands to actuate in the controllable components
in the activation message. In addition, each meter returns the simulated data using :code:`q_out` queue.
It is important to remark there is only one queue to gather data from all the N meters.
Thus, when N messages finally arrive on the queue, *MeterManager* can launch the power flow simulation, if needed,
and then return the data received to the eventual controller.

Regarding the controlling algorithms of the components, currently the following three approaches are available in *Optisim*:

* Loop without control
* Loop with external controller
* Loop with internal controller

Loop without control
*******************************

This approach, basically figured in the last figure, simulates an open-loop system where no control is operated on the components.
To run a simulation step, *MeterManager* sends 0 commands on :code:`q_forceoff[.]` for all the controllable components.
In *Optisim* convention, a 0 received on a :code:`q_forceoff[M]` queue related to a generic component C of meter M means that no control
is actuated on C. Instead, a 1 command means that the component has to be switched off.

.. _loop_with_external_controller:

Loop with external controller
*******************************

In this configuration, an external program (in the following called CTRL) is required to run the controllable components' algorithms.
The following figure shows the interactions between *CTRL* and *Optisim*.

.. figure:: images/schema_optisim_external_controller.svg
  :width: 600
  :align: center

  Processes and queues interactions in *Optisim* when an external controller operates

The data exchange between the two applications is managed via a `RabbitMQ <https://www.rabbitmq.com>`_ instance. The data returned to *MeterManager* are sent
on the :code:`q_forceoff[.]` queues.

Section :ref:`external-interfaces` describes in detail how data are transferred using the broker.

Loop with internal controller
*******************************

In order to maximize the simulation of the controlled simulations, Optisim is able to run internal controller algorithms,
currently based on pre-defined commands profiles, avoiding using external controllers. The following picture shows this approach.

.. figure:: images/schema_optisim_internal_controller.svg
  :width: 600
  :align: center

  Processes and queues interactions in *Optisim* when the internal controller operate

The internal controller works in parallel mode using queues, similarly to the aforementioned meters simulations.
Basically, a controller process corresponds to each meter. When all the components data are received in :code:`q_out` (light blue arrow),
then the controller processes are activated by *MeterManager* using the :code:`q_controller[.]` queues (green arrow).
Once the algorithm has run the controls of the components belonging to the related meters, the commands are sent on the
:code:`q_forceoff[.]` queues (violet and orange arrows).

Currently, the power flow simulation is still to be developed in the case of the internal controller.



