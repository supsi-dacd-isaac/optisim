.. _configuration-file-structure:

Configuration file structure
===================================

*Optisim* requires an configuration file to operate correctly, which is passed to the program as an input arguments
(see section :ref:`installation-procedure` for details).

The input file is in JSON format and contains all the configurable parameters managing the simulation aspects
that will be described in the following sections. It is important to consider how these files can have a huge dimension,
depending on the quantity of meters and components involved in the simulation. In order to give a general idea about
how this file is constituted, the most important sections if the files are reported in the following list:

* :code:`simulation`: containing the general parameters of the simulation, among them:

    * Simulation identifier (:code:`ID`)
    * Start/end date of the simulation and its time resolution
    * Flags about the usage of internal/external controllers and Krangpower (see section :ref:`internal-structure` for details)
    * Path of the file for the connections to the external services. The main strucuture of this file will be explained in the following in a dedicated subsection.

* :code:`location`: containing the information of the location related to the simulation

* :code:`meteo`: containing parameters about the meteorological data sets required by the simulation (see sections :ref:`external-interfaces` and :ref:`inputs-data` for details)

* :code:`krang`: containing parameters about the Krangpower usage (e.g. the circuit file describing the grid, the signals to send to the eventual external controller, etc.)

* :code:`meters`: containing all the parameters related to the meters and their components. This section is by far the most complicated and structured; thus, in the following a dedicated subsection will describe it in details.


.. _conns_file:

Connections file
************************

This file contains the parameters required by *Optisim* to interact with external services, which are InfluxDB servers and RabbiMQ broker
(see sections :ref:`external-interfaces`, :ref:`inputs-data` and :ref:`outputs-data` for details). The main sections are reported in the following list:

* :code:`influxDB`: containing the connection parameters to the InfluxDB server used for the input data.
* :code:`influxDBResults`: containing the connection parameters to the InfluxDB server used for the storage of the simulation results.
* :code:`rabbit`: containing the connection parameters to the RabbitMQ message broker.
* :code:`input_output`: containing a list of all the data exchanging between InfluxDB, RabbitMQ and *Optisim*, below the settings of each element are explained:

    * :code:`name`: the data exchanging identifier
    * :code:`type`: the data exchanging type (:code:`external_to_db | external_from_db | internal`)
    * :code:`topic`: the RabbitMQ topic to use
    * :code:`exchange`: the RabbitMQ exchange to use
    * :code:`measurement`: the prefix of the InfluxDB measurement used to store the simulations results (the final name is calculated by *Optisim* doing :code:`concat(measurement; '_'; simulation.ID)`)


Meters section
************************

The :code:`meters` section is constituted by a list of element, each of them contains all the settings related to a specified meter,
among them the parameters of the related components. Bascially, the main elements composing a meter's configuration are three:
:code:`name`, the meter's name, :code:`bus`, the identifier used by Krangpower in the power flow simulation and :code:`components`,
a list of the meter's components with the related settings.

The following list reports the parameters of a generic component (for details please refer to section :ref:`components-models`):

* :code:`name`: the name/identifier
* :code:`type`: the component type (e.g. uncontrolled, heat pump, PV, etc.)
* :code:`fields`: data needed by the component to simulate correctly, which have to be downloaded from InfluxDB database (see section :ref:`external-interfaces` for details)
* :code:`phases`: electrical phases taken into account in some components like the uncontrolled loads (:code`a | b | c`)
* :code:`results`: a list of data exchanges (see :ref:`conns_file` for details) used by *Optisim* to transfer data externally (:code:`output_name`), name of the signals to transfer (:code:`selection`) and how long the saving has to be performed (:code:`send_interval`).
* :code:`pars`: configurable parameters of the component model (e.g. :code`v_boiler` is the capacity of a boiler).

The following reports an example of the :code:`results` section, with the two elements. The former is used to save
on InfluxDB database (:code:`models_2_db` case) the signals starting with :code:`p` and :code:`p` evey 10 steps.
Instead the latter is used to send signals starting with :code:`p` to an external controller (:code:`2_algo` case) evey 5 steps.

.. code::

    "results": [
        {
          "output_name": "models_2_db",
          "selection": [
            "^p",
            "^q"
          ],
          "send_interval": 10
        },
        {
          "output_name": "2_algo",
          "selection": [
            "^p",
          ],
          "send_interval": 5
        }
    ],

As regards the :code:`components` structure, it is important to note that they can have a nested structure for what concerns the components
related to the heating plants (eg. building, heat pump, boiler, etc), as it will be explained in details in section :code:`components-models`.
Consequently, the JSON file has to take into account this aspect having the nested sections.
The structure :code:`building->floor_heating->boiler->HP+DHW` is an exhaustive example of the aforementioned nesting.
The :code:`building` contains a :code:`heating_element` section of type :code:`floor_heating` and so on until :code:`DHW` component as
explained in the following example. Each :code:`heating_element` and :code:`dhw` section contains all the aforementioned configurations
(e.g. :code:`fields`, :code:`type`, etc.) about the related component.

.. code::

    {
      .....
      "type": "building",
      .....
      "heating_element": {
        .....
        "type": "floor_heating",
        "heating_element": {
          ....
          "type": "boiler",
          "heating_element": {
            ....
            "type": "heat_pump",
            "dhw": {
              .....
            },

          },
        },
      },
    }


