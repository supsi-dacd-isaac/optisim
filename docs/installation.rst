.. _installation-procedure:

Installation and running
===================================


Currently, *Optisim* was successfully installed and tested on Ubuntu>=18.04 operating system using Python 3.7 as interpreter.
It is supposed to run also with higher Python 3 versions, but no tests were performed at the moment.

It is **strictly** recommended to create a Python virtual environment.

The following BASH commands, tested on Ubuntu 18.04.5 and 20.04.2 machines, show how to install *Optisim* in a generic $ROOT folder.

.. code::

    sudo apt-get update -y
    sudo apt-get install -y gcc g++ python3.7 python3.7-venv python3.7-dev python3-wheel libgomp1 llvm-10 libjemalloc-dev
    sudo ln -s /usr/bin/llvm-config-10 /usr/bin/llvm-config
    cd $ROOT
    git clone https://gitlab.com/supsi-dacd-isaac/optisim.git
    git clone https://gitlab.com/supsi-dacd-isaac/krangpower.git
    export PYTHONPATH=$PYTHONPATH:$ROOT/optisim:$ROOT/krangpower
    export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so
    cd optisim
    python3 -m venv venv
    source venv/bin/activate
    (venv) pip install -r optisim/requirements.txt
    (venv) pip install -r krangpower/requirements.txt


In addition, :code:`PYTHONPATH` and :code:`LD_PRELOAD` environment variables must be set as explained above in :code:`~/.bashrc` file.

Now we are ready to launch an *Optisim* simulation, typing the following commands:

.. code::

    (venv) cd $ROOT
    (venv) python optisim/sim_grid.py -c $CONFIGURATION_FILE

:code:`sim_grid.py` is the script initializing and starting the simulation, :code:`$CONFIGURATION_FILE` is the unique input
arguments required; it basically contains the configurable parameters of the simulations.
The next section (:ref:`configuration-file-structure`) describes the main parts of :code:`$CONFIGURATION_FILE` file.
