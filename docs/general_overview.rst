.. _general_overview:

General overview
===================================

*Optisim* the energy consumption and production of a group of PODs. Each **POD** in an *Optisim* simulation is constituted by a **meter**,
and a set of **components**, which are responsible for the consumption (e.g. heat pump, electrical heaters, uncontrolled loads),
the production (e.g. PV plant) and the local storage (e.g. battery, EV) related to the POD.
Regarding the power flow, performed at meter level, *Optisim* simulates it using the `Krangpower <https://gitlab.com/supsi-dacd-isaac/krangpower>`_ library.

Basically, a simulation acts as described in the following structure:

.. code::

    INITIALIZATION:
        * Input data are loaded from an InfluxDB database
        * The processes needed (i.e. each meter corresponds to a unique process) to run the simulation are created
        * The Krangpower circuit is properly instantiated

    SIMULATION MAIN CYCLE:
        * For each meter process (MP) ->
            ** MP reads on its queue to see the commands to actuate in the components belonging to the related meter.
            ** MP simulates the models of the components for the current time step
            ** MP writes on the output queues the results
        * Once all the MPs have finished to run the time step ->
            ** The MPs needed fort the powerflow are collected and sent to Krangpower
            ** Krangpower performs the powerflow step
            ** The data returned by Krangpower are sent on the output queue

*Optisim* is able to operate both in open-loop and in closed-loop mode, in this case using an internal controller algorithm
or an external application as reported in the following pictures.

.. figure:: images/optisim_generic_schema_extctrl.svg
  :width: 400
  :align: center

  *Optisim* interacting with an external controller


.. figure:: images/optisim_generic_schema_intctrl.svg
  :width: 400
  :align: center

  *Optisim* operating with its internal controller

The images and schema above show how *Optisim* works from a high-level point of view. In order to described exhaustively
how *Optisim* really operates, the present documentation is constituted by the following sections:

* Section :ref:`installation-procedure` shows how to install *Optisim*.
* Section :ref:`configuration-file-structure` describes the configuration file required by *Optisim* to run.
* Section :ref:`simulation-time-management` explains how the aspects related to time management in a simulation (e.g. resolution).
* Section :ref:`internal-structure` shows in detail the internal structure of *Optisim* and how it operates, as shown in the figures above.
* Section :ref:`external-interfaces` describes the interfaces used by *Optisim* to interacting with external applications (e.g. controllers).
* Section :ref:`inputs-data` shows the input dataset required by *Optisim* to work properly.
* Section :ref:`outputs-data` explains the simulated results and how they are stored.
* Section :ref:`components-models` describes the models related to the available components.






