from ..utils import setup_logger
from .meter import Meter


def meter_process(ix, name, meteo_data, semaphore, q_force_off, q_out, q_prod, config, logging_conf):
    logging_conf['name'] = logging_conf['name']+'.'+config['meters'][ix]['name']
    logger = setup_logger(**logging_conf)
    logger.info('started meter %i process' % (ix + 1))
    with semaphore:
        logger.info('Init meter %i' % (ix + 1))
        try:
            meter = Meter(config, name, meteo_data, q_prod, ix, logging_conf)
        except Exception as e:
            logger.error(e)
            q_out.put({'ix': ix, 'name': name, 'msg': 'error'})
            return

    q_out.put({'ix': ix, 'name': name, 'msg': 'init ok'})
    logger.debug('Meter %i initialized, listening to force_off queue...' % (ix + 1))

    while True:
        force_off = q_force_off.get()
        logger.debug('Meter %i got force_off queue, waiting for semaphore...' % (ix + 1))
        with semaphore:
            logger.debug('Simulating meter %i...' % (ix + 1))
            if isinstance(force_off, str) and force_off == 'kill':
                logger.info('{} killed'.format(name))
                break
            else:
                res = meter.simulate(force_off_df=force_off, kws=None)
                # meter.wait_until_dumped()
                q_out.put({'ix': ix, 'name': name, 'res': res})

            # meter.reset_dumped()
