import re
from multiprocessing import Event

import numpy as np
import pandas as pd


class Agent:
    def __init__(self, data_manager, broker_producer, descriptor=None, available_vars=None, meter=None):
        """ Constructor
        :param data_manager: object able to get data from InfluxDB and maintain them internally
        :type data_manager: classes.utils.input_data_manager.InputDataManager
        :param broker_producer: broker producer used to send message to the configured broker broker
        :type broker_producer: optisim.utils.rabbit_sim_producer.RabbitSimulationProducer object
        :param descriptor: dictionary containing all the configuration settings needed by Agent
        :type descriptor: dict
        :param available_vars: list of variables whose values will be saved in InfluxDB
        :type available_vars: list
        :param meter: meter object
        :type meter: optisim.agents.meter.Meter object
        """
        self.meter = meter
        self.dumped = Event()
        self.descriptor = descriptor
        self.data_manager = data_manager
        # self.meteo_data_manager = meter.meteo_data_manager
        self.t_origin_unix = data_manager.get_t_unix()[0][0]
        self.dt = self.data_manager.get_dt()
        self.simulation_start_ts = self.data_manager.get_simulation_start_ts()
        self.simulation_day_start_ts = int(pd.to_datetime(self.simulation_start_ts, unit='s').floor('D').timestamp())
        self.simulation_end_ts = self.data_manager.get_simulation_end_ts()

        # broker topics and address
        for i in range(0, len(self.descriptor['results'])):
            # todo implement a function-queue for data_manager.master_descriptor
            self.descriptor['results'][i]['topic'] = next((l for l in self.data_manager.get_master_descriptor()['input_output'] if
                                                           l['name'] == self.descriptor['results'][i]['output_name']),
                                                          None)['topic']
            self.descriptor['results'][i]['measurement'] = next((l for l in self.data_manager.get_master_descriptor()['input_output'] if
                                                           l['name'] == self.descriptor['results'][i]['output_name']),
                                                          None)['measurement']

            assert all([any([re.match(j, k) for k in available_vars]) for j in self.descriptor['results'][i]['selection']]), \
                'Error: isaac is trying to write some variables in the DB, which were not returned by this agent'

            # take the sub-dictionary of data_dict, based on the signals you want to save (self.signal_set)
            take_these = [any([re.match(k, j) for k in self.descriptor['results'][i]['selection']]) for j in available_vars]
            self.descriptor['results'][i]['selection'] = [j for (j, v) in zip(available_vars, take_these) if v]

        # where to save states
        self.state_vars = []
        self.state_descriptor = {'measurement':
                                     next((l for l in self.data_manager.get_master_descriptor()['input_output'] if
                                           l['name'] == 'states_2_db'),
                                          None)['measurement'],
                                 'measurement_base':
                                     next((l for l in self.data_manager.get_master_descriptor()['input_output'] if
                                           l['name'] == 'states_2_db'),
                                          None)['measurement_base'],
                                 'topic':
                                     next((l for l in self.data_manager.get_master_descriptor()['input_output'] if
                                           l['name'] == 'states_2_db'),
                                          None)['topic'],
                                 'save_interval': self.meter.config['simulation']['save_states_interval'] if 'save_states_interval' in self.meter.config['simulation'] else 'daily'}

        self.is_load_states_from_db = self.meter.config['simulation']['load_states_from_db'] if 'load_states_from_db' in \
                                                                                                self.meter.config[
                                                                                                    'simulation'] else False

        self.load_states_from_db_measurement = '{}_{}'.format(
            self.state_descriptor['measurement_base'],
            self.meter.config['simulation']['load_states_from_db_measurement']) if 'load_states_from_db_measurement' in self.meter.config['simulation'] else self.state_descriptor['measurement']

        self.broker_producer = broker_producer

        self.data_buffer = dict()
        for res in self.descriptor['results']:
            d = dict()
            buffer = {sel: np.empty([res['send_interval']]) for sel in res['selection']}
            d['buffer'] = buffer
            d['sel_i'] = 0
            d['timestamps'] = np.empty([res['send_interval']], dtype=np.uint64)
            self.data_buffer.update({res['output_name']: d})

        self.pq_buffer = None
        self.backward_summoning = False
        self.force_off = None
        self.power_factor = 0.1 # TO DO: this need to be changed and configured in the master_descriptor

        if 'phases' in descriptor:
            self.phases = descriptor['phases']
            self.num_phases = len(self.phases)
            self.phases_num = {'a': 0, 'b': 1, 'c': 2}

    def increment_buffer(self, data_dict, time):
        """ Add an element to the InfluxDB buffer
        :param data_dict: dictionary with the data to be saved on the db. Each key in the dictionary must be a member
                          of self.signal_set
        :type data_dict: dict
        :param time: Simulation time in seconds
        :type time: ndarray
        """
        #timestamp_idx = [time]
        timestamp_idx = time

        for res in self.descriptor['results']:
            for k in res['selection']:
                # check if you are writing a multistep output. If so, augment the data_buffer ndarray
                t_start = self.data_buffer[res['output_name']]['sel_i']
                t_end = t_start + np.size(timestamp_idx)
                l_buffer = len(self.data_buffer[res['output_name']]['buffer'][k])
                if l_buffer< t_end:
                    self.data_buffer[res['output_name']]['buffer'][k] = np.append(self.data_buffer[res['output_name']]['buffer'][k],np.zeros((t_end-l_buffer,1)).ravel())
                # write on buffer
                self.data_buffer[res['output_name']]['buffer'][k][t_start:t_end] = data_dict[k].flatten()

            # check if you are writing a multistep output for timestamps. If so, augment the data_buffer ndarray
            t_start = self.data_buffer[res['output_name']]['sel_i']
            t_end = t_start + np.size(timestamp_idx)
            l_buffer = len(self.data_buffer[res['output_name']]['timestamps'])
            if l_buffer< t_end:
                self.data_buffer[res['output_name']]['timestamps']= np.append(self.data_buffer[res['output_name']]['timestamps'],np.zeros((t_end-l_buffer,1)).ravel())
            # write on buffer
            self.data_buffer[res['output_name']]['timestamps'][t_start:t_end] = np.asanyarray(self.simulation_start_ts + time,int)*1e6
            # the current time is incremented by t_end-l_buffer +1
            self.data_buffer[res['output_name']]['sel_i'] += t_end-t_start

            if self.data_buffer[res['output_name']]['sel_i'] >= res['send_interval']:
                self.data_buffer[res['output_name']]['sel_i'] = 0
                to_send = pd.DataFrame(self.data_buffer[res['output_name']]['buffer'],
                                       index=self.data_buffer[res['output_name']]['timestamps'],
                                       dtype=np.float64)
                self.dump_on_broker(to_send, res['topic'], res['measurement'])

        #save states
        ts = int((self.simulation_start_ts + np.atleast_1d(time)[-1]))
        if self.state_descriptor['save_interval'] == 'daily':
            interval_divider = 86400
        elif self.state_descriptor['save_interval'] == 'hourly':
            interval_divider = 3600
        elif self.state_descriptor['save_interval'] == 'always':
            interval_divider = 1
        else:
            raise 'save_states_interval {} not allowed'.format(self.state_descriptor['save_interval'])

        if 'force_off' in self.state_vars:
            if int(ts - self.simulation_day_start_ts) % interval_divider == 0:
                to_send = pd.DataFrame(data={'force_off': [data_dict['force_off'].flatten()[-1]]},
                                       index=[ts*1000000],
                                       dtype=np.float64)
                self.dump_on_broker(to_send, self.state_descriptor['topic'], self.state_descriptor['measurement'])

        if int(ts - self.simulation_day_start_ts + self.dt) % interval_divider == 0:
            to_send = pd.DataFrame(data={k: [data_dict[k].flatten()[-1]] for k in self.state_vars if k != 'force_off'},
                                   index=[ts*1000000],
                                   dtype=np.float64)
            self.dump_on_broker(to_send, self.state_descriptor['topic'], self.state_descriptor['measurement'])

    def dump_on_broker(self, data, topic, measurement):
        """ Dump the data sending a message to the message broker
        :param data: pandas Dataframe with the data to send
        :type data: pandas.Dataframe
        :param topic: topic used to send the message
        :type topic: str
        :param measurement: InfluxDB measurement where data have to be stored
        :type measurement: str
        """
        data = data.to_json()

        dump_me = {
                    'case': 'model',
                    'meter_id': self.descriptor['meter_id'],
                    'bus_id': self.descriptor['bus_id'],
                    'name': self.descriptor['name'],
                    'type': self.descriptor['type'],
                    'measurement': measurement,
                    'data': data
                   }

        future = self.broker_producer.send(topic, dump_me, 'model')
        self.dumped.set()

    def read_internal_dataset(self, time_start, time_end, dataset_name):
        """ Read the internal datasets filtering on time
        :param time_start: simulation start time in seconds
        :type time_start: int
        :param time_end: simulation end time in seconds
        :type time_end: int
        :param dataset_name: dataset name (meteo | force_off)
        :type dataset_name: str
        """
        if dataset_name == 'meteo':
            data = self.data_manager.get_meteo_dataset(time_start + self.simulation_start_ts,
                                                             time_end + self.simulation_start_ts)
        elif dataset_name == 'force_off':
            filt_idx = (self.force_off.index >= (time_start + self.simulation_start_ts) * 1e3) & (
                        self.force_off.index <= (time_end + self.simulation_start_ts) * 1e3)

            data = self.force_off[filt_idx].values.reshape(-1, 1)
        else:
            data = None

        return data

    def write_pq_buffer(self, p, q, time_start, time_end):
        """ Save the P-Q buffer state
        :param p: P dataset
        :type p: ndarray()
        :param q: Q dataset
        :type q: ndarray()
        :param time_start: simulation start time in seconds
        :type time_start: int
        :param time_end: simulation end time in seconds
        :type time_end: int
        """
        # todo this function must be changed to manage the multistep feature
        filt_idx = (self.pq_buffer.index >= (time_start + self.simulation_start_ts) * 1e3) & (
                    self.pq_buffer.index <= (time_end + self.simulation_start_ts) * 1e3)
        self.pq_buffer.iloc[filt_idx] = np.hstack([p, q])

    def get_states(self):
        """Retrieve all the agent's states and encode them into a dictionary"""
        var_names = list(vars(self).keys())
        state_keys = [v for v in var_names if 'state' in v]
        if 'time' in var_names:
            state_keys.append('time')
        states_dict = {key: vars(self)[key] for key in state_keys}

        return states_dict

    def load_states(self, states_dict):
        """Load a dict of saved states into the agent"""
        var_names = list(vars(self).keys())
        state_keys = [v for v in var_names if 'state' in v]
        if 'time' in var_names:
            state_keys.append('time')
        assert np.all([s in state_keys for s in states_dict.keys()]), \
            ValueError('Some of the keys in states_dict are not actual states of {} '.format(self.descriptor['name']))
        [setattr(self, key, val) for (key, val) in states_dict.items()]

    def load_states_from_db(self, states_dict):
        for key, val in states_dict.items():
            try:
                t = val['t'] if 't' in val else 0
                t -= self.dt
                val['tags'].update({'component_type': self.descriptor['type'], 'component_name': self.descriptor['name'], 'meter_id': self.meter.name})
                state_df, _ = self.data_manager.download_df_data(self.load_states_from_db_measurement, val['tags'], ['value'], t_start=t,
                                                                 t_end=t,
                                                                 aggr_fun=None)
                if '[' in key:
                    var, pos = key.split('[')
                    pos = int(pos.split(']')[0])
                    getattr(self, var)[pos] = state_df.values[0][0].copy()
                else:
                    setattr(self, key, state_df.value.values[0].copy())
            except Exception as e:
                self.meter.logger.warning('unable to load state {} for agent {} in measurement {}. Error: {}'.format(key, self.descriptor['name'], self.load_states_from_db_measurement, e))
