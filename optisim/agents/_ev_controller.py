import numpy as np
from abc import abstractmethod


class BaseController:
    def __init__(self, pars, model_pars, descriptor, dt, n_sa):
        self.pars = pars
        self.model_pars = model_pars
        self.random_state = np.random.RandomState(int.from_bytes(descriptor['profile'].encode(), 'little') % 2**32)
        self.e_n = self.get_par('E_max')
        self.eta_in = self.get_par('eff_charge')
        self.eta_out = self.get_par('eff_discharge')
        self.e_max = self.get_par('E_max')
        self.e_min = self.get_par('E_min')
        self.dod = (self.e_max - self.e_min) / self.e_max
        self.p_max_charge = self.get_par('p_max_charge')
        self.p_max_discharge = self.get_par('p_max_discharge')
        self.dt = dt
        self.n_sa = n_sa  # number of control steps
        self.min_soc_for_charging = model_pars['min_soc_for_charging'] if 'min_soc_for_charging' in model_pars.keys() else 1
        self.plug_probability_at_soc_100 = model_pars['plug_probability_at_soc_100'] if 'plug_probability_at_soc_100' in model_pars.keys() else 0.5
        self.min_soc_for_micro_charging = model_pars['min_soc_for_micro_charging'] if 'min_soc_for_micro_charging' in model_pars.keys() else 1

        if 'solver' in pars.keys():
            self.solver = pars['solver']
            if 'solver_pars' in pars.keys():
                self.solver_pars = pars['solver_pars']
            else:
                self.solver_pars = {}
        else:
            self.solver = 'ECOS'
            self.solver_pars = {}

    def get_par(self, key):
        return self.pars[key] if key in self.pars.keys() else self.model_pars[key]


    @abstractmethod
    def control(self, *args):
        """call core controller, return control action"""
        u = None
        return u


class ReactiveController(BaseController):
    def __init__(self, pars, model_pars, descriptor, dts, n_sa):
        super().__init__(pars, model_pars, descriptor, dts, n_sa)
        self.wants_to_charge = 'maybe'
        self.reached_max_soc = False

    def is_going_to_plug_it(self, soc):
        if self.min_soc_for_charging == 1:
            return True
        else:
            return self.random_state.random() > (soc - self.min_soc_for_charging) / (1 - self.min_soc_for_charging) * (
                        1 - self.plug_probability_at_soc_100)

    def control(self, available, reached_max_soc, soc, num_steps, **kwargs):
        """
        If the EV is plugged, charge it with its nominal power
        :param available: binary n_sa np.ndarray, if 1 the car is available to be plugged
        :param reached_max_soc: if True the battery just reached max SOC in the previous time step
        :param soc: state of charge
        :param num_steps: step to simulate
        :param kwargs:
        :return:
        """
        p = np.zeros(num_steps)
        for s in range(num_steps):
            if available[s]:
                if reached_max_soc:
                    self.reached_max_soc = True
                if self.wants_to_charge == 'yes':
                    if not self.reached_max_soc or soc <= self.min_soc_for_micro_charging:
                        p[s] = self.p_max_charge
                        self.reached_max_soc = False

                elif self.wants_to_charge == 'maybe':
                    if self.is_going_to_plug_it(soc):
                        self.wants_to_charge = 'yes'
                        # choose if we want to charge
                        p[s] = self.p_max_charge
                    else:
                        self.wants_to_charge = 'no'
            else:
                self.wants_to_charge = 'maybe'
                self.reached_max_soc = False
        return p
