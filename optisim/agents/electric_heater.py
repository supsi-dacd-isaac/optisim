# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #
import numpy as np

from .agent import Agent


# --------------------------------------------------------------------------- #
# Classes section
# --------------------------------------------------------------------------- #
class Heater(Agent):
    """
    dummy electric heater, when asked returns its nominal power, which is equal to its nominal thermal power
    """
    def __init__(self,
                 data_manager,
                 broker_producer,
                 descriptor=None,
                 pars=None,
                 meter=None,
                 **_kwargs):
        """ Constructor
        :param data_manager: object able to get data from InfluxDB and maintain them internally
        :type data_manager: classes.utils.input_data_manager.InputDataManager
        :param broker_producer: broker producer used to send message to the configured broker broker
        :type broker_producer: optisim.utils.rabbit_sim_producer.RabbitSimulationProducer object
        :param descriptor: dictionary containing all the configuration settings needed by Agent
        :type descriptor: dict
        :param q_nom: nominal Q
        :type q_nom: float
        :param meter: meter object
        :type meter: optisim.agents.meter.Meter object
        """
        available_vars = ['q_heating', 'force_off', 'p_tot', 'q_tot', 'p_a', 'p_b', 'p_c', 'q_a', 'q_b', 'q_c']
        super().__init__(data_manager=data_manager, broker_producer=broker_producer, descriptor=descriptor,
                         available_vars=available_vars, meter=meter)

        self.state_vars.append('force_off')

        if pars is None:
            self.q_nom = 10
            self.cos_phi = 1
        else:
            self.q_nom = pars['q_nom']
            self.cos_phi = pars['cos_phi']

        self.data_manager = data_manager

    def get_q(self, T_in, control_state, time=None):
        """ Get Q
        :param T_in: hot temperature, not needed
        :type T_in: float
        :param control_state: if equal to 1, the heat pump works in heating mode, if equals -1, works in cooling mode
        :type control_state: int
        :param time: simulation time
        :type time: int
        :return: Q
        :rtype: numpy array
        """
        if control_state == 1:
            q_heating = self.q_nom
            p = self.q_nom
        elif control_state == -1:
            q_heating = -self.q_nom
            p = -self.q_nom
        else:
            q_heating = 0
            p = 0

        # consider override
        force_off = self.read_internal_dataset(time, time, 'force_off')
        q_heating = q_heating * (1 - force_off)
        p = p * (1 - force_off)
        q = np.sqrt(1-self.descriptor['pars']['cos_phi']**2)/self.descriptor['pars']['cos_phi'] * p

        p_all = np.zeros((p.shape[0], 3))
        q_all = np.zeros((q.shape[0], 3))

        try:
            for phase in self.phases:
                p_all[:, self.phases_num[phase]] = p / self.num_phases
                q_all[:, self.phases_num[phase]] = q / self.num_phases
        except AttributeError:
            print(self.descriptor)
            raise
        # write on pq_buffer
        self.write_pq_buffer(p_all, q_all, time, time)

        # write data on internal buffer
        buffer_dict = {'p_tot': p,
                       'q_tot': q,
                       'q_heating': q_heating,
                       'force_off': force_off,
                       'p_a': p_all[:, 0],
                       'p_b': p_all[:, 1],
                       'p_c': p_all[:, 2],
                       'q_a': q_all[:, 0],
                       'q_b': q_all[:, 1],
                       'q_c': q_all[:, 2]
                       }

        # write in the current buffer
        dt = self.dt
        times = np.arange(time, time + dt, dt)
        self.increment_buffer(buffer_dict, times)

        # temperature at which the heat for the useful effect is exchanged
        T_source = T_in

        return q_heating, T_source

    def propagate_datamanager(self):
        return
