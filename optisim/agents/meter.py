from datetime import datetime

import krangpower as kp
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from . import battery
from . import ev
from . import boiler
from . import building
from . import electric_heater
from . import floor_heating
from . import heat_pump
from . import pv
from . import uncontrolled
from ..utils import InputDataManager
from ..utils import setup_logger


class _MeterBrokerProducer:
    def __init__(self, config, queue_producer):
        self.queue_producer = queue_producer
        self.config = config

    def send(self, topic, *args):
        for io_item in self.config['input_output']:
            if io_item['topic'] == topic:
                exchange = io_item['exchange']
                break
        else:
            raise ValueError('No valid exchange found for topic {} in the conns file'.format(topic))
        self.queue_producer.put((exchange, topic, {'function': 'send', 'args': args}))


class Meter:

    def __init__(self, config, name, meteo_data, queue_producer, meter_ix, logging_conf):
        self.broker_producer = _MeterBrokerProducer(config, queue_producer)
        self.config = config
        self.descriptor = config['meters'][meter_ix]
        self.check_descriptor(self.descriptor)
        logging_conf['name'] = logging_conf['name'] + '.Meter'
        self.logger = setup_logger(**logging_conf)
        self.queue_producer = queue_producer
        self.meter_ix = meter_ix
        self.name = name  # todo this is here for providing the components with the name of their meter,
                          # allowing for easy access of their generator counterparts in krang

        # define the InputDatamanager
        dateformat = '%Y-%m-%d %H:%M'
        start_time = datetime.strptime(config['simulation']['startDateTime'], dateformat)
        end_time = datetime.strptime(config['simulation']['endDateTime'], dateformat)
        sim_hours = ((end_time - start_time).total_seconds() * kp.UM.s).to('hour')
        self.data_manager = InputDataManager(master_descriptor=config, logger=self.logger,
                                             sim_steps=dict(start=0, end=int(sim_hours.magnitude) * 3600))
        self.data_manager.meteo_dataset_ts = meteo_data
        self.data_manager.set_t_unix()

        self.t_origin_unix = self.data_manager.get_t_unix()[0][0]
        self.kws = None
        self._latest_pq_dataframe_tot = None
        self.simulation_start_ts = self.data_manager.get_simulation_start_ts()
        self.simulation_step_ts = self.data_manager.get_simulation_start_ts()

        # instantiate each component in the descriptor
        components = []
        for component_desc in self.descriptor['components']:
            components.append(self.init_component(component_desc, data_manager=self.data_manager))
        self.components = components
        self.components_names = self.get_component_list(self.descriptor['components'])

    def wait_until_dumped(self):
        for c in self.components:
            c.dumped.wait()

    def reset_dumped(self):
        for c in self.components:
            c.dumped.clear()

    def simulate(self, force_off_df, kws):

        # initialize force_off signals in all the components
        self.init_force_off(force_off_df)
        self.init_simulation_buffer()
        self.kws = kws

        # simulate components
        t_start = int(force_off_df.index[0] / 1e3 - self.simulation_start_ts)
        t_end = int(force_off_df.index[-1] / 1e3 - self.simulation_start_ts)
        self.simulation_step_ts = t_start

        # cycling the simulation over the components
        for i in range(0, len(self.components)):
            self.logger.debug('Simulate meter: {}, component: {}, type: {}'
                              .format(self.descriptor['name'],
                                      self.descriptor['components'][i]['name'],
                                      self.descriptor['components'][i]['type']))

            self.components[i].simulate(t_start=t_start, t_end=t_end)

        pq_dataframe = self.retrieve_pq()

        return pq_dataframe

    def init_component(self, component_descriptor, data_manager):
        """
        Initialize components. Expand on 'heating_elements' and 'components' subdictionaries
        :param component_descriptor:
        :return:
        """

        self.check_component(component_descriptor)

        # augment descriptor with meter and bus ids (used later to write data on rabbit)
        component_descriptor["meter_id"] = self.descriptor["name"]
        component_descriptor["bus_id"] = self.descriptor["bus"]

        # if heating element or domestic hot water descriptor are present, go on initializing them, recursively
        if 'heating_element' in component_descriptor.keys():
            heating_element = self.init_component(component_descriptor['heating_element'], data_manager)
        else:
            heating_element = None

        if 'dhw' in component_descriptor.keys():
            dhw_element = self.init_component(component_descriptor['dhw'], data_manager)
        else:
            dhw_element = None

        # initialize component based on its declared type
        if component_descriptor['type'] == 'building':
                component = building.Building(data_manager=data_manager,
                                              broker_producer=self.broker_producer,
                                              descriptor=component_descriptor,
                                              pars=component_descriptor['pars'],
                                              heating_element=heating_element,
                                              meter=self)

        elif component_descriptor['type'] == 'boiler':
                component = boiler.Boiler(data_manager=data_manager,
                                          broker_producer=self.broker_producer,
                                          descriptor=component_descriptor,
                                          pars=component_descriptor['pars'],
                                          heating_element=heating_element,
                                          meter=self)

        # todo still to be implemented
        # elif component_descriptor['type'] == 'boiler_fmu':
        #         component = boilerFMU.BoilerFMU(data_manager=data_manager,
        #                                         broker_producer=self.broker_producer,
        #                                         descriptor=component_descriptor,
        #                                         pars=component_descriptor['pars'],
        #                                         heating_element=heating_element,
        #                                         meter=self)

        elif component_descriptor['type'] == 'floor_heating':
                component = floor_heating.FloorHeating(data_manager=data_manager,
                                                       broker_producer=self.broker_producer,
                                                       descriptor=component_descriptor,
                                                       pars=component_descriptor['pars'],
                                                       heating_element=heating_element,
                                                       meter=self)

        elif component_descriptor['type'] == 'heat_pump':
            component = heat_pump.HeatPump(data_manager=data_manager,
                                           broker_producer=self.broker_producer,
                                           descriptor=component_descriptor,
                                           pars=component_descriptor['pars'],
                                           dhw_tank=dhw_element,
                                           meter=self)

        elif component_descriptor['type'] == 'electric_heater':
            component = electric_heater.Heater(data_manager=data_manager,
                                               broker_producer=self.broker_producer,
                                               descriptor=component_descriptor,
                                               pars=component_descriptor['pars'],
                                               meter=self)

        elif component_descriptor['type'] == 'uncontrolled':
            component = uncontrolled.Uncontrolled(data_manager=data_manager,
                                                  broker_producer=self.broker_producer,
                                                  descriptor=component_descriptor,
                                                  meter=self)

        elif component_descriptor['type'] == 'pv':
            component = pv.PV(data_manager=data_manager,
                              broker_producer=self.broker_producer,
                              descriptor=component_descriptor,
                              meter=self)

        elif component_descriptor['type'] == 'battery':
            component = battery.Battery(data_manager=data_manager,
                                        broker_producer=self.broker_producer,
                                        descriptor=component_descriptor,
                                        pars=component_descriptor['pars'],
                                        meter=self)

        # todo still to be implemented
        # elif component_descriptor['type'] == 'battery_fmu':
        #     component = batteryFMU.BatteryFMU(data_manager=data_manager,
        #                                       broker_producer=self.broker_producer,
        #                                       descriptor=component_descriptor,
        #                                       pars=component_descriptor['pars'],
        #                                       meter=self)

        elif component_descriptor['type'] == 'dhw':
            component = boiler.Boiler(data_manager=data_manager,
                                      broker_producer=self.broker_producer,
                                      descriptor=component_descriptor,
                                      pars=component_descriptor['pars'],
                                      heating_element=heating_element,
                                      meter=self)

        elif component_descriptor['type'] == 'ev':
            component = ev.EV(data_manager=data_manager,
                                        broker_producer=self.broker_producer,
                                        descriptor=component_descriptor,
                                        pars=component_descriptor['pars'],
                                        meter=self)
        else:
            raise ValueError('The type of the component is not in the list of known components!')

        return component

    def init_force_off(self, force_off_df, components=None):
        if components is None:
            components = self.components
        for component in components:
            if 'heating_element' in component.descriptor.keys():
                self.init_force_off(force_off_df, components=[component.heating_element])
            elif 'dhw' in component.descriptor.keys():
                self.init_force_off(force_off_df, components=[component.dhw_tank])

            col_names = list(force_off_df.columns)
            component_idx = [name == component.descriptor['name'] for name in col_names]

            # if the component name is present in force_off_df, store values in component. If not present set as
            # uncontrolled. If present multiple times raise error
            if sum(component_idx) == 1:
                self.logger.debug('component.descriptor[\'name\'] -> %s' % component.descriptor['name'])
                component.force_off = force_off_df[component.descriptor['name']]
            elif sum(component_idx) == 0:
                if component.descriptor['type'] in ['battery', 'ev']:
                    component.force_off = pd.DataFrame(np.nan*np.empty(len(force_off_df.index)), index=force_off_df.index, dtype=np.float)
                else:
                    component.force_off = pd.DataFrame(np.zeros(len(force_off_df.index)), index=force_off_df.index, dtype=np.float)
            elif sum(component_idx) > 1:
                raise ValueError('the decision Dataframe has multiple entries matching to the component name: %s'
                                 % component.descriptor['name'])

    def init_simulation_buffer(self, components=None):
        if components is None:
            components = self.components
        for component in components:
            if 'heating_element' in component.descriptor.keys():
                self.init_simulation_buffer(components=[component.heating_element])
            elif 'dhw' in component.descriptor.keys():
                self.init_simulation_buffer(components=[component.dhw_tank])

            n_steps = len(component.force_off)
            component.pq_buffer = pd.DataFrame(np.zeros((n_steps, 6)),index=component.force_off.index,
                                               columns=['P1', 'P2', 'P3', 'Q1', 'Q2', 'Q3'], dtype=np.float)

    def retrieve_pq(self, components=None):
        pq_dataframe_sub = None
        pq_dataframes = []
        pq_dataframe_tot = []
        if components is None:
            components = self.components
        for component in components:
            if 'heating_element' in component.descriptor.keys():
                pq_dataframe_sub = self.retrieve_pq(components=[component.heating_element])
            elif 'dhw' in component.descriptor.keys():
                pq_dataframe_sub = self.retrieve_pq(components=[component.dhw_tank])
            else:
                pq_dataframe_sub = None

            if pq_dataframe_sub is not None:
                pq_dataframe = component.pq_buffer + pq_dataframe_sub
            else:
                pq_dataframe = component.pq_buffer

            pq_dataframes.append(pq_dataframe)

        pq_dataframe_tot = pd.DataFrame(np.zeros((len(pq_dataframes[0]), 12)),
                                        columns=['P1', 'P2', 'P3', 'Q1', 'Q2', 'Q3',
                                                 'P1z', 'P2z', 'P3z', 'Q1z', 'Q2z', 'Q3z'],
                                        index=pq_dataframes[0].index,
                                        dtype=np.float)
        for d in pq_dataframes:
            pq_dataframe_tot += d

        self._latest_pq_dataframe_tot = pq_dataframe_tot

        return pq_dataframe_tot

    def get_my_powers(self, phases=3):
        if self._latest_pq_dataframe_tot is not None:
            for trm in range(1, phases + 1):

                p = -self._latest_pq_dataframe_tot.tail(1).iloc[0]['P' + str(trm)]
                q = -self._latest_pq_dataframe_tot.tail(1).iloc[0]['Q' + str(trm)]

                yield p + 1j*q
        else:
            for trm in range(1, 4):
                yield 0j

    @staticmethod
    def check_descriptor(descriptor):
        """
        Check if the dictionary descriptor is a valid descriptor
        :param descriptor: dictionary
        :return: boolean, True = valid descriptor
        """
        required_keys = ['name','simulation_kwargs','krang_pars','components']
        assert all([k in descriptor.keys() for k in required_keys]),'Error: some required keys are not present in the ' \
                                                                    'meter descriptor'

    @staticmethod
    def check_component(descriptor):
        required_component_keys = ['name','type','profile','fields','pars','results']
        assert all([k in descriptor.keys() for k in required_component_keys]), 'Error: some required keys are not ' \
                                                                              'present in the meter %s descriptor' \
                                                                              % descriptor['name']

    def plot_isaac_buffer(self,components=None):
        if components is None:
            components = self.components
        for component in components:
            if 'heating_element' in component.descriptor.keys():
                self.plot_isaac_buffer(components=[component.heating_element])
            elif 'dhw' in component.descriptor.keys():
                self.plot_isaac_buffer(components=[component.dhw_tank])

            if np.shape(component.data_buffer_isaac)[1]>0:
                plt.figure()
                plt.plot(component.data_buffer_isaac)
                plt.title(component.descriptor['name'])
                plt.show()

    @staticmethod
    def get_component_list(descriptor):
        names = []
        subnames = []
        if type(descriptor) is dict:
            names.append(descriptor['name'])
            if 'heating_element' in descriptor.keys():
                subnames = Meter.get_component_list(descriptor['heating_element'])
            elif 'dhw' in descriptor.keys():
                subnames = Meter.get_component_list(descriptor['dhw'])
            names += subnames
        else:
            for i in np.arange(len(descriptor)):
                names += Meter.get_component_list(descriptor[i])

        return names

    def get_states(self,components=None):
        """Get all the components states"""
        states_dict = {}
        if components is None:
            components = self.components
        for component in components:
            partial_dict = component.get_states()
            exclusion_list = ['heating_element','dhw','dhw_tank']
            if np.any([ex in component.descriptor.keys() for ex in exclusion_list]):
                excluded_names = [k for k in vars(component).keys() if np.any([k in ex for ex in exclusion_list])]
                for ex in excluded_names:
                    sub_dict = self.get_states(components=[eval('component.'+ ex)])
                    partial_dict[ex + '_child'] = sub_dict
            states_dict[component.descriptor['name']] = partial_dict
        return states_dict

    def load_states(self,states_dict,components=None):
        """Load all the components states"""
        if components is None:
            components = self.components
        for component in components:
            c_name = component.descriptor['name']
            c_states = states_dict[c_name]
            exclusion_list = ['heating_element_child', 'dhw_child', 'dhw_tank_child']
            exclusion_names = ['heating_element', 'dhw', 'dhw_tank']
            this_component_states = {k: c_states[k] for k in c_states.keys() if np.all([e not in k for e in exclusion_list])}
            component.load_states(this_component_states)
            print(component.descriptor['name'])
            for i,e in enumerate(exclusion_list):
                child_component_states = {k: c_states[k] for k in c_states.keys() if e in k}
                for k in child_component_states.keys():
                    ccs = child_component_states[k]
                    if ccs is None:
                        pass
                    else:
                        self.load_states(ccs, components=[eval('component.' + exclusion_names[i])])


