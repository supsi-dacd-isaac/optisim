# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #
import numpy as np
from numba import jit, float64
from scipy.optimize import minimize

from .agent import Agent
from .boiler import Boiler


# --------------------------------------------------------------------------- #
# Functions
# --------------------------------------------------------------------------- #
def heating_pars_builder(r_up_eq=5.29, r_down_eq=100, c=4.186, m_flow=0.2,nominal_power=7.6):
    rho = (r_up_eq+r_down_eq)/(r_up_eq*r_down_eq)
    T_sh_nom = 35
    T_z_nom = 20
    l_min = 100
    T_g =10
    m_flow_nom, l = choose_m_flow_nom(T_sh_nom,T_z_nom,T_g,r_down_eq,r_up_eq,rho,c,nominal_power)
    m_flow = max(m_flow,m_flow_nom)
    l = max(l_min,l)
    x_adim = c * m_flow / rho
    heating_pars = {
                    'c': c,
                    'm_flow': m_flow,
                    'r_up_eq': r_up_eq,
                    'r_down_eq': r_down_eq,
                    'rho': rho,
                    'l': l,            # length of the serpentine
                    'x_adim': x_adim,      # adimensional length
                    'T_sh_nom':T_sh_nom,
                    'T_z_nom':T_z_nom,
                    'T_g':T_g
                    }
    return heating_pars

# --------------------------------------------------------------------------- #
# Classes
# --------------------------------------------------------------------------- #
class FloorHeating(Agent):
    def __init__(self,
                 data_manager,
                 broker_producer,
                 descriptor=None,
                 pars=None,
                 heating_element=None,
                 meter=None,
                 **_kwargs):
        """ Constructor
        :param data_manager: object able to get data from InfluxDB and maintain them internally
        :type data_manager: classes.utils.input_data_manager.InputDataManager
        :param broker_producer: broker producer used to send message to the configured broker broker
        :type broker_producer: optisim.utils.rabbit_sim_producer.RabbitSimulationProducer object
        :param descriptor: dictionary containing all the configuration settings needed by Agent
        :type descriptor: dict
        :param pars: dictionary containing all the configuration parameters
        :type pars: dict
        :param heating_element: list of variables whose values will be saved in InfluxDB
        :type heating_element: optisim.agents.electric_heater.Heater object
        :param meter: meter object
        :type meter: optisim.agents.meter.Meter object
        """
        available_vars = ['T_mean', 'q_heating', 'control_state', 'T_outlet', 'T_inlet']
        super().__init__(data_manager=data_manager, broker_producer=broker_producer, descriptor=descriptor,
                         available_vars=available_vars, meter=meter)

        self.state_vars.append('T_inlet')
        self.state_vars.append('T_outlet')

        self.pars = pars

        if pars is None:
            heating_pars = heating_pars_builder()
        else:
            heating_pars = heating_pars_builder(pars['r_up_eq'], pars['r_down_eq'], pars['c'], pars['m_flow'], pars['nominal_power'])

        if heating_element is None:
            boiler_descriptor = {'meter_id': descriptor['meter_id'],
                                 'load_type': 'heat_pump',
                                'signal_set': ['Q_heating', 'P_heating'],
                             }
            heating_element = Boiler(data_manager, descriptor=boiler_descriptor)

        self.data_manager = data_manager
        self.heating_pars = heating_pars
        self.heating_element = heating_element
        self.state_T_inlet = 35.0
        self.state_T_outlet = 30.0
        self.T_g = heating_pars['T_g']
        valve_pars = {'a_heat': descriptor['pars']['valve_a_heat'], 'b_heat': descriptor['pars']['valve_b_heat'],
                      'a_cool': descriptor['pars']['valve_a_cool'], 'b_cool': descriptor['pars']['valve_b_cool']}
        self.three_way_valve = ThreeWayValve(valve_pars)

        # init from db
        if self.is_load_states_from_db:
            states = dict()
            states.update({'state_T_inlet': {'tags': {'signal': 'T_inlet'}}})
            states.update({'state_T_outlet': {'tags': {'signal': 'T_outlet'}}})
            self.load_states_from_db(states)

    def get_q(self, T_z,working_mode, control_state, time, T_a=0):
        """ Get Q
        :param T_z: internal air building temperature
        :type T_z: int
        :param control_state: if True, compute
        :type control_state: bool
        :param time: if 1, the floor heating works in heating mode, if -1, works in cooling mode
        :type time: int
        :param T_a: external temperature
        :type T_a: float
        :return: Q
        :rtype: numpy array
        """
        # ask m_flow to three way valve
        if control_state:
            m_flow_source = self.three_way_valve.get_mass_flow(working_mode, self.state_T_outlet, self.heating_element.state[-1], T_a, self.heating_pars['m_flow'])
        else:
            m_flow_source = 0
        # ask power to the heating element, using the current T_outlet as referral temperature
        q_heating, T_source = self.heating_element.get_q(self.state_T_outlet, working_mode, time=time,
                                                         m_flow_input=m_flow_source)

        # update inlet temperature with the outlet temperature of the boiler
        self.state_T_inlet = (T_source*m_flow_source+self.state_T_outlet*(self.heating_pars['m_flow'] - m_flow_source)) / self.heating_pars['m_flow']

        # if the hysteresis state is not zero,  the heating system pump is active: compute ingoing/outgoing
        #  thermal power
        # if control_state:
        # compute the new outlet temperature
        if working_mode != 0:
            T_star = (T_z*self.heating_pars['r_down_eq'] + self.T_g*self.heating_pars['r_up_eq'])/(self.heating_pars['r_down_eq']+self.heating_pars['r_up_eq'])
            self.state_T_outlet = T_star + (self.state_T_inlet - T_star)*np.exp(-self.heating_pars['l']/self.heating_pars['x_adim'])

            # compute heating/cooling power into the building
            q_tot = self.heating_pars['m_flow']*self.heating_pars['c']*(self.state_T_inlet-self.state_T_outlet)
            #q = q_tot*(self.heating_pars['r_up_eq']/(self.heating_pars['r_down_eq']+self.heating_pars['r_up_eq']) )
            delta_T = -(self.state_T_outlet -self.state_T_inlet)
            q_up = ((T_star -T_z)*self.heating_pars['l']-(self.state_T_outlet-self.state_T_inlet)*self.heating_pars['x_adim'])/self.heating_pars['r_up_eq']
            q_down = ((T_star -self.T_g)*self.heating_pars['l']-(self.state_T_outlet-self.state_T_inlet)*self.heating_pars['x_adim'])/self.heating_pars['r_down_eq']
            q_check = q_tot-q_up-q_down
            q = q_up/1000
        else:
            q = np.zeros(1)

        T_mean = (self.state_T_inlet+self.state_T_outlet)/2

        ##################################
        # write data on internal buffer
        ##################################

        buffer_dict = {'T_mean': T_mean,
                       'q_heating': q,
                       'control_state': control_state,
                       'T_outlet': np.float64(self.state_T_outlet),
                       'T_inlet': np.float64(self.state_T_inlet)}

        # write in the current buffer
        self.increment_buffer(buffer_dict, time)

        return q, T_mean

    def propagate_datamanager(self):
        self.heating_element.data_manager = self.data_manager
        self.heating_element.propagate_datamanager()


class ThreeWayValve:
    def __init__(self, pars):
        """ 3-way valve
        :param pars: valve parameters
        :type pars: dict
        """
        # slope: y = a*x + b
        self.a_heat = pars['a_heat']
        self.b_heat = pars['b_heat']
        self.a_cool = pars['a_cool']
        self.b_cool = pars['b_cool']

    def get_mass_flow(self, working_mode, T_in, T_tank, T_ext, m_flow_nom):
        return self._get_mass_flow(self.a_heat if working_mode == 1 else self.a_cool, self.b_heat if working_mode == 1 else self.b_cool, T_in, T_tank, T_ext, m_flow_nom)

    @staticmethod
    @jit(float64(float64, float64, float64, float64, float64, float64), nopython=True)
    def _get_mass_flow(a, b, T_in, T_tank, T_ext, m_flow_nom):
        return max(0, min((a * T_ext + b - T_in) / (T_tank - T_in) * m_flow_nom, m_flow_nom))

def choose_m_flow_nom(T_inlet,T_z,T_g,r_down_eq,r_up_eq,rho,c,nominal_power):
    x_0 = np.array([0.1,100])
    result = minimize( of_m_flow, x_0, method='SLSQP',  args=(T_inlet, T_z, T_g, r_down_eq, r_up_eq,  rho, c, nominal_power), bounds=[(0.05, 1), (0, 10000)])
    results = result.x
    m_flow, l = results
    return m_flow, l

def of_m_flow(x,T_inlet,T_z,T_g,r_down_eq,r_up_eq,rho,c,nominal_power):

    m_flow = x[0]
    l = x[1]

    T_star = (T_z * r_down_eq + T_g * r_up_eq) / (r_down_eq + r_up_eq)
    T_outlet = T_star + (T_inlet - T_star) * np.exp(-l / (c*m_flow/rho))
    q_up = ((T_star - T_z) * l - (T_outlet - T_inlet) * (c*m_flow/rho)) / r_up_eq/1000
    m_flow_punish = 0.001*(0.27-m_flow)**2
    of = (nominal_power-q_up)**2 +m_flow_punish
    return of