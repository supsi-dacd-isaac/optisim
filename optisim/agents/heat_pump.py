import os

import numpy as np
import pandas as pd
from scipy import interpolate

from .agent import Agent

_prefix = os.getcwd()
_rel_path = 'optisim/optisim/agents/data/HP/'

def heat_pump_builder(Q_nom,model='default', cos_phi=0.75):
    Q = pd.read_csv(os.path.join(_prefix, _rel_path, model + '_Q.csv'),header=None)
    P = pd.read_csv(os.path.join(_prefix, _rel_path, model + '_P.csv'),header=None)
    # Q = pd.read_csv(_prefix + _rel_path + model + '_Q.csv', header=None)
    # P = pd.read_csv(_prefix + _rel_path + model + '_P.csv', header=None)
    pars = {'model':model,
            'P_table':P.values,
            'Q_table':Q.values,
            'Q_nom':Q_nom,
            'cos_phi': cos_phi
            }
    return pars


class HeatPump(Agent):
    def __init__(self,
                 data_manager,
                 broker_producer,
                 pars=None,
                 descriptor=None,
                 dhw_tank=None,
                 meter=None,
                 **_kwargs):
        available_vars = ['q_heating', 'p_tot', 'q_tot', 'p_a', 'p_b', 'p_c', 'q_a', 'q_b', 'q_c', 'force_off',
                          'T_eva', 'T_cond','control_state']
        super().__init__(data_manager=data_manager, broker_producer=broker_producer, descriptor=descriptor, available_vars=available_vars, meter=meter)

        self.state_vars.append('force_off')

        self.data_manager = data_manager

        # define q_nom is the nominal power at Teva = -4, Tcond = 35
        if pars is None:
            pars = heat_pump_builder(7.5, 'default')
        else:
            pars = heat_pump_builder(pars['Q_nom'], pars['model'], pars['cos_phi'])

        self.q_nom = pars['Q_nom']
        self.P_table = pars['P_table']
        self.Q_table = pars['Q_table']

        # create linear interpolator for P
        T_cond = self.P_table[1:,0]
        T_eva = self.P_table[0, 1:]
        assert all(np.diff(T_cond) > 0), 'err: condensation temperature vector (columns) must be monotone'
        assert all(np.diff(T_eva) > 0), 'err: evaporation temperature vector (rows) must be monotone'
        assert len(np.unique(T_cond)) == len(T_cond), 'err: condensation temperatures (columns) must be unique'
        assert len(np.unique(T_eva)) == len(T_eva), 'err: evaporation temperatures (rows) must be unique'

        P = self.P_table[1:, 1:]
        self.p_f = interpolate.interp2d(T_eva, T_cond, P, kind='linear')

        # create linear interpolator for Q
        T_cond = self.Q_table[1:, 0]
        T_eva = self.Q_table[0, 1:]

        Q = self.Q_table[1:, 1:]
        self.q_f = interpolate.interp2d(T_eva, T_cond, Q, kind='linear')
        # define Q ref as the power at Teva = -5, Tcond = 35
        self.q_ref = self.q_f(-5, 35)

        # If a domestic hot water tank is present, it is first simulated at each call of get_qp
        self.dhw_tank = dhw_tank
        if dhw_tank is not None:
            self.dhw_tank.heating_element = self

        self.simulate_dhw = True

    def get_q(self, T_in, control_state, time, T_outside=None):
        """
        :param T_in: condensator (hot) temperature
        :param control_state: if equal to 1, the heat pump works in heating mode, if equals -1, works in cooling mode
        :return:
        """

        # find current external temperature input
        if T_outside is None:
            T_outside = self.read_internal_dataset(time,time,'meteo')['T'].values[0]

        if self.dhw_tank is not None and self.simulate_dhw:

            # simulate the dhw tank from its internal current time up to the next timestep
            # load domestic hot water profile up to now
            t_start = self.dhw_tank.time
            t_end = time

            fields = self.dhw_tank.descriptor['fields']
            tag = {'type': self.dhw_tank.descriptor['type'],
                   'name': self.dhw_tank.descriptor['profile']}
            aggr_fun = 'sum'
            name = self.descriptor['name']
            interval = self.dt
            dhw_data = self.data_manager.get_simulation_dataset(t_start, t_end, name,fields, tag, interval,aggr_fun)

            dhw_data = dhw_data[self.dhw_tank.descriptor['fields']].values.reshape(-1,1)/self.dt

            # in case of backward summoning, augment the force off vector
            self.simulate_dhw = False
            q_dhw, y = self.dhw_tank.simulate(dhw_data, working_mode=1, t_end=t_end, t_start=t_start)
            self.simulate_dhw = True

            # override heating if the heat pump generated heat for the dhw during this timestep
            force_off_dhw = np.asanyarray(q_dhw[-1] > 0,dtype=int)
        else:
            q_dhw = np.zeros((1, 1))
            force_off_dhw = np.zeros((1, 1),dtype=int)

        if control_state == 1:
            T_eva = T_outside
            T_cond = T_in
            q_heating = self.q_f(T_eva, T_cond) * self.q_nom/self.q_ref
            p = self.p_f(T_eva, T_cond) * self.q_nom / self.q_ref

        elif control_state == -1:
            T_cond = T_outside
            T_eva = T_in
            q_heating = self.q_f(T_eva, T_cond) * self.q_nom/self.q_ref
            p = self.p_f(T_eva, T_cond) * self.q_nom / self.q_ref
            q_heating = p-q_heating # refrigerating thermal power

        else:
            T_eva = T_outside
            T_cond = T_in
            q_heating = 0
            p = 0

        T_source = T_in

        # consider override
        force_off = self.read_internal_dataset(time, time, 'force_off')
        q_heating = q_heating * (1 - force_off) * (1 - force_off_dhw)
        p = p * (1 - force_off) * (1 - force_off_dhw)
        q = np.sqrt(1 - self.descriptor['pars']['cos_phi'] ** 2) / self.descriptor['pars']['cos_phi'] * p

        p_all = np.zeros((force_off.shape[0], 3))
        q_all = np.zeros((force_off.shape[0], 3))
        for phase in self.phases:
            p_all[:, self.phases_num[phase]] = p / self.num_phases
            q_all[:, self.phases_num[phase]] = q / self.num_phases

        if ((q_heating > 0) and not self.simulate_dhw) or (self.simulate_dhw and not force_off_dhw):
            ##################################
            # write on pq_buffer
            ##################################

            self.write_pq_buffer(p_all, q_all, time, time)

            ##################################
            # write data on internal buffer
            ##################################

            buffer_dict = {'q_heating': q_heating,
                           'p_tot': p,
                           'q_tot': q,
                           'p_a': p_all[:, 0],
                           'p_b': p_all[:, 1],
                           'p_c': p_all[:, 2],
                           'q_a': q_all[:, 0],
                           'q_b': q_all[:, 1],
                           'q_c': q_all[:, 2],
                           'force_off': force_off,
                           'T_eva': T_eva,
                           'T_cond': T_cond,
                           'control_state': control_state
                           }

            # write in the current buffer
            self.increment_buffer(buffer_dict, time)

        return q_heating, T_source

    def propagate_datamanager(self):
        return
