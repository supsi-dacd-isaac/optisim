from time import time as time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from datetime import timezone, datetime

from .agent import Agent
from ._ev_controller import ReactiveController


def sim_pars_builder(do_plots=False, data_source='.'):
    """
    Initialize the battery parameters dictionary

    :param do_plots: if true, plot states and variables at the end of the simulations
    :param data_source: data source for the simulation, as a local or http address
    :type do_plots: bool
    :type data_source: string
    :return: parameters
    :rtype: dict
    """
    pars = {'do_plots': do_plots}
    return pars


class EV(Agent):
    """ Model of an EV"""
    def __init__(self,
                 data_manager,
                 descriptor=None,
                 pars=None,
                 broker_producer=None,
                 sim_pars=sim_pars_builder(),
                 meter=None,
                 **_kwargs):
        """ Constructor

        :param data_manager: object able to get data from InfluxDB and maintain them internally
        :param descriptor: dictionary containing all the configuration settings needed by Agent
        :param pars: dictionary containing all the model parameters
        :param broker_producer: broker producer used to send message to the configured broker broker
        :param sim_pars: dictionary containing all the simulation parameters
        :param state: state of the battery (currently not used)
        :param meter: meter object
        :type data_manager: classes.utils.input_data_manager.InputDataManager
        :type descriptor: dict
        :type pars: dict
        :type broker_producer: optisim.utils.rabbit_sim_producer.RabbitSimulationProducer object
        :type sim_pars: dict
        :type state: int
        :type meter: optisim.agents.meter.Meter object
        """
        available_vars = ['p_tot', 'q_tot', 'p_a', 'p_b', 'p_c', 'q_a', 'q_b', 'q_c', 'SOC', 'p_main',
                          'p_main_no_batt', 'force_off', 'p_set', 'plug_state', 'available_steps']

        super().__init__(data_manager=data_manager, descriptor=descriptor, broker_producer=broker_producer,
                         available_vars=available_vars, meter=meter)

        self.state_vars.append('SOC')
        self.state_vars.append('force_off')

        # set datamanager
        self.data_manager = data_manager
        self.descriptor = descriptor

        # check that all the parameters are present
        pars_list = {
                     'E_min',
                     'E_max',
                     'eff_charge',
                     'eff_discharge',
                     'p_max_charge',
                     'p_max_discharge',
                     'use_internal_controller',
                     'tau_sd'
                     }
        assert all([k in pars.keys() for k in pars_list]), "ERROR: some keys are not present in ev_pars"
        sim_pars_list = {'do_plots'}
        assert all([k in sim_pars.keys() for k in sim_pars_list]), "ERROR: some keys are not present in sim_pars"

        self.pars = pars
        self.sim_pars = sim_pars

        # -----------------------
        # initialize state and time
        # -----------------------

        # normal initialization
        self.state = self.pars['init_soc'] if 'init_soc' in self.pars.keys() else self.pars['E_max']
        if self.state > self.pars['E_max']:
            self.meter.logger.warning('your init_soc is bigger than E_max in {}'.format(self.descriptor['name']))
            self.state = self.pars['E_max']
        elif self.state < self.pars['E_min']:
            self.meter.logger.warning('your init_soc is smaller than E_min in {}'.format(self.descriptor['name']))
            self.state = self.pars['E_min']

        #init from db
        if (self.pars['load_init_soc'] if 'load_init_soc' in self.pars.keys() else False) or \
                self.is_load_states_from_db:
            states = {'state': {'tags': {'signal': 'SOC'}}}
            self.load_states_from_db(states)

        self.charging_state = 0
        self.time = 0
        self.reached_e_max = False

        if pars['use_internal_controller']:
            self.internal_controller = ReactiveController(pars['controller'], pars, descriptor, self.dt, n_sa=1)
        self.Pb_old = 0

        # retrieve the whole year scheduling for this EV
        fields = self.descriptor['fields'].copy()
        tag = {'type': self.descriptor['type'],
               'name': self.descriptor['profile']}
        measurement = self.data_manager.master_descriptor['influxdb']['inputMeasurement']
        self.influx_data, _ = self.data_manager.download_df_data(measurement, tag, fields, t_start=0-86400*30,
                                                           t_end=self.simulation_end_ts-self.simulation_start_ts,
                                                                 aggr_fun=None, aggr_tag='location')

        # the plug_status df is a full dataset with an entry per timestep, for the whole simulation
        # the kwh_used is a sparse dataset, containing the kwh used by the EV for every arrival event
        self.plug_state, self.available_steps,  self.kwh_used = \
            self.build_ev_datasets(0, self.simulation_end_ts-self.simulation_start_ts)

    def simulate(self, t_start=0, t_end=15*60):
        """ Perform a simulation step

        :param t_start: starting time of simulation, default = 0
        :param t_end: final time of simulation
        :type t_start: int
        :type t_end: int
        :return: p_real, injected and drawn power by the battery
        :rtype: numpy ndarray
        """

        # if an arrival time is contained in the current time range, account for the consume kWh from the beginning of
        # this time range
        kwh_used = self.kwh_used[(t_start + self.simulation_start_ts <= self.kwh_used.index) &
                                 (self.kwh_used.index < t_end + self.simulation_start_ts + self.dt)]
        if not kwh_used.empty:
            if len(kwh_used) > 1:
                print('EV WARNING: more than one charging event detected between t_start {} '
                      'and t_end {}'.format(t_start, t_end))
            e_used = kwh_used.values.sum()
            self.state = self.state - e_used
            if self.state < self.pars['E_min']:
                print('EV WARNING: too much energy was requested by the kWh_used dataframe. E set to E_min')
                self.state = self.pars['E_min']

        # get power setpoint from mulder-algo
        force_off = self.read_internal_dataset(t_start, t_end, 'force_off')
        p_set = force_off.copy()

        # read power of the household's main
        P_m = np.asarray(np.real(-sum(self.meter.get_my_powers())))
        if abs(P_m) > 1e9:
            P_m = 0

        n_steps = int((t_end - t_start) / self.dt) + 1

        plugged = self.plug_state[(t_start <= self.plug_state.index) &
                                  (self.plug_state.index <= t_end)].values

        if any(np.isnan(p_set)):
            if self.pars['use_internal_controller']:
                soc = (self.state-self.pars['E_min'])/(self.pars['E_max']-self.pars['E_min'])
                p_set = self.internal_controller.control(plugged, self.reached_e_max, soc, n_steps)
            else:
                p_set = np.zeros_like(plugged)

        # if the car is not plugged do not charge or discharge
        p_set[not plugged] = 0

        x = np.zeros(n_steps+1)

        x[0] = self.state
        p_in = np.zeros(n_steps+1)  # power in
        p_out = np.zeros(n_steps+1)  # power out
        p = np.zeros(n_steps+1)  # realized power

        if not(isinstance(p_set, (list, tuple, np.ndarray))):
            p_set = np.ones(n_steps)*p_set

        # physical model of the battery: just accounts for the charging/discharging power and trim it if capacity limits
        # are violated.

        dt_int = int(self.dt)


        for i in range(0, n_steps):
            p_in[i] = min(max(p_set[i], 0), self.pars['p_max_charge'])
            p_out[i] = min(max(-p_set[i], 0), self.pars['p_max_discharge'])

            if x[i] <= 0:
                p_out[i] = 0
            elif x[i] >= self.pars['E_max']:
                p_in[i] = 0

            if p_in[i] > 0:
                x[i+1] = x[i] + self.pars['eff_charge']*p_in[i]/3600*dt_int
                if x[i+1] > self.pars['E_max']:
                    x[i + 1] = self.pars['E_max']
                    p[i] = (x[i + 1]-x[i]) / self.pars['eff_charge']*3600/dt_int
                else:
                    p[i] = p_in[i]
            elif p_out[i] > 0:
                x[i + 1] = x[i] - p_out[i]/3600*dt_int/self.pars['eff_discharge']
                if x[i+1] < self.pars['E_min']:
                    x[i + 1] = self.pars['E_min']
                    p[i] = (x[i]-x[i + 1]) * self.pars['eff_discharge']*3600/dt_int
                else:
                    p[i] = -p_out[i]
            else:
                x[i + 1] = x[i]

            # see if max SOC was reached before accounting for self discharge
            self.reached_e_max = True if x[i + 1] == self.pars['E_max'] else False

            # take into account self-discharge, at the end of the operations. Prevent energy from being negative
            if x[i + 1] > self.dt * x[i + 1]/self.pars['tau_sd']:
                x[i + 1] -= self.dt * x[i + 1]/self.pars['tau_sd']

        # save the state and time
        q = np.sqrt(1-self.descriptor['pars']['cos_phi']**2)/self.descriptor['pars']['cos_phi'] * p

        p_all = np.zeros((n_steps, 3))
        q_all = np.zeros((n_steps, 3))

        for phase in self.phases:
            p_all[:, self.phases_num[phase]] = p[:-1] / self.num_phases
            q_all[:, self.phases_num[phase]] = q[:-1] / self.num_phases

        self.state = x[-1]
        self.time = t_end

        # write on pq_buffer
        self.write_pq_buffer(p_all, q_all, t_start, t_end)

        # write data on internal buffer
        buffer_dict = {'p_tot': p[:-1],
                       'q_tot': q[:-1],
                       'p_a': p_all[:, 0],
                       'p_b': p_all[:, 1],
                       'p_c': p_all[:, 2],
                       'q_a': q_all[:, 0],
                       'q_b': q_all[:, 1],
                       'q_c': q_all[:, 2],
                       'SOC': self.state,
                       'p_main': P_m,
                       'p_main_no_batt': P_m - self.Pb_old,
                       'force_off': force_off,
                       'p_set': p_set,
                       'plug_state': self.plug_state.loc[self.time].values,
                       'available_steps': self.available_steps.loc[self.time].values
                       }

        # write in the current buffer
        times = np.arange(t_start, t_end + self.dt, self.dt)
        self.increment_buffer(buffer_dict, times)

        self.Pb_old = p[-2]

        return p, self.state


    def plots(self, y, p_real, p_set):
        """
        Show the plots of energy, setpoint and measurement power

        :param y: Energy [kWh]
        :param p_real: Measured power [W]
        :param p_set: Setpoint [W]
        :type y: Numpy array
        :type p_real: Numpy array
        :type p_set: Numpy array
        """
        fig = plt.gcf()
        fig.clear()
        #fig = plt.figure()
        ax = fig.add_subplot(211)
        plt.hlines(self.pars['E_min'], 0, len(y), label='Emin')
        plt.hlines(self.pars['E_max'], 0, len(y), 'g', label='Emax')
        ax.plot(y, '-', label='E')
        plt.ylabel('E [kWh]')
        plt.xlabel('steps')

        plt.legend()
        ax.grid()

        ax = fig.add_subplot(212)
        plt.hlines(self.pars['p_max_charge'], 0, len(y), label='P_in_max')
        plt.hlines(-self.pars['p_max_discharge'], 0, len(y), 'g', label='P_out_max')
        ax.plot(p_set, '-', label='P_set')
        ax.plot(p_real, '-', label='P_real')
        plt.ylabel('P [W]')
        plt.xlabel('steps')
        plt.legend()
        ax.grid()
        plt.pause(0.001)
        fig.canvas.draw()
        fig.canvas.flush_events()

    def from_DatetimeIndex_to_unix_timestamp(self, t:pd.DatetimeIndex):
        return np.array([i.replace(tzinfo=timezone.utc).timestamp() for i in pd.to_datetime(t)])

    def build_ev_datasets(self, t_start, t_end):
        """
        Build the SOC and plug status datasets in unix timestamp
        :param t_start: unix timestamp
        :param t_end: unix timestamp
        :return: plug_state: if the car is available at the designed location.
                 available_steps: steps left before next departure

        """

        data_ts = pd.DataFrame(int(0), columns=['plug_state', 'available_steps'],
                                  index=np.arange(t_start,t_end+1, self.dt))

        location = self.descriptor['location']
        is_in_place = self.influx_data['location'] == location
        cumsum = np.cumsum(is_in_place.values)
        km = np.bincount(np.hstack([0, cumsum[:-1]]), weights=self.influx_data['km'])

        kWh_used = pd.DataFrame(km[0:np.sum(is_in_place)] * self.pars['kwh_per_km'], index=self.influx_data.index[is_in_place], columns=['kWh_used'])

        influx_data = self.influx_data.loc[self.influx_data['location'] == location].copy(deep=True)
        influx_data.loc[:, 'departure_time_ts'] = influx_data['departure_time_ts']/1e9

        for i in influx_data.index:
            ts_arr = i
            ts_dep = influx_data.loc[i, 'departure_time_ts']
            index = (ts_arr - self.simulation_start_ts < data_ts.index + self.dt/2) &\
                    (data_ts.index + self.dt/2 <= ts_dep - self.simulation_start_ts)
            data_ts.loc[index, 'plug_state'] = int(1)
            data_ts.loc[index, 'available_steps'] = np.flip(np.arange(index.sum()))

        return data_ts[['plug_state']], data_ts[['available_steps']], kWh_used
