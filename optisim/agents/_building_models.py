# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #
from numba import jit, float64


# --------------------------------------------------------------------------- #
# Functions
# --------------------------------------------------------------------------- #
def pars_builder_default(k1: float=1e-3,
                         k2: float=1e-3,
                         k3: float=1e-3,
                         zone_capacitance: float=10*3600,
                         dtemp_hys: float=0.5,
                         ma_days: float=7,
                         Tmin_ma: float=15,
                         Tmax_ma: float=25,
                         shade_ratio: float=0.9):
    """
    Create a dictionary containing the needed building parameters
    :param k1: influence of internal temperature
    :type k1: float
    :param k2: influence of external temperature
    :type k2: float
    :param k3: influence of irradiation
    :type k3: float
    :param zone_capacitance: capacitance of the zone [kWh/K]
    :type zone_capacitance: float
    :param dtemp_hys: delta T for hysteresys control [K]
    :type dtemp_hys: float
    :return: building parameters
    :rtype: dict
    """
    return {'k1': k1,
            'k2': k2,
            'k3': k3,
            'C': zone_capacitance,
            'dT_hys': dtemp_hys,
            'ma_days': ma_days,
            "Tmin_ma": Tmin_ma,
            "Tmax_ma": Tmax_ma,
            "shade_ratio": shade_ratio
            }


def pars_builder_1s(walls_thermal_resistance: float=47,
                    area_thermal_resistance: float=5e10,
                    windows_thermal_resistance: float=4.75,
                    alpha: float=0.7,
                    walls_ucoeff: float=5e-2,
                    zone_capacitance: float=10.51*3600,
                    south_area_windows: float=1e-3,
                    rho_air: float=1.24,
                    cp_air: float=1.005,
                    m_vent: float=250*0.15*1e-3,
                    heater: float='electric',
                    q_nom: float=6,
                    dtemp_hys: float=0.5,
                    ma_days:float=7,
                    Tmin_ma:float=15,
                    Tmax_ma:float=25,
                    shade_ratio: float = 0.9):
    """
    Building pars builder for a one-state building. The model and the default parameters are taken from "Towards 100%
    renewable energy supply for urban areas and the role of smart control, Richard Pieter van Leeuwen, 2017".
    :param walls_thermal_resistance: [K/kW] thermal resistance of insulated walls
    :type walls_thermal_resistance: float
    :param area_thermal_resistance: [m2K/kW]  area specific thermal resistance
    :type area_thermal_resistance: float
    :param windows_thermal_resistance: [K/kW] thermal resistance of windows
    :type windows_thermal_resistance: float
    :param alpha: [-] external wall solar absorptance
    :type alpha: float
    :param walls_ucoeff: [kW/m2/K] exterior wall surface heat transfer coefficient
    :type walls_ucoeff: float
    :param zone_capacitance: capacitance of the zone [kWh/K]
    :type zone_capacitance: float
    :param south_area_windows: [m2] effective window area on South side of the building
    :type south_area_windows: float
    :param rho_air: [kg/m3] air specific density
    :type rho_air: float
    :param cp_air: [kj/kg/K] air specific heat
    :type cp_air: float
    :param m_vent: [kg/s] ventilation, constant rate
    :type m_vent: float
    :param heater: string, type of heating element, in {heat_pump, electric}
    :type heater: float
    :param q_nom: [kW] nominal heating power of the heating element
    :type q_nom: float
    :param dtemp_hys: [K] delta T for the hysteresis control of the heating system
    :type dtemp_hys: float
    :return: building parameters
    :rtype: dict
    """
    return {'R': walls_thermal_resistance,
            'R_s': area_thermal_resistance,
            'R_w': windows_thermal_resistance,
            'alpha': alpha,
            'Uwall': walls_ucoeff,
            'C': zone_capacitance,
            'Aw': south_area_windows,
            'rho_air': rho_air,
            'cp_air': cp_air,
            'm_vent': m_vent,
            'heater': heater,
            'Qnom': q_nom,
            'dT_hys': dtemp_hys,
            'ma_days': ma_days,
            "Tmin_ma": Tmin_ma,
            "Tmax_ma": Tmax_ma,
            "shade_ratio": shade_ratio
            }


def pars_builder_2s(walls_thermal_resistance: float=10.59,
                    area_thermal_resistance: float=5000,
                    windows_thermal_resistance: float=47.5,
                    alpha: float=0.7,
                    walls_ucoeff: float=5e-3,
                    zone_capacitance: float=7.68*3600,
                    south_area_windows: float=8.91,
                    rho_air: float=1.24,
                    cp_air: float=1.005,
                    m_vent: float=250*0.15*1e-3,
                    heater: str='electric',
                    q_nom: float=5,
                    dtemp_hys: float=0.5,
                    floor_resistance: float=0.996,
                    floor_capacitance: float=9.08*3600,
                    ma_days:float=7,
                    Tmin_ma:float=15,
                    Tmax_ma:float=25,
                    shade_ratio: float=0.9
                    ):
    """
    Building pars builder for a one-state building. The model and the default parameters are taken from "Towards 100%
    renewable energy supply for urban areas and the role of smart control, Richard Pieter van Leeuwen, 2017".
    :param walls_thermal_resistance: [K/kW] thermal resistance of insulated walls
    :type walls_thermal_resistance: float
    :param area_thermal_resistance: [m2K/kW]  area specific thermal resistance
    :type area_thermal_resistance: float
    :param windows_thermal_resistance: [K/kW] thermal resistance of windows
    :type windows_thermal_resistance: float
    :param alpha: [-] external wall solar absorptance
    :type alpha: float
    :param walls_ucoeff: [kW/m2/K] exterior wall surface heat transfer coefficient
    :type walls_ucoeff: float
    :param zone_capacitance: [kWh/K] capacitance of the zone
    :type zone_capacitance: float
    :param south_area_windows: [m2] effective window area on South side of the building
    :type south_area_windows: float
    :param rho_air: [kg/m3] air specific density
    :type rho_air: float
    :param cp_air: [kj/kg/K] air specific heat
    :type cp_air: float
    :param m_vent: [kg/s] ventilation, constant rate
    :type m_vent: float
    :param heater: string, type of heating element, in {heat_pump, electric}
    :type heater: float
    :param q_nom: [kW] nominal heating power of the heating element
    :type q_nom: float
    :param dtemp_hys: [K] delta T for the hysteresis control of the heating system
    :type dtemp_hys: float
    :param floor_resistance: [K/kW] floor resistance
    :type floor_resistance: float
    :param floor_capacitance: [kWh/K] capacitance of the floor
    :type floor_capacitance: float
    :return: building parameters
    :rtype: dict
    """
    pars = {'R': walls_thermal_resistance,
            'R_s': area_thermal_resistance,
            'R_w': windows_thermal_resistance,
            'alpha': alpha,
            'Uwall': walls_ucoeff,
            'C': zone_capacitance,
            'Aw': south_area_windows,
            'rho_air': rho_air,
            'cp_air': cp_air,
            'm_vent': m_vent,
            'heater': heater,
            'Qnom': q_nom,
            'dT_hys': dtemp_hys,
            'R_f': floor_resistance,
            'C_f': floor_capacitance,
            'ma_days': ma_days,
            "Tmin_ma": Tmin_ma,
            "Tmax_ma": Tmax_ma,
            "shade_ratio": shade_ratio
            }
    return pars


@jit(float64[:](float64[:], float64, float64, float64, float64, float64, float64, float64, float64), nopython=True)
def ode_default(x: float,
                Qint: float,
                Qs: float,
                Qheating: float,
                Ta: float,
                k1: float,
                k2: float,
                k3: float,
                C: float):
    """
    Default ODE
    :param x:
    :param Qint:
    :param Qs:
    :param Qheating:
    :param Ta:
    :param k1:
    :param k2:
    :param k3:
    :param C:
    :return:
    """
    dx = ((Qint+Qheating+k1*x+k2*Ta+k3*Qs) / C)
    return dx


# @jit(nopython=True)
def ode_1s(x: float,
           Qint: float,
           Qs: float,
           Qheating: float,
           Ta: float,
           m_vent: float,
           R: float,
           R_s: float,
           R_w: float,
           alpha: float,
           Uwall: float,
           C: float,
           Aw: float,
           rho_air: float,
           cp_air: float):
    """
    1-state ODE
    :param x:
    :param Qint:
    :param Qs:
    :param Qheating:
    :param Ta:
    :param m_vent:
    :param R:
    :param R_s:
    :param R_w:
    :param alpha:
    :param Uwall:
    :param C:
    :param Aw:
    :param rho_air:
    :param cp_air:
    :return:
    """
    Tsa = (x+R_s*(alpha*Qs+Uwall*Ta))/(1+Uwall*R_s)
    Qvent = m_vent*rho_air*cp_air*(Ta-x)
    Qwalls = (Tsa - x)/R
    Qwindows = (Ta - x)/R_w
    dx = ((Qwalls+Qwindows+Qvent+Aw*Qs+Qint+Qheating) / C)
    return dx


# @jit(nopython=True)
def ode_2s(x: float,
           Qint: float,
           Qs: float,
           Qheating: float,
           Ta: float,
           m_vent: float,
           R: float,
           R_s: float,
           R_w: float,
           alpha: float,
           Uwall: float,
           C: float,
           Aw: float,
           rho_air: float,
           cp_air: float,
           R_f: float,
           C_f: float):
    """
    2-states ODE
    :param x:
    :param Qint:
    :param Qs:
    :param Qheating:
    :param Ta:
    :param m_vent:
    :param R:
    :param R_s:
    :param R_w:
    :param alpha:
    :param Uwall:
    :param C:
    :param Aw:
    :param rho_air:
    :param cp_air:
    :param R_f:
    :param C_f:
    :return:
    """
    tsa = (x+R_s*(alpha*Qs+Uwall*Ta))/(1+Uwall*R_s)
    q_vent = m_vent*rho_air*cp_air*(Ta-x)
    q_walls = (tsa - x[1])/R
    q_windows = Aw*(Ta - x[1])/R_w
    q_floor = (x[2]-x[1])/R_f
    dx = []
    dx[1] = ((q_floor + q_walls + q_windows + q_vent + Aw*Qs+Qint) / C)
    dx[2] = (Qheating - q_floor)/C_f
    return dx


def heating_model_default(x: float,
                          t: float,
                          Qint: float,
                          Qs: float,
                          Qheating: float,
                          Ta: float,
                          shading: float,
                          building_pars: float):
    """
    Default heating model
    :param x:
    :param t:
    :param Qint:
    :param Qs:
    :param Qheating:
    :param Ta:
    :param building_pars:
    :return:
    """
    return ode_default(x=x,
                       Qint=Qint,
                       Qs=Qs,
                       Qheating=Qheating,
                       Ta=Ta,
                       k1=building_pars['k1'],
                       k2=building_pars['k2'],
                       k3=(1-shading*building_pars['shade_ratio'])*building_pars['k3'],
                       C=building_pars['C'])


def heating_model_1s(x: float,
                     t: float,
                     Qint: float,
                     Qs: float,
                     Qheating: float,
                     Ta: float,
                     building_pars: float):
    """
    1-state heating model
    :param x:
    :param t:
    :param Qint:
    :param Qs:
    :param Qheating:
    :param Ta:
    :param building_pars:
    :return:
    """
    return ode_1s(x=x,
                  Qint=Qint,
                  Qs=Qs,
                  Qheating=Qheating,
                  Ta=Ta,
                  m_vent=building_pars['m_vent'],
                  R=building_pars['R'],
                  R_s=building_pars['R_s'],
                  R_w=building_pars['R_w'],
                  alpha=building_pars['alpha'],
                  Uwall=building_pars['Uwall'],
                  C=building_pars['C'],
                  Aw=building_pars['Aw'],
                  rho_air=building_pars['rho_air'],
                  cp_air=building_pars['cp_air'])


def heating_model_2s(x: float,
                     t: float,
                     Qint: float,
                     Qs: float,
                     Qheating: float,
                     Ta: float,
                     building_pars: float):
    """
    2-states heating model
    :param x:
    :param t:
    :param Qint:
    :param Qs:
    :param Qheating:
    :param Ta:
    :param building_pars:
    :return:
    """
    return ode_2s(x=x,
                  Qint=Qint,
                  Qs=Qs,
                  Qheating=Qheating,
                  Ta=Ta,
                  m_vent=building_pars['m_vent'],
                  R=building_pars['R'],
                  R_s=building_pars['R_s'],
                  R_w=building_pars['R_w'],
                  alpha=building_pars['alpha'],
                  Uwall=building_pars['Uwall'],
                  C=building_pars['C'],
                  Aw=building_pars['Aw'],
                  rho_air=building_pars['rho_air'],
                  cp_air=building_pars['cp_air'],
                  R_f=building_pars['R_f'],
                  C_f=building_pars['C_f'])
