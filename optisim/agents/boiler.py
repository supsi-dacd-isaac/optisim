# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #
from time import time as time

import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from .heat_pump import HeatPump
from .agent import Agent


# --------------------------------------------------------------------------- #
# Functions
# --------------------------------------------------------------------------- #

def boiler_pars_builder(cp=4.182, heating_layers=[0, 1], sensor_layer=2, n_layers=6, T_in=10, Text=20,
                        k_amb=1.12*1e-3, lamb=(0.615+1.635)*1e-3, Tmin=40, Tmax=10, Qnom=1, T_0=25, dT_hys=1,
                        v_boiler=1, rho=1e3, h_boiler=None):
    """ Build boiler dictionary, with default values
    :param cp: specific heat of medium (water) [kJ/kg/K]
    :type cp: float
    :param heating_layers: number of layer with the heating resistance, from the bottom [-]
    :type heating_layers: list
    :param sensor_layer: number of layer with the temperature sensor, from the bottom [-]
    :type sensor_layer: int
    :param n_layers: number of layers [-]
    :type n_layers: int
    :param T_in: inlet temperature of the water, constant [K]
    :type T_in: float
    :param Text: external temperature (ambient), constant [K]
    :type Text: float
    :param k_amb: equivalent coefficient for heat dissipation into the ambient Qa = A*k_amb*(Text-T) [kW/K]
    :type k_amb: float
    :param lamb: thermal conductivity of water [kW/m/K]
    :type lamb: float
    :param Tmin: minimum temperature for hysteresis control [K]
    :type Tmin: float
    :param Tmax: maximum temperature for hysteresis control (in case of heat pump cooling) [K]
    :type Tmax: float
    :param Qnom: nominal power of the electric resistance [W]
    :type Qnom: float
    :param T_0: initial temperature of the layers [K]
    :type T_0: float
    :param dT_hys: temperature difference for the hysteresis control [K]
    :type dT_hys: float
    :param v_boiler: volume of the boiler [m^3]
    :type v_boiler: float
    :param rho: density of the medium (water) [kg/m^3]
    :type rho: float
    :param h_boiler: height of the boiler [m]
    :type h_boiler: float
    :return: pars dictionary
    :rtype: dict
    """

    if h_boiler is None:
        h_boiler = (4*v_boiler)**(1/3)
    A_base = v_boiler / h_boiler
    h_layer = h_boiler / n_layers
    r = np.sqrt(A_base/np.pi)
    A_layer = 2 * np.pi * r * h_layer
    pars = {'cp': cp,
            'heating_layers': heating_layers,
            'sensor_layer': sensor_layer,
            'n_layers': n_layers,
            'T_in': T_in,
            'Text': Text,
            'k_amb': k_amb,
            'Tmin': Tmin,
            'Tmax': Tmax,
            'Qnom': Qnom,
            'T_0': T_0,
            'dT_hys': dT_hys,
            'v_boiler': v_boiler,
            'v_layer': v_boiler / n_layers,  # volume of a single layer
            'rho': rho,
            'c_layer': rho * cp * v_boiler / n_layers,  # thermal capacity of each layer
            'h_layer': h_layer,
            'A_base': A_base,
            'A_layer': A_layer,
            'u_lam': lamb * A_base / h_layer,
            'u_amb': A_layer*k_amb}
    return pars


def sim_pars_builder(do_plots=False, data_source='.'):
    """
    :param do_plots: if true, plot states and variables at the end of the simulations
    :param data_source: data source for the simulation, as a local or http address
    :return:
    """
    pars = {'do_plots': do_plots}
    return pars


#@jit(float64[:](float64, float64, float64, float64[:], float64[:], float64, float64[:], float64[:], float64[:],
#               float64[:], float64), nopython=True)
def quick_ode_boiler(u_amb, u_lam, Text, X, Qh, cp, m, T_lower, T_upper, T_inlet, C_layer):
    """ Quick ODE function solver (not using time)
    :param u_amb: equivalent thermal coefficient for ambient thermal losses [W/K]
    :type u_amb: float
    :param u_lam: equivalent thermal coefficient for laminar conduction between layers [W/K]
    :type u_lam: float
    :param Text: external temperature (ambient) [K]
    :type Text: float
    :param X: state vector
    :type X: list
    :param Qh: thermal power from the electrical resistance (controlled by hysteresis) for each level
    :type Qh: float
    :param cp: specific thermal capacity of the medium (water) [kJ/kg/K]
    :type cp: float
    :param m: inlet mass flow [kg/s]
    :type m: float
    :param T_upper: upper temperature [K]
    :type T_upper: float
    :param T_lower: lower temperature [K]
    :type T_lower: float
    :param T_inlet: inlet temperature [K]
    :type T_inlet: floatnt
    :param C_layer: thermal capacity of the layers [kJ/K]
    :type C_layer: float
    :return: ODE result
    :rtype: list
    """
    q_buo = 0.024*np.maximum(T_lower-X, 0)*len(X)**1.5
    q_buo_out = q_buo.take(np.concatenate((np.arange(1, len(X)), np.arange(0, 1))))

    q_loss = u_amb * (Text - X)
    qcond_up = u_lam * (T_upper - X)
    qcond_down = u_lam * (T_lower - X)
    dX = ((q_buo - q_buo_out + Qh + q_loss + qcond_up + qcond_down + cp * m * (T_inlet - X)) / C_layer)
    return dX


def boiler_model_odeint(X, t, Qheater, m, T_inlet, u_amb, u_lam, n_layers, heating_layers, Text, C_layer, cp):
    """ ODE function solver
    :param X: state vector
    :type X: list
    :param t: time
    :type t: int
    :param Qheater: thermal power from the electrical resistance (controlled by hysteresis)
    :type Qheater: float
    :param m: inlet mass flow [kg/s]
    :type m: float
    :param T_in: inlet temperature [K]
    :type T_in: float
    :param u_amb: equivalent thermal coefficient for ambient thermal losses [W/K]
    :type u_amb: float
    :param u_lam: equivalent thermal coefficient for laminar conduction between layers [W/K]
    :type u_lam: float
    :param n_layers: number of layers
    :type n_layers: int
    :param heating_layers: index of the heating layer (bottom layer = 0)
    :type heating_layers: int
    :param Text: external temperature (ambient) [K]
    :type Text: float
    :param C_layer: thermal capacity of the layers [kJ/K]
    :type C_layer: float
    :param cp: specific thermal capacity of the medium (water) [kJ/kg/K]
    :type cp: float
    :return: ODE result
    :rtype: list
    """
    Qh = np.zeros(n_layers)
    Qh[heating_layers] = Qheater
    temp_lower = np.insert(X[:-1], 0, X[0])
    temp_in = np.copy(temp_lower)
    temp_in[0] = T_inlet
    temp_upper = np.insert(X[1:], len(X[1:]), X[-1])
    u_amb_destrat = u_amb * np.ones(len(X))
    u_amb_destrat[-1] *= 0.99
    dX = quick_ode_boiler(u_amb_destrat, u_lam, Text, X, Qh, cp, m, temp_lower, temp_upper, temp_in, C_layer)
    return dX

# --------------------------------------------------------------------------- #
# Classes
# --------------------------------------------------------------------------- #

class Boiler(Agent):
    def __init__(self,
                 data_manager,
                 broker_producer,
                 descriptor=None,
                 heating_element=None,
                 pars=None,
                 sim_pars=sim_pars_builder(),
                 state=None,
                 meter=None,
                 **_kwargs):
        """ Constructor
        :param data_manager: object able to get data from InfluxDB and maintain them internally
        :type data_manager: classes.utils.input_data_manager.InputDataManager
        :param broker_producer: broker producer used to send message to the configured broker broker
        :type broker_producer: optisim.utils.rabbit_sim_producer.RabbitSimulationProducer object
        :param descriptor: dictionary containing all the configuration settings needed by Agent
        :type descriptor: dict
        :param heating_element: list of variables whose values will be saved in InfluxDB
        :type heating_element: optisim.agents.electric_heater.Heater object
        :param pars: dictionary containing all the boiler parameters
        :type pars: dict
        :param sim_pars: dictionary containing all the simulation parameters
        :type sim_pars: dict
        :param state: dictionary containing all the simulation parameters
        :type state: numpy array
        :param meter: meter object
        :type meter: optisim.agents.meter.Meter object
        """
        # set the datamanager
        self.data_manager = data_manager
        if pars is None:
            boiler_pars = boiler_pars_builder()
        else:
            boiler_pars = boiler_pars_builder(cp=pars['cp'], heating_layers=pars['heating_layers'],
                                              sensor_layer=pars['sensor_layer'], n_layers=pars['n_layers'],
                                              T_in=pars['T_in'], Text=pars['Text'],k_amb=pars['k_amb'],
                                              lamb=pars['lamb'], Tmin=pars['Tmin'],Tmax=pars['Tmax'], Qnom=pars['Qnom'],
                                              T_0=pars['T_0'], dT_hys=pars['dT_hys'],v_boiler=pars['v_boiler'],
                                              rho=pars['rho'],
                                              h_boiler=(pars['h_boiler'] if ('h_boiler' in pars.keys()) else None))

        # check that all the parameters are present
        pars_list = {'n_layers','Qnom','dT_hys','c_layer','T_in','v_boiler','rho','u_amb','T_0','A_layer','u_lam',
                     'Tmin','cp','sensor_layer','A_base','v_layer','k_amb','h_layer','Text','heating_layers'}
        assert all([k in boiler_pars.keys() for k in pars_list]), "ERROR: some keys are not present in boiler_pars"
        sim_pars_list = {'do_plots'}
        assert all([k in sim_pars.keys() for k in sim_pars_list]), "ERROR: some keys are not present in sim_pars"

        self.pars = pars
        self.boiler_pars = boiler_pars
        self.sim_pars = sim_pars

        # initialize internal states (equal to external temperature if not specified otherwise)
        if state is None:
            self.state = boiler_pars['T_0']*np.ones(boiler_pars['n_layers'])
        else:
            assert len(state) == boiler_pars['n_layers'], "state must have boiler_pars['n_layers'] entries"
            self.state = state

        # initialize the temperature of the heating source
        self.T_s = self.state[0]

        # -----------------------
        # initialize hysteresis state and time
        # -----------------------

        self.hysteresis_state = 0
        self.time = 0

        # define available variables
        available_vars = ['q_heating', 'hy_state', 'm_flow', 'T_source', 'working_mode']
        for i in np.arange(len(self.state)):
            available_vars.append('state_' + str(i))

        super().__init__(data_manager=data_manager, broker_producer=broker_producer, descriptor=descriptor,
                         available_vars=available_vars, meter=meter)

        for i in np.arange(len(self.state)):
            self.state_vars.append('state_' + str(i))
        self.state_vars.append('T_source')
        self.state_vars.append('hy_state')

        # init from db
        if self.is_load_states_from_db:
            states = dict()
            for i in np.arange(len(self.state)):
                states.update({'state[{}]'.format(i): {'tags': {'signal': 'state_' + str(i)}}})
            states.update({'T_s': {'tags': {'signal': 'T_source'}}})
            states.update({'hysteresis_state': {'tags': {'signal': 'hy_state'}}})
            self.load_states_from_db(states)

        if heating_element is not None:
            self.heating_element = heating_element




    '''   
    def heating_hysteresis(self, T, previous_state, Tmin, DT):
        """ Hysteresis control for heating
        :param T: temperature of the sensor [K]
        :type T: float
        :param previous_state: previous state of the hysteresis control
        :type previous_state: numpy array
        :param Tmin: minimum temperature for the hysteresis [K]
        :type Tmin: float
        :param DT: delta T for the hysteresis [K]
        :type DT: float
        :return: state of the hysteresis
        :type: int
        """
        if T < Tmin:
            state = 1
        elif T > Tmin and T < Tmin + DT and previous_state > 0:
            state = 1
        else:
            state = 0
        return state

    def cooling_hysteresis(self, T, previous_state, Tmax, DT):
        """ Hysteresis control for cooling
        :param T: temperature of the sensor [K]
        :type T: float
        :param previous_state: previous state of the hysteresis control
        :type previous_state: numpy array
        :param Tmin: maximum temperature for the hysteresis [K]
        :type Tmin: float
        :param DT: delta T for the hysteresis [K]
        :type DT: float
        :return: state of the hysteresis
        :type: int
        """
        if T > Tmax:
            state = -1
        elif T < Tmax and T > Tmax - DT and previous_state < 0:
            state = -1
        else:
            state = 0
        return state
 '''
    def heating_hysteresis(self, T,T_low, previous_state, Tmin, DT):
        """ Hysteresis control for heating
        :param T: temperature of the sensor [K]
        :type T: float
        :param previous_state: previous state of the hysteresis control
        :type previous_state: numpy array
        :param Tmin: minimum temperature for the hysteresis [K]
        :type Tmin: float
        :param DT: delta T for the hysteresis [K]
        :type DT: float
        :return: state of the hysteresis
        :type: int
        """
        # if T_low < Tmin-DT/2:
        #     state = 1
        # elif T > Tmin +DT/2:
        #     state = 0
        # else:
        #     state = previous_state
        # return state

        if T < Tmin - DT/2:
            state = 1
        elif T > Tmin + DT/2:
            state = 0
        else:
            state = previous_state
        return state

    def cooling_hysteresis(self, T, T_low, previous_state, Tmax, DT):
        """ Hysteresis control for cooling
        :param T: temperature of the sensor [K]
        :type T: float
        :param previous_state: previous state of the hysteresis control
        :type previous_state: numpy array
        :param Tmin: maximum temperature for the hysteresis [K]
        :type Tmin: float
        :param DT: delta T for the hysteresis [K]
        :type DT: float
        :return: state of the hysteresis
        :type: int
        """
        if T_low < 5 or T < Tmax-DT/2:
            state = 0
        elif T > Tmax +DT/2 or T_low > Tmax +DT/2:
            state = -1
        else:
            state = previous_state
        return state

    def simulate(self, m_flow=None, working_mode=None, t_end=15*60, t_start=0, T_in=None):
        """ Perform a simulation step
        :param m_flow: inlet mass flow
        :type m_flow: float
        :param working_mode: if present, overwrite the heating control
        :type working_mode: int
        :param t_end: final time of simulation
        :type t_end: int
        :param dt: discretization time (timestep)
        :type t_end: int
        :param t_start: starting time of simulation, default = 0
        :type t_end: int
        :param T_in: inlet temperature of the flow
        :type T_in: numpy ndarray
        :return: q_hy, injected power by the electric resistance
        :rtype: numpy ndarray
        """
        self.heat_exchanger = isinstance(self.heating_element, HeatPump)
        if self.heat_exchanger:
            Q_nom = self.heating_element.q_nom
            DT_nom = 20  # nominal DT between mean HEX fluid temperature and mean tank fluid temperature
            self.UA_i = Q_nom/DT_nom/len(self.boiler_pars['heating_layers'])
        else:
            self.UA_i = None

        if working_mode is None:
            working_mode = 1
        else:
            assert working_mode in [1, 0, -1], 'Error: boiler working mode must be 1 or -1'

        if m_flow is None:
            fields = self.descriptor['fields']
            tag = {'type': self.descriptor['type'],
                   'name': self.descriptor['profile']}
            aggr_fun = 'sum'
            name = self.descriptor['name']
            interval = self.dt
            m_flow = self.data_manager.get_simulation_dataset(t_start, t_end, name, fields, tag, interval, aggr_fun)
            m_flow = m_flow[self.descriptor['fields']].values.reshape(-1, 1) / self.dt

        dt = self.dt
        t_end += dt
        n_steps = int((t_end - t_start) / dt)
        y_sol = np.zeros((n_steps+1,len(self.state)))

        y_sol[0, :] = self.state

        # initialize q hysteresis vector, rename parameters for speed up (in case more than one step is solved)
        q_heating = np.zeros(n_steps+1)  # incoming thermal power into the building, due to heating system
        hy_state = np.zeros(n_steps+1,'int')  # internal building temperature hysteresis state
        hy_state[0] = self.hysteresis_state  # set hysteresis state to the internal hysteresis state

        if T_in is None:
            T_in = np.ones((n_steps,1))
            T_in = T_in*self.boiler_pars['T_in']

        sensor_layer = self.boiler_pars['sensor_layer']
        Tmin = self.boiler_pars['Tmin']
        Tmax = self.boiler_pars['Tmax']
        dT_hys = self.boiler_pars['dT_hys']
        u_amb = self.boiler_pars['u_amb']
        u_lam = self.boiler_pars['u_lam']
        n_layers = self.boiler_pars['n_layers']
        heating_layer = self.boiler_pars['heating_layers'][0]
        heating_layers = self.boiler_pars['heating_layers']
        n_heating_layers = len(self.boiler_pars['heating_layers'])
        Text = self.boiler_pars['Text']
        c_layer = self.boiler_pars['c_layer']
        cp = self.boiler_pars['cp']
        t0 = time()
        i = 0

        dt_int = int(dt)
        for current_time in np.arange(t_start, t_end, dt_int):
            # compute the incoming power if needed
            if self.backward_summoning:
                if working_mode == 1:
                    # compute the state of the hysteresis
                    t_low = np.min(y_sol[i][heating_layer])
                    hy_state[i + 1] = self.heating_hysteresis(y_sol[i][sensor_layer],t_low, hy_state[i], Tmin,
                                                              dT_hys)
                    if hy_state[i] == 1:
                        q_heating[i + 1], T_source = self.heating_element.get_q(y_sol[i,heating_layer],
                                                                                hy_state[i],current_time)
                elif working_mode ==-1:
                    # compute the state of the hysteresis
                    t_low = np.min(y_sol[i][heating_layer])
                    hy_state[i + 1] = self.cooling_hysteresis(y_sol[i][sensor_layer],t_low, hy_state[i], Tmax,
                                                              dT_hys)
                    if hy_state[i+1] == 1:
                        q_heating[i + 1], T_source = self.heating_element.get_q(y_sol[i, heating_layer],
                                                                                hy_state[i],current_time)
                else:
                    q_heating[i + 1] = 0

            else:
                if working_mode == 1:
                    # compute the state of the hysteresis
                    t_low = np.min(y_sol[i][heating_layer])
                    hy_state[i + 1] = self.heating_hysteresis(y_sol[i][sensor_layer],t_low, hy_state[i], Tmin,
                                                              dT_hys)
                elif working_mode == -1:
                    # compute the state of the hysteresis
                    t_low = np.min(y_sol[i][heating_layer])
                    hy_state[i + 1] = self.cooling_hysteresis(y_sol[i][sensor_layer],t_low, hy_state[i], Tmax,
                                                              dT_hys)

                q_heating[i + 1],T_source = self.heating_element.get_q(self.T_s, hy_state[i],current_time)

            # if the heating element of the boiler is an heatpump, compute the mean temperature inside the heat
            # exchanger needed to dissipate Q_heating
            if self.heat_exchanger:
                self.T_s = q_heating[i + 1]/(n_heating_layers*self.UA_i) + np.sum(y_sol[i, heating_layers])/n_heating_layers
                q_hl = self.UA_i*(self.T_s-y_sol[[i], heating_layers])
            else:
                q_hl = q_heating[i + 1]*np.ones(n_heating_layers) / n_heating_layers
                self.T_s = y_sol[i, heating_layer]


            # set parameters
            #ode.set_f_params(q_heating[i+1], m_flow[i], T_in[i], u_amb, u_lam,n_layers,heating_layer, Text, c_layer, cp)
            y = odeint(boiler_model_odeint, y0=y_sol[i, :], t=(current_time, current_time + dt_int),
                       args=(q_hl, m_flow[i], T_in[i], u_amb, u_lam, n_layers, heating_layers, Text, c_layer,
                             cp))

            # save solution
            y_sol[i+1,:] = y[1,:]
            i += 1

        elapsed_time = time() - t0

        # save the state and time
        if len(y_sol)>1:
            y = np.vstack(y_sol[1:])
            q_heating = q_heating[1:].reshape(-1, 1)
        else:
            y = np.atleast_1d(y_sol)
            q_heating = np.atleast_1d(q_heating)
        self.state = y[-1]
        self.hysteresis_state = hy_state[-1]
        self.time = t_end

        ##################################
        # write data on internal buffer
        ##################################

        buffer_dict = {'q_heating': q_heating,
                       'hy_state': hy_state[1:],
                       'm_flow': m_flow,
                       'T_source': self.T_s,
                       'working_mode': np.array(working_mode)}

        for i in np.arange(y.shape[1]):
            state_str = 'state_'+str(i)
            buffer_dict[state_str] = y[:,i]

        # write in the current buffer
        times = np.arange(t_start, t_end, dt)
        self.increment_buffer(buffer_dict, times)

        return q_heating, self.state[-1]

    def get_q(self, T_in, control_state, time, m_flow_input=None):
        """ Get Q of the boiler
        :param T_in: numpy ndarray vector. Inlet temperature of the flow
        :type T_in: numpy ndarray
        :param control_state: scalar, one in {1,-1}.
        :type control_state: int
        :param time: simulation time
        :type time: int
        :param m_flow_input: mass flow
        :type m_flow_input: float
        :return: heating Q
        :rtype: numpy ndarray
        :return: temperature at which the heat for the useful effect is exchanged
        :rtype: float
        """
        t_start = self.time
        t_end = time + self.dt
        if t_end < t_start:
            return 0, self.state[-1]

        n_steps = int((t_end-t_start)/self.dt)
        m_flow = np.zeros((n_steps,1))
        if control_state != 0:
            m_flow[-1] = m_flow_input
        T_in_vect = np.zeros((n_steps, 1))
        T_in_vect[-1] = T_in        # the pump is activated only in the last step
        # in case of backward summoning, augment the force off vector

        q_heating, y = self.simulate(m_flow, working_mode=control_state, t_end=time, t_start=t_start, T_in=T_in_vect)

        # temperature at which the heat for the useful effect is exchanged
        # get the last temperature of the outlet
        T_source = self.state[-1]

        return q_heating, T_source

    def propagate_datamanager(self):
        """ Propagate datamanager obect """
        self.heating_element.data_manager = self.data_manager
        self.heating_element.propagate_datamanager()

    @staticmethod
    def plots(y, q_hy, m, Tmin, Text, n_layers):
        """ Create plots
        :param y: Data to plot
        :type y: numpy ndarray
        :param: q_hy, injected power by the electric resistance
        :type: numpy ndarray
        :param m:
        :type m: numpy ndarray
        :param Tmin: minimum temperature for hysteresis control [K]
        :type Tmin: float
        :param Text: external temperature (ambient), constant [K]
        :type Text: float
        :param n_layers: umber of layers
        :type n_layers: int
        """
        fig = plt.figure()
        ax = fig.add_subplot(311)
        for i in np.arange(n_layers):
            ax.plot(y[:, i], label=''.join(('T_', str(i))))
        plt.hlines(Tmin, 0, len(y), label='Tmin')
        plt.hlines(Text, 0, len(y), 'g', label='Text')
        plt.ylabel('T [K]')
        plt.xlabel('time [s]')

        plt.legend()
        ax.grid()

        ax = fig.add_subplot(312)
        ax.plot(q_hy, '--', label='Qhy')
        plt.ylim(-np.max(q_hy)*0.1, np.max(q_hy)*1.1)
        plt.ylabel('Q [W]')
        plt.xlabel('time [s]')

        plt.legend()
        ax.grid()

        ax = fig.add_subplot(313)
        ax.plot(m, label='m in')
        plt.ylabel('m_flow [l/s]')
        plt.xlabel('time [s]')
        plt.legend()
        ax.grid()

        plt.show()