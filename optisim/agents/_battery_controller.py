# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #
import numpy as np
import scipy.sparse as sparse
import cvxpy as cvx
import quadprog
import scipy as sp
from numba import jit
from functools import reduce
import matplotlib.pyplot as plt
from optisim.controllers.holt_winters import HoltWinters

# --------------------------------------------------------------------------- #
# Constants
# --------------------------------------------------------------------------- #
SECS_IN_15M = 60*15
SECS_IN_1Y = 365*24*3600
YEARS_LIFETIME = 20

# --------------------------------------------------------------------------- #
# Functions
# --------------------------------------------------------------------------- #
def controller_pars_builder(e_n: float=1,
                            c: float=1,
                            ts: int=SECS_IN_15M,
                            tau_sd: int=SECS_IN_1Y,
                            lifetime: int=YEARS_LIFETIME,
                            dod: float=0.8,
                            nc: float=3000,
                            eta_in: float=0.9,
                            eta_out: float=0.9,
                            h: float=48,
                            pb: float=0.2,
                            ps: float=0.07,
                            type: str='peak_shaving',
                            alpha: float=1,
                            rho: float=0.5):
    """
    :param e_n: nominal capacity of the battery [kWh]
    :type e_n: double
    :param c: C factor [kW/kWh]
    :type c: double
    :param ts: sampling time for the problem solution [s]
    :type ts: int        
    :param tau_sd: time constant for self-discharge [s]
    :type tau_sd: int
    :param lifetime: lifetime of the battery [years]
    :type lifetime: int
    :param dod: depth of discharge [-]
    :type dod: int
    :param nc: maximum cycles before removal [-]
    :type nc: double
    :param eta_in: charge efficiency [-]
    :type eta_in: double
    :param eta_out: discharge efficiency [-]
    :type eta_out: double
    :param h:
    :type h: float
    :param pb:
    :type pd: float
    :param ps:
    :type ps: float
    :param type:
    :type type: str
    :param alpha:
    :type alpha: float
    :param rho:
    :type rho: float
    """
    return {'e_n': e_n,
            'c': c,
            'ts': ts,
            'tau_sd': tau_sd,
            'lifetime': lifetime,
            'dod': dod,
            'nc': nc,
            'eta_in': eta_in,
            'eta_out': eta_out,
            'h': h,
            'pb': pb,
            'ps': ps,
            'type': type,
            'alpha': alpha,
            'rho': rho}

# --------------------------------------------------------------------------- #
# Classes
# --------------------------------------------------------------------------- #
class BatteryController:
    def __init__(self, pars=None, logger=None):
        """
        Constructor
        :param pars: controller parameters
        :type pars: dict
        :param logger: logger object
        :type logger: logger
        """

        self.pars = pars
        self.cvx_solver = None
        self.pm = None
        self.ref = None
        self.u = None
        self.dsch_punish = None
        self.x_mat = None
        self.b_mat = None
        self.l_mat = None
        self.cvx_mat_solver = None

        if self.pars['num_log_steps'] > 0:
            sum_stp = 0
            expend = 2.7
            h = self.pars['h'] / self.pars['ts']
            assert h == int(h), 'h is not a multiple of ts'
            h = int(h)
            steps = 0
            while sum_stp != h:
                steps = np.asanyarray(np.logspace(0, expend, self.pars['num_log_steps']), int)
                sum_stp = np.sum(steps)
                if sum_stp > h:
                    expend = expend - 0.00001
                else:
                    expend = expend + 0.00001

            self.h = self.pars['num_log_steps']
            self.steps = steps
            self.pars['ts'] = steps * self.pars['ts']
        else:
            # control horizon
            self.h = self.pars['h'] / self.pars['ts']
            assert self.h == int(self.h), 'h is not a multiple of ts'
            self.h = int(self.h)
            self.steps = list(np.ones(self.h, int))

        # Battery current states, initialized at half maximum capacity
        self.E_0 = self.pars['e_n']
        self.x_start = self.E_0/2
        self.x = None
        self.y = None

        # State matrix
        self.Ac = -1/self.pars['tau_sd']

        # Input matrix
        self.Bc = np.array([self.pars['eta_in']/3600, - 1/(self.pars['eta_out'] * 3600)])

        # Exact discretization of Ac
        self.Ad = np.asanyarray(np.exp(self.pars['ts']*self.Ac),float)

        # Exact discretization of Bc
        Bd = []
        if np.size(self.Ad) == 1:
            Bd.append(np.divide((self.Ad - 1) * self.Bc, self.Ac).reshape(1, 2))
        else:
            for i in np.arange(np.size(self.Ad)):
                Bd.append(np.divide((self.Ad[i] - 1)*self.Bc, self.Ac).reshape(1,2))
        self.Bd = np.vstack(Bd)
        # Calendar aging coefficient [1/s]
        self.k_t = -0.2/self.pars['lifetime']*SECS_IN_1Y

        # Coefficient of degradation
        self.k_q = -0.2/(2*self.pars['nc']*self.pars['e_n']*self.pars['dod'])

        # Logger
        self.logger = logger

        # build difference matrix for total power from / to the battery
        D = np.zeros((self.h, 2 * self.h))
        for i in np.arange(self.h):
            D[i, i*2: 2 *(i+1)] = [1, -1]

        self.D = D

        # build fixed constraints
        self.x_u = self.E_0
        self.x_l = self.E_0 * (1-self.pars['dod'])
        self.u_l = np.zeros((1,2)).reshape(-1, 1)
        self.u_u = np.ones((1, 2)).reshape(-1, 1) * self.E_0 * self.pars['c']

        #-------- Build norm 2 matrix ----------
        if self.pars['type'] in ['distributed','peak_shaving']:
            self.Tcvx = self.D
        elif self.pars['type'] == 'stochastic':
            self.Tcvx = sparse.eye(self.h * 2)
        elif self.pars['type'] == 'dist_stoc':
            SScvx = sparse.eye(self.h * 2)
            self.Tcvx = np.vstack((self.D,SScvx))

        self.alpha = self.pars['alpha']
        self.rho = self.pars['rho']

        k = self.alpha / (2 * self.rho)
        self.k = k

        # ------ Build a CVX solver as reference -------
        self.build_cvx_solver(k)
        self.x_start.value = [self.E_0*0.5]

        # ------ Initialize a Hoolt Winter forecaster -------
        if np.size(self.pars['ts']) > 1:
            n_step_per_day = int(86400/self.pars['ts'][0])
        else:
            n_step_per_day = int(86400 / self.pars['ts'])


        # TODO: integrate conf
        alphas = []
        betas = []
        gammas = []
        alpha_a = 0.9611
        alpha_b = 0.2342
        alpha_c = 0.00893

        for i in range(len(self.steps)):
            if i < 60:
                alphas.append(alpha_a * np.exp(-alpha_b * i) + alpha_c)
                #     alphas.append(0.3)
                # elif i < 8:
                #     alphas.append(0.2)
                # elif i < 12:
                #     alphas.append(0.1)
            else:
                alphas.append(0.0)

            #gammas.append([0.3, 0.01])
            gammas.append([0.3])
            betas.append(0.0)

        hw_pars = {'step_length': 1,
                   'steps': self.steps,
                   'alphas': alphas,
                   'betas': betas,
                   'gammas': gammas,
                   # 'period': [1440, 1440 * 7],
                   'period': [1440],
                   'method': 'add',
                   'history_length': 1,
                   'history_offset': 1440}

        self.forecaster = HoltWinters(hw_pars)
        #self.forecaster = HoltWinters(alpha=0.05, beta=0, gamma=np.array([0.9,0.9]), period=np.array([1440,1440*7]), horizon=np.array(range(1,n_step_per_day+1)), method='add')

        self.P_hat = None

        self.batch = Batch(self)
        self.x_batch = self.E_0/2

    def build_cvx_solver(self, k):
        """
        Define the CVX solver
        :param k: sum_squares factor
        :type k: float
        """
        p_buy = self.pars['pb']
        p_sell = self.pars['ps']
        H = self.h
        x = cvx.Variable((H + 1,1))
        y = cvx.Variable((H, 1))
        u = cvx.Variable((H, 2))
        pm = cvx.Parameter((H, 1))
        x_start = cvx.Parameter(1)
        one_v = np.ones((1, H))
        constraints = [x[1:] <= self.x_u]
        if np.size(self.Ad)>1:
            assert np.size(self.Ad) == H , 'Error:length of ts must be equal to the horizon'
            for i in np.arange(len(self.Ad)):
                #constraints.append( x[i+1] == self.Ad[i] * x[i] +  u[[i],:] * self.Bd[[i],:].T)
                constraints.append(x[1:] == np.diag(self.Ad) * x[0:-1] + cvx.reshape(cvx.diag(u * self.Bd.T),(len(self.Ad),1)))
        else:
            constraints.append( x[1:] == self.Ad * x[0:-1] +  u * self.Bd.T)
        constraints.append(x[1:] <= self.x_u)
        constraints.append(x[1:] >= self.x_l)
        constraints.append(u[:, 0] >= 0)
        constraints.append(u[:, 1] >= 0)
        constraints.append(u[:, 0] <= self.E_0 * self.pars['c'])
        constraints.append(u[:, 1] <= self.E_0 * self.pars['c'])
        constraints.append(y >= p_buy * (pm + cvx.reshape((u[:, 0] - u[:, 1]),(H,1))))
        constraints.append(y >= p_sell * (pm + cvx.reshape((u[:, 0] - u[:, 1]),(H,1))))
        constraints.append(x[0] == x_start)
        # eco_cost = ct * (pm + (u[0] - u[1]))
        if self.pars['type'] == 'economic':
            cost = one_v * y
            ref = None
            dsch_punish = None
        elif self.pars['type'] in ['stochastic','distributed','peak_shaving','dist_stoc']:
            ref = cvx.Parameter((self.Tcvx.shape[0], 1))
            dsch_punish = cvx.Parameter((1,self.Tcvx.shape[0]))
            cost = one_v * y + dsch_punish*u[:,1] + k*cvx.sum_squares(self.Tcvx*cvx.reshape(u.T,(H*2,1))-ref)
        else:
            raise TypeError('pars["type"] not recognized')

        # peak_cost = cvx.square(pm + (u[0]-u[1]))
        obj = cost
        prob = cvx.Problem(cvx.Minimize(obj), constraints)

        self.cvx_solver = prob
        self.pm = pm
        self.ref =ref
        self.x_start = x_start
        self.u = u
        self.x = x
        self.y = y
        self.dsch_punish = dsch_punish


    def solve_step(self):
        """
        Solve a single step
        """
        P_hat = self.P_hat
        self.pm.value = P_hat
        if self.pars['type'] == 'peak_shaving':
            self.ref.value = -P_hat
            self.dsch_punish.value = np.maximum(self.ref.value, 0).T * 100
        try:
            solution = self.cvx_solver.solve(solver='OSQP')
        except:
            solution = self.cvx_solver.solve(solver='SCS')

        if np.size(self.Ad) == 1:
            self.x_start.value = self.Ad * self.x_start.value + self.Bd.dot(
                self.u.value[0, :].reshape(-1, 1)).flatten()
        else:
            self.x_start.value = self.Ad[0] * self.x_start.value + self.Bd[[0],:].dot(self.u.value[0,:].reshape(-1, 1)).flatten()
        p_all = self.u.value[:, [0]] - self.u.value[:, [1]]
        p_set = p_all[0]
        return p_set, p_all

    def solve_batch(self,P_hat):
        """
        Solve a single step
        """
        #P_hat = self.P_hat
        self.pm.value = P_hat
        p_b = np.ones((self.h,1)) * self.pars['pb']
        p_s = np.ones((self.h, 1)) * self.pars['ps']

        if self.pars['type'] == 'peak_shaving':
            r = -P_hat

        method = 'cvx'
        self.u_batch = self.batch.solve_batch(self.x_batch,P_hat,r,method)
        self.e_batch = self.batch.H0*self.x_batch +self.batch.Hu.dot(self.u_batch.reshape(-1,1))
        if np.size(self.Ad) == 1:
            self.x_batch = self.Ad * self.x_batch + self.u_batch[0, :].dot(self.Bd.T)
        else:
            self.x_batch = self.Ad[0] * self.x_batch + self.u_batch[0,:].dot(self.Bd[[0],:].T)
        p_set = self.u_batch[0,0] - self.u_batch[0,1]
        return p_set

    def update_forecast(self, P, Pb_old):
        """
        Update the forecast
        :param P:
        :type P: numpy ndarray
        :param Pb_old:
        :type P_old: numpy ndarray
        """
        p_corrected = P - Pb_old
        self.P_hat = self.forecaster.predict(p_corrected).reshape(-1,1)
        # plt.cla()
        # plt.plot(self.P_hat)
        # plt.ylim(-5.0, 5.0)
        # plt.pause(0.001)

class HoltWinters_2:
    """
    Holt_Winters class

    Attributes:
    """

    def __init__(self, alpha=0.1, beta=1e-5, gamma=np.array([0.3]), period=np.array([96]),
                 horizon=np.array(range(1, 97)), method='add'):
        """
        Constructor
        :param alpha:
        :type alpha: float
        :param beta:
        :type bety: float
        :param gamma:
        :type gamma: numpy ndarray
        :param period:
        :type period: numpy ndarray
        :param horizon
        :type horizon: numpy ndarray
        :param method:
        :type method: str
        """

        assert 0 < period.size <= 2, "wrong period size (the class accepts 1 or 2 seasons)"
        assert period.size == gamma.size, "gamma and period must have the same length"
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        self.method = method
        self.period = period
        self.horizon = horizon

        self.am1 = 0
        self.bm1 = 0
        self.s1mp = np.zeros(self.period[0])
        if self.period.size == 2:
            self.s2mp = np.zeros(self.period[1])

    def fit(self, y_history):
        """
        Fit
        :param y_history: y history
        :type y_history: numpy ndarray
        """
        if self.method == 'add':
            self._fit_add(y_history)

    def _fit_add(self, y_history):
        """
        Fit additive
        :param y_history: y history
        :type y_history: numpy ndarray
        """
        if self.period.size == 1:
            a = np.zeros(y_history.size)
            b = np.zeros(y_history.size)
            s = np.zeros(y_history.size)
            for i in range(max(self.period), y_history.size):
                a[i] = self.alpha * (y_history[i] - s[i - self.period[0]]) + (1 - self.alpha) * (
                            a[i - 1] + b[i - 1])
                b[i] = self.beta * (a[i] - a[i - 1]) + (1 - self.beta) * b[i - 1]
                # s[i] = self.gamma[0] * (y_history[i] - a[i-1] - b[i-1]) + (1 - self.gamma[0]) * s[i - self.period[0]]
                s[i] = self.gamma[0] * (y_history[i] - a[i]) + (1 - self.gamma[0]) * s[i - self.period[0]]
                # y_hat = np.zeros(self.horizon.size)
                # for j, h in enumerate(self.horizon):
                #    y_hat[j] = a[i] + h * b[i] + s[i - self.period[0] + (h - 1) % self.period[0]+1]
            self.am1 = a[-1]
            self.bm1 = b[-1]
            self.s1mp = s[-self.period[0]:]
        else:
            a = np.zeros(y_history.size)
            b = np.zeros(y_history.size)
            s1 = np.zeros(y_history.size)
            s2 = np.zeros(y_history.size)
            for i in range(max(self.period), y_history.size):
                a[i] = self.alpha * (y_history[i] - s1[i - self.period[0]] - s2[i - self.period[1]]) + (
                            1 - self.alpha) * (a[i - 1] + b[i - 1])
                b[i] = self.beta * (a[i] - a[i - 1]) + (1 - self.beta) * b[i - 1]
                s1[i] = self.gamma[0] * (y_history[i] - a[i] - s2[i - self.period[1]]) + (1 - self.gamma[0]) * \
                        s1[i - self.period[0]]
                s2[i] = self.gamma[1] * (y_history[i] - a[i] - s1[i - self.period[0]]) + (1 - self.gamma[1]) * \
                        s2[i - self.period[1]]
            self.am1 = a[-1]
            self.bm1 = b[-1]
            self.s1mp = s1[-self.period[0]:]
            self.s2mp = s2[-self.period[1]:]

    def predict(self, y):
        """
        Predict
        :param y: y
        :type y: numpy ndarray
        """
        if self.method == 'add':
            return self._predict_add(y)

    def _predict_add(self, y):
        """
        Predict additive
        :param y: y
        :type y: numpy ndarray
        """
        y_hat = np.zeros(self.horizon.size)
        if self.period.size == 1:
            a = self.alpha * (y - self.s1mp[0]) + (1 - self.alpha) * (self.am1 + self.bm1)
            b = self.beta * (a - self.am1) + (1 - self.beta) * self.bm1
            # s = self.gamma[0] * (y - self.am1 -self.bm1) + (1 - self.gamma[0]) * self.s1mp[0]
            s = self.gamma[0] * (y - a) + (1 - self.gamma[0]) * self.s1mp[0]
            self.s1mp = np.roll(self.s1mp, -1)
            self.s1mp[-1] = s
            self.am1 = a
            self.bm1 = b
            for i, h in enumerate(self.horizon):
                y_hat[i] = a + h * b + self.s1mp[(h - 1) % self.period[0]]
        else:
            a = self.alpha * (y - self.s1mp[0] - self.s2mp[0]) + (1 - self.alpha) * (self.am1 + self.bm1)
            b = self.beta * (a - self.am1) + (1 - self.beta) * self.bm1
            s1 = self.gamma[0] * (y - a - self.s2mp[0]) + (1 - self.gamma[0]) * self.s1mp[0]
            s2 = self.gamma[1] * (y - a - self.s1mp[0]) + (1 - self.gamma[1]) * self.s2mp[0]
            self.s1mp = np.roll(self.s1mp, -1)
            self.s1mp[-1] = s1
            self.s2mp = np.roll(self.s2mp, -1)
            self.s2mp[-1] = s2
            self.am1 = a
            self.bm1 = b
            for i, h in enumerate(self.horizon):
                y_hat[i] = a + h * b + self.s1mp[(h - 1) % self.period[0]] + self.s2mp[(h - 1) % self.period[1]]
        return y_hat

class Batch:
    def __init__(self, controller):
        """
        Define optimiyation problem in matrix form
        :param battery: battery
        :type battery: Battery object
        :param x_0: initial state
        :type x_0: double
        :param ct: cost profile
        :type ct: NumPy array
        :param h: number of steps ahead
        :type h: int
        """
        self.controller = controller
        self.p_buy = controller.pars['pb']
        self.p_sell = controller.pars['ps']
        self.D = controller.D
        self.h = controller.h
        self.Ad = controller.Ad
        self.Bd = controller.Bd
        self.U_u = np.kron(np.ones((controller.h, 1)), controller.u_u.reshape(-1, 1))
        self.U_l = np.kron(np.ones((controller.h, 1)), controller.u_l.reshape(-1, 1))
        self.X_u = np.kron(np.ones((controller.h, 1)), controller.x_u)
        self.X_l = np.kron(np.ones((controller.h, 1)), controller.x_l)

        self.H0 = self.build_H0()
        self.Hu = self.build_Hu()
        self.A = self.build_A()
        self.k = controller.k
        self.T = controller.Tcvx
        Z = 1e-6*np.ones((self.h,self.h))
        T = np.hstack([Z,self.T])
        self.Q = self.k*T.T.dot(T)

        self.cvx_mat_solver = None
        self.build_cvx_matrix_solver()


    def build_H0(self):
        '''
        Define matrix X for the optimization
        :param A: discretized state matrix for the controlled battery
        :type A: numpy ndarray
        :param h: horizon of the MPC in steps
        :type: double
        :return: X
        :type: numpy ndarray
        '''
        H0 = np.zeros((self.h, 1),dtype=float)
        r = 1
        if np.size(self.Ad) == 1:
            for ix in np.arange(self.h+1):
                H0[r*ix:(ix+1)*r ,:] = np.power(self.Ad,ix)
        else:
            for ix in np.arange(self.h):
                H0[r * ix :(ix +1)* r, :] = np.power(self.Ad[ix], ix)
        return H0


    def build_Hu(self):
        '''
        Define matrix X for the optimization
        :param A: discretized state matrix for the controlled battery
        :type A: numpy ndarray
        :param B: discretized input matrix for the controlled battery
        :type B: numpy ndarray
        :param h: horizon of the MPC in steps
        :type: double
        :return: M
        :type: numpy ndarray
        '''

        nb = 1
        mb = 2

        Hu = np.zeros((self.h * nb, self.h * mb))

        if np.size(self.Ad) == 1:
            for i in np.arange(self.h):
                matarray = []
                for j in np.arange(self.h-i):
                    matarray.append(np.power(self.Ad, i)*self.Bd)
                ABdiag = sp.linalg.block_diag(*matarray)
                Hu[nb*i:,0: self.h*mb - i*mb] = Hu[nb*i:, 0:self.h*mb -i*mb] + ABdiag
        else:
            for step in np.arange(self.h):
                column = []
                for t in np.arange(self.h-step):
                    if t>0:
                        Aprod = reduce(np.dot,self.Ad[step:step+t])
                    else:
                        Aprod =1

                    column.append(np.dot(Aprod,self.Bd[step]))
                Hu[step*nb:,step*mb:(step+1)*mb] = np.vstack(column)

        return Hu

    def build_A(self):
        E = np.eye(self.h)
        Eu = np.eye(self.h*2)
        Z = np.zeros((self.h,self.h))
        Zu = np.zeros((self.h*2, self.h))

        A1 = np.hstack([E,-self.D*self.p_buy])
        A1 = np.vstack([A1,np.hstack([E,-self.D*self.p_sell])])
        A2 = np.hstack([Zu,Eu])
        A2 = np.vstack([A2,np.hstack([Zu,- Eu])])
        A3 = np.hstack([Z, self.Hu])
        A3 = np.vstack([A3, np.hstack([Z, -self.Hu])])
        A = np.vstack([A1,A2,A3])
        return A

    #@jit
    def build_b(self,x_0,Pm):
        b1 = np.vstack([self.p_buy * Pm, self.p_sell * Pm])
        b2 = np.vstack([self.U_l,-self.U_u])
        b3 = np.vstack([self.X_l-self.H0*x_0,-(self.X_u-self.H0*x_0)])
        b = np.vstack([b1,b2,b3])
        return b

    def solve_batch(self,x_0,Pm,r,method):
        '''
        Solve the batch cost problem min c_t*(Am*x-r_t) s.t. Ain*x<=bin
        :param Ain: inequality matrix for the problem
        :type Ain: numpy ndarray
        :param bin: inequality vector for the problem
        :type bin: numpy array
        :param Am: transformation matrix for the objective
        :type Am: numpy ndarray
        :param X: X matrix
        :type X: numpy ndarray
        :param M: M matrix
        :type M: numpy ndarray
        :param r_t: reference vector for the batch (uncontrolled loads vector)
        :type r_t: numpy array
        :param c_t: cost vector for the batch
        :type c_t: numpy array
        :param method: solver method. WARNING: gurobi is not implemented yet!
        :type method: string in {'cvx','gurobi'}
        :return:u_opt, r_opt, e_opt
        :type: numpy ndarray
        '''

        b = self.build_b(x_0,Pm)
        lin_cost = np.ones((1,self.h))
        dsch_punish = 100*np.tile(np.array([0,1]),(1,self.h))*np.tile(np.asanyarray(r>0,int),(1,2)).reshape(1,-1)
        ref_cost = -2*self.k*r.T.dot(self.T)
        l = np.hstack([lin_cost,dsch_punish+ref_cost])
        z_opt = self.solve_lin_q_prog(self.A,b,l,self.Q,method)
        u_opt = z_opt[self.h:].reshape(-1,2)

        return u_opt

    def solve_lin_q_prog(self,A,b,l,Q, method):
        '''
        Solve cost problem min c_t*(A*x-b) s.t. Ain*x<=bin  using matrix formulation
        :param A: A matrix
        :type A: numpy ndarray
        :param b: b reference vector
        :type b: numpy array
        :param Ain: inequality constraint matrix
        :type Ain: numpy ndarray
        :param bin: inequality constraint vector
        :type bin: numpy array
        :param method: solver method. WARNING: gurobi is not implemented yet!
        :type method: string in {'cvx','gurobi'}
        :return:
        '''

        if method == 'cvx':
            '''x = cvx.Variable((np.shape(A)[1],1))
            obj = l*x + cvx.quad_form(x,Q)
            constraints = [A*x >= b]
            prob = cvx.Problem(cvx.Minimize(obj), constraints)
            solution = prob.solve(verbose=False)
            x_opt = x.value
            '''

            self.controller.b_mat.value = b
            self.controller.l_mat.value = l
            self.cvx_mat_solver.solve(verbose=False)
            x_opt = self.controller.x_mat.value

        elif method == 'quadprog':
            x_opt = self.quadprog_solve_qp(Q, l, G=-A, h=-b.flatten())
            x_opt = x_opt[0:n]
        elif method == 'osqp':
            #prob = osqp.OSQP()
            #P = 1e-16 * sparse.eye(np.shape(A)[1])
            #        prob.setup(P=P,q=f.T,A=A,l=lb,u=ub)
    #        prob.codegen('code',python_ext_name='emosqp')
            emosqp.update_bounds(lb,ub)
            emosqp.update_lin_cost(f.T)
            a = emosqp.solve()
            x_opt = a[0][0:n]

        return x_opt

    def quadprog_solve_qp(self,P, q, G=None, h=None, A=None, b=None):
        qp_G = .5 * (P + P.T)  # make sure P is symmetric
        qp_a = -q
        if A is not None:
            qp_C = -np.vstack([A, G]).T
            qp_b = -np.hstack([b, h])
            meq = A.shape[0]
        else:  # no equality constraint
            qp_C = -G.T
            qp_b = -h
            meq = 0
        return quadprog.solve_qp(qp_G, qp_a.T.flatten(), qp_C, qp_b.flatten(), meq)[0]

    def build_cvx_matrix_solver(self):

        A = self.A
        Q = self.Q
        x = cvx.Variable((self.h*3, 1))
        b = cvx.Parameter((self.h * 8, 1))
        l = cvx.Parameter((1,self.h * 3))

        obj = l * x + cvx.quad_form(x, Q)
        constraints = [A * x >= b]
        prob = cvx.Problem(cvx.Minimize(obj), constraints)
        self.cvx_mat_solver = prob
        self.controller.x_mat = x
        self.controller.l_mat = l
        self.controller.b_mat = b
