import numpy as np

from .agent import Agent


class PV(Agent):
    def __init__(self,
                 data_manager,
                 broker_producer,
                 descriptor,
                 meter=None,
                 **_kwargs):
        """ Constructor
        :param data_manager: object able to get data from InfluxDB and maintain them internally
        :type data_manager: classes.utils.input_data_manager.InputDataManager
        :param broker_producer: broker producer used to send message to the configured broker broker
        :type broker_producer: optisim.utils.rabbit_sim_producer.RabbitSimulationProducer object
        :param descriptor: dictionary containing all the configuration settings needed by Agent
        :type descriptor: dict
        :param meter: meter object
        :type meter: optisim.agents.meter.Meter object
        """
        available_vars = ['p_tot', 'q_tot', 'p_a', 'p_b', 'p_c', 'q_a', 'q_b', 'q_c']
        super().__init__(data_manager=data_manager, broker_producer=broker_producer, descriptor=descriptor,
                         available_vars=available_vars, meter=meter)

    def simulate(self, t_start: float, t_end: float):
        """ Perform a simulation step
        :param t_start: starting time of simulation, default = 0
        :type t_start: int
        :param t_end: final time of simulation
        :type t_end: int
        :return: P and Q data
        :rtype: numpy ndarray
        """
        fields = self.descriptor['fields']
        tag = {'type': self.descriptor['type'],
               'name': self.descriptor['profile']}
        aggr_fun = 'mean'
        name = self.descriptor['name']
        interval = self.dt
        pv_data = self.data_manager.get_simulation_dataset(t_start, t_end, name, fields, tag, interval, aggr_fun)
        p_norm = pv_data['P'].values
        p = - np.minimum(np.ones(p_norm.shape) * self.descriptor['pars']['Pac'], self.descriptor['pars']['Pdc'] * p_norm)
        q = np.sqrt(1-self.descriptor['pars']['cos_phi']**2)/self.descriptor['pars']['cos_phi'] * p

        p_all = np.zeros((p.shape[0], 3))
        q_all = np.zeros((q.shape[0], 3))

        for phase in self.phases:
            p_all[:, self.phases_num[phase]] = p / self.num_phases
            q_all[:, self.phases_num[phase]] = q / self.num_phases

        # write on pq_buffer
        self.write_pq_buffer(p_all, q_all, t_start, t_end)

        # write data on internal buffer
        buffer_dict = {'p_tot': p,
                       'q_tot': q,
                       'p_a': p_all[:, 0],
                       'p_b': p_all[:, 1],
                       'p_c': p_all[:, 2],
                       'q_a': q_all[:, 0],
                       'q_b': q_all[:, 1],
                       'q_c': q_all[:, 2]
                       }

        # write in the current buffer
        dt = self.dt
        times = np.arange(t_start, t_end + dt, dt)
        self.increment_buffer(buffer_dict, times)

        return p_all, q_all
