# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #
from time import time as time

import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint

from .agent import Agent
from ._building_models import *
from .electric_heater import Heater


# --------------------------------------------------------------------------- #
# Functions
# --------------------------------------------------------------------------- #
def sim_pars_builder(do_plots=False, data_source='.'):
    """
    :param do_plots: if true, plot states and variables at the end of the simulations
    :param data_source: data source for the simulation, as a local or http address
    :return:
    """
    pars = {'do_plots': do_plots}
    return pars


# --------------------------------------------------------------------------- #
# Classes
# --------------------------------------------------------------------------- #
class Building(Agent):
    def __init__(self,
                 data_manager,
                 broker_producer,
                 descriptor=None,
                 heating_element=None,
                 pars=None,
                 sim_pars=sim_pars_builder(),
                 meter=None,
                 **_kwargs):
        """ Constructor
        :param data_manager: object able to get data from InfluxDB and maintain them internally
        :type data_manager: classes.utils.input_data_manager.InputDataManager
        :param broker_producer: broker producer used to send message to the configured broker broker
        :type broker_producer: optisim.utils.rabbit_sim_producer.RabbitSimulationProducer object
        :param descriptor: dictionary containing all the configuration settings needed by Agent
        :type descriptor: dict
        :param heating_element: list of variables whose values will be saved in InfluxDB
        :type heating_element: optisim.agents.electric_heater.Heater object
        :param pars: dictionary containing all the building parameters
        :type pars: dict
        :param sim_pars: dictionary containing all the simulation parameters
        :type sim_pars: dict
        :param meter: meter object
        :type meter: optisim.agents.meter.Meter object
        """
        # -----------------------
        # Set the building model
        # ----------------------

        model_list = {'default', '1s', '2s'}
        building_model = pars['building_model']
        if pars is not None:
            pars_list = {"ma_days", "Tmin_ma", "Tmax_ma", "shade_ratio"}
            assert all(
                [k in pars.keys() for k in pars_list]), "ERROR: some keys are not present in building_pars"

        if descriptor['profile'] is None:
            pars_list = {"Tmin", "Tmax"}
            assert all(
                [k in pars.keys() for k in pars_list]), "ERROR: some keys are not present in building_pars"

        assert building_model in model_list, 'Error: Building model is not in model library!'
        self.building_model = building_model

        if building_model == 'default':
            self.n_states = 1
            if pars is None:
                self.pars = pars_builder_default()
            else:
                pars_list = {"k1","k2","k3","dT_hys","C"}
                assert all(
                    [k in pars.keys() for k in pars_list]), "ERROR: some keys are not present in building_pars"
                self.pars = pars
            self.ode = ode_default
            self.heating_model = heating_model_default

        elif building_model == '1s':
            self.n_states = 1
            if pars is None:
                self.pars = pars_builder_1s()
            else:
                pars_list = {"R", "R_s", "R_w", "alpha", "Uwall", "C", "Aw", "rho_air", "cp_air"}
                assert all(
                    [k in pars.keys() for k in pars_list]), "ERROR: some keys are not present in building_pars"
                self.pars = pars
            self.ode = ode_1s
            self.heating_model = heating_model_1s

        elif building_model == '2s':
            self.n_states = 2
            if pars is None:
                self.pars = pars_builder_2s()
            else:
                pars_list = {"R", "R_s", "R_w", "alpha", "Uwall", "C", "Aw", "rho_air", "cp_air"}
                assert all(
                    [k in pars.keys() for k in pars_list]), "ERROR: some keys are not present in building_pars"
                self.pars = pars
            self.ode = ode_2s
            self.heating_model = heating_model_2s

        available_vars = ['q_heating', 'hy_state', 'Text_ma', 'working_mode', 'shading']
        for i in np.arange(self.n_states):
            available_vars.append('state_' + str(i))

        super().__init__(data_manager=data_manager, descriptor=descriptor, broker_producer=broker_producer, available_vars=available_vars, meter=meter)

        self.state_vars = ['hy_state', 'Text_ma', 'Text_ma_prev']
        for i in np.arange(self.n_states):
            self.state_vars.append('state_' + str(i))
        # -----------------------
        # Set the heating element and propagate datamanager to the offspring
        # ----------------------
        self.heating_element = heating_element

        self.heating_element.data_manager = data_manager
        self.heating_element.propagate_datamanager()

        # check that all the parameters are present
        sim_pars_list = {'do_plots'}
        assert all([k in sim_pars.keys() for k in sim_pars_list]), "ERROR: some keys are not present in sim_pars"
        self.sim_pars = sim_pars

        # initialize internal temperatures to 20 °C
        self.state = 20 * np.ones(self.n_states)

        # initialize hysteresis state
        self.hysteresis_state = 0

        # initialize MA of external temperature
        self.state_Text_ma = 0
        self.Tmin_ma = self.pars['Tmin_ma']
        self.Tmax_ma = self.pars['Tmax_ma']
        self.state_last_Text_ma = None
        self.n_ma = int(self.pars['ma_days']*86400/self.dt)

        # init from db
        if self.is_load_states_from_db:
            states = dict()
            for i in np.arange(len(self.state)):
                states.update({'state[{}]'.format(i): {'tags': {'signal': 'state_' + str(i)}}})
            states.update({'hysteresis_state': {'tags': {'signal': 'hy_state'}}})
            states.update({'state_Text_ma': {'tags': {'signal': 'Text_ma'}}})
            states.update({'state_last_Text_ma': {'tags': {'signal': 'Text_ma_prev'}}})
            self.load_states_from_db(states)


    def hysteresis(self, T, working_mode, previous_state, Tmin, Tmax, DT):
        """ Hysteresis control
        :param T: temperature of the sensor [K]
        :type T: float
        :param previous_state: previous state of the hysteresis
        :type previous_state: int
        :param Tmin: minimum temperature for the hysteresis [K]
        :type Tmin: int
        :param Tmax: maximum temperature for the hysteresis [K]
        :type Tmax: int
        :param DT: delta T for the hysteresis [K]
        :type DT: float
        :return: Q_h, electric power of the resistance
        :rtype: float
        """
        if working_mode == 1:
            if T < Tmin - DT/2:
                state = True
            elif T < (Tmin + DT/2) and previous_state:
                state = True
            else:
                state = False
        elif working_mode == -1:
            if T > Tmax + DT/2:
                state = True
            elif T > (Tmax - DT/2) and previous_state:
                state = True
            else:
                state = False
        else:
            state = False

        if working_mode == -1 or T > Tmax:
            shading = True
        else:
            shading = False

        return state, shading

    def getT(self, t_start, t_end, T_name):
        """ Get temperature
        :param t_start: starting time
        :type t_start: int
        :param t_end: ending time
        :type t_end: int
        :param T_name: temperature name
        :type T_name: str
        :return: temperature at which the heat for the useful effect is exchanged
        :rtype: float
        """
        if self.descriptor['profile'] is None:
            return self.pars[T_name] * np.ones(int((t_end-t_start+self.dt)/self.dt))
        else:
            fields = self.descriptor['fields']
            tag = {'type': 'TSet',
                   'name': self.descriptor['profile']}
            aggr_fun = 'mean'
            name = self.descriptor['name']
            interval = self.dt
            T_data = self.data_manager.get_simulation_dataset(t_start, t_end, name, fields, tag, interval, aggr_fun)
            return T_data[T_name].values

    def simulate(self, t_start, t_end, q_int=None, Tmin=None, Tmax=None):
        """ Perform a simulation step
        :param t_start: starting time of simulation, default = 0
        :type t_start: int
        :param t_end: final time of simulation
        :type t_end: int
        :param q_int: internal gains
        :type q_int: numpy ndarray
        :param Tmin: minimum setpoint temperature as a function of time
        :type Tmin: float | numpy ndarray
        :param Tmax: scalar or ndarray, minimum setpoint temperature as a function of time
        :type Tmax: float | numpy ndarray
        :return: injected power by the electric resistance
        :rtype numpy ndarray
        :return: new state of the building
        :rtype numpy ndarray
        :return: elapsed time
        :rtype float
        """

        dt = self.dt
        # filter meteo data based on force_off timestamps
        meteo_data = self.read_internal_dataset(t_start, t_end, 'meteo')

        Ta = meteo_data['T'].values.flatten()
        GHI = meteo_data['G_F'].values.flatten()/1e3
        n_steps = len(Ta)

        self.state_Text_ma = np.zeros(n_steps+1)
        if t_start == 0 and self.state_last_Text_ma is None:
            self.state_last_Text_ma = Ta[0]

        self.state_Text_ma[0] = self.state_last_Text_ma

        for i in np.arange(n_steps):
            self.state_Text_ma[i+1] = (self.state_Text_ma[i]*(self.n_ma-1) + Ta[i])/self.n_ma

        self.state_last_Text_ma = self.state_Text_ma[-1]



        # initialize Qint
        if q_int is None:
            q_int = np.zeros(n_steps)
        else:
            assert len(q_int) == n_steps, "length of Qint must be equal to the length of Ta"

        # initialize Tmin and Tmax
        if Tmin is None and Tmax is None:
            Tmin = self.getT(t_start, t_end, 'Tmin')
            Tmax = self.getT(t_start, t_end, 'Tmax')
        elif Tmin is not None and Tmax is not None:
            assert len(Tmin) == len(Tmax), "length of Tmin must be equal to the length of Tmax"
            if len(Tmin) == 1:
                Tmin = Tmin * np.ones(n_steps)
                Tmax = Tmax * np.ones(n_steps)
            else:
                assert len(Tmin) == n_steps, "length of Tmin must be 1 or equal to the length of Ta"
                assert len(Tmax) == n_steps, "length of Tmax must be 1 or equal to the length of Ta"
        else:
            if Tmin is None:
                Tmin = self.getT(t_start, t_end, 'Tmin')
                if len(Tmax) == 1:
                    Tmax = Tmax * np.ones(n_steps)
                else:
                    assert len(Tmax) == n_steps, "length of Tmax must be 1 or equal to the length of Ta"
            else:
                if len(Tmin) == 1:
                    Tmin = Tmin * np.ones(n_steps)
                else:
                    assert len(Tmax) == n_steps, "length of Tmax must be 1 or equal to the length of Ta"
                Tmax = self.getT(t_start, t_end, 'Tmax')

        # initialize heating model integrator and its states
        y_sol = np.zeros((n_steps + 1, len(self.state)))
        y_sol[0, :] = self.state


        # initialize Qheating vector
        q_heating = np.zeros(n_steps + 1)  # incoming thermal power into the building, due to heating system

        hy_state = np.zeros(n_steps + 1, 'int')  # internal building temperature hysteresis state
        hy_state[0] = self.hysteresis_state  # set hysteresis state to the internal hysteresis state
        working_mode = np.zeros(n_steps, 'int')
        shading = np.zeros(n_steps, 'int')


        sensor_layer = 0  # the first state is always the internal air temperature, on which the hysteresis is computed

        t0 = time()
        i = 0

        dt_int = int(dt)
        for current_time in np.arange(t_start, t_end+dt_int, dt_int):
            if self.state_Text_ma[i] > self.Tmax_ma:
                working_mode[i] = -1
            elif self.state_Text_ma[i] < self.Tmin_ma:
                working_mode[i] = 1
            else:
                working_mode[i] = 0

            # compute the state of the hysteresis and the working mode (based on external temperature moving average)
            hy_state[i+1], shading[i] = self.hysteresis(y_sol[i][sensor_layer], working_mode[i], hy_state[i], Tmin[i],
                                              Tmax[i],
                                              self.pars['dT_hys'])

            # compute the incoming power if needed
            if self.backward_summoning:
                if hy_state[i]:
                    q_heating[i + 1], T_source = self.heating_element.get_q(y_sol[i, sensor_layer], working_mode[i], hy_state[i],
                                                                            current_time, Ta[i])
                else:
                    q_heating[i + 1] = 0
            else:
                q_heating[i + 1], T_source = self.heating_element.get_q(y_sol[i, sensor_layer], working_mode[i], hy_state[i], current_time, Ta[i])


            y = odeint(self.heating_model, y0=y_sol[i,:], t= (current_time, current_time + dt_int),
                       args=(q_int[i], GHI[i], q_heating[i + 1], Ta[i], shading[i], self.pars))

            y_sol[i + 1, :] = y[1,:]
            i += 1

        elapsed_time = time() - t0

        # save the state
        y = np.vstack(y_sol[1:])
        self.state = y[-1]
        self.hysteresis_state = hy_state[-1]

        q_heating = q_heating[1:].reshape(-1, 1)

        ##################################
        # write data on internal buffer
        ##################################

        buffer_dict = {'q_heating': q_heating,
                       'hy_state': hy_state[1:].reshape(-1, 1),
                       'Text_ma': self.state_Text_ma[1:].reshape(-1, 1),
                       'Text_ma_prev': self.state_last_Text_ma,
                       'working_mode': working_mode,
                       'shading': shading}

        for i in np.arange(y.shape[1]):
            state_str = 'state_' + str(i)
            buffer_dict[state_str] = y[:, i].reshape(-1, 1)

        # write in the current buffer
        times = np.arange(t_start, t_end + dt, dt)
        self.increment_buffer(buffer_dict, times)
        # self.time = t_end
        self.time = t_end + dt

        return q_heating, y, elapsed_time

    @staticmethod
    def plots(y, q_heating, GHI, Tmin, Ta, q_int):
        """ Create plots
        :param y: Data to plot
        :type y: numpy ndarray
        :param q_heating: heating Q
        :type q_heating: numpy ndarray
        :param GHI: irradiance [GHI/Wm^2]
        :type GHI: numpy ndarray
        :param Tmin: minimum temperature for hysteresis control [K]
        :type Tmin: float
        :param Ta: ambient temperature [K]
        :type Ta: float
        :param q_int:
        :type q_int: numpy ndarray
        """
        fig = plt.figure()
        ax = fig.add_subplot(311)
        for i in np.arange(y.shape[1]):
            ax.plot(y[:, i], label=''.join(('T_', str(i))))
        plt.hlines(Tmin, 0, len(y), label='Tmin')
        ax.plot(Ta, 'g', label='Text')
        plt.ylabel('T [°C]')
        plt.xlabel('time [s]')

        plt.legend()
        ax.grid()

        ax = fig.add_subplot(312)
        ax.plot(q_heating, '--', label='Q_heating')
        ax.plot(q_int, label='Q_int')

        plt.ylim(np.min(q_heating), np.max(q_heating) * 1.1)
        plt.ylabel('Q [kW]')
        plt.xlabel('time [s]')

        plt.legend()
        ax.grid()

        ax = fig.add_subplot(313)
        ax.plot(GHI, label='GHI')
        plt.ylabel('ghi [kW/m2]')
        plt.xlabel('time [s]')
        plt.legend()
        ax.grid()

        plt.show()





