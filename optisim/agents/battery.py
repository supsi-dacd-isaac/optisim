from time import time as time

import matplotlib.pyplot as plt
import numpy as np

from .agent import Agent
from ._battery_controller import BatteryController


def sim_pars_builder(do_plots=False, data_source='.'):
    """
    Initialize the battery parameters dictionary

    :param do_plots: if true, plot states and variables at the end of the simulations
    :param data_source: data source for the simulation, as a local or http address
    :type do_plots: bool
    :type data_source: string
    :return: parameters
    :rtype: dict
    """
    pars = {'do_plots': do_plots}
    return pars


class Battery(Agent):
    """ Model of a battery"""
    def __init__(self,
                 data_manager,
                 descriptor=None,
                 pars=None,
                 broker_producer=None,
                 sim_pars=sim_pars_builder(),
                 state=None,
                 meter=None,
                 **_kwargs):
        """ Constructor

        :param data_manager: object able to get data from InfluxDB and maintain them internally
        :param descriptor: dictionary containing all the configuration settings needed by Agent
        :param pars: dictionary containing all the model parameters
        :param broker_producer: broker producer used to send message to the configured broker broker
        :param sim_pars: dictionary containing all the simulation parameters
        :param state: state of the battery (currently not used)
        :param meter: meter object
        :type data_manager: classes.utils.input_data_manager.InputDataManager
        :type descriptor: dict
        :type pars: dict
        :type broker_producer: optisim.utils.rabbit_sim_producer.RabbitSimulationProducer object
        :type sim_pars: dict
        :type state: int
        :type meter: optisim.agents.meter.Meter object
        """
        available_vars = ['p_tot', 'q_tot', 'p_a', 'p_b', 'p_c', 'q_a', 'q_b', 'q_c', 'SOC', 'SOC_controller', 'p_main',
                          'p_main_no_batt', 'force_off']
        super().__init__(data_manager=data_manager, descriptor=descriptor, broker_producer=broker_producer,
                         available_vars=available_vars, meter=meter)

        self.state_vars = ['SOC', 'force_off']

        # set datamanager
        self.data_manager = data_manager

        # check that all the parameters are present
        pars_list = {
                     'E_min',
                     'E_max',
                     'eff_charge',
                     'eff_discharge',
                     'p_max_charge',
                     'p_max_discharge',
                     'use_internal_controller'
                     }
        assert all([k in pars.keys() for k in pars_list]), "ERROR: some keys are not present in boiler_pars"
        sim_pars_list = {'do_plots'}
        assert all([k in sim_pars.keys() for k in sim_pars_list]), "ERROR: some keys are not present in sim_pars"

        self.pars = pars
        self.sim_pars = sim_pars

        # -----------------------
        # initialize state and time
        # -----------------------
        self.state = self.pars['E_max'] * 0.5
        self.time = 0

        # init from db
        if self.is_load_states_from_db:
            states = {'state': {'tags': {'signal': 'SOC'}}}
            self.load_states_from_db(states)

        if pars['use_internal_controller']:
            pars['controller']['ts'] = self.dt
            self.battery_controller = BatteryController(pars=pars['controller'])
        self.Pb_old = 0

    def simulate(self, t_start=0, t_end=15*60):
        """ Perform a simulation step

        :param t_start: starting time of simulation, default = 0
        :param t_end: final time of simulation
        :type t_start: int
        :type t_end: int
        :return: p_real, injected and drawn power by the battery
        :rtype: numpy ndarray
        """

        force_off = self.read_internal_dataset(t_start, t_end, 'force_off')
        p_set = force_off
        P_m = np.asarray(np.real(-sum(self.meter.get_my_powers())))
        # print('P_m = %f' % P_m)
        if abs(P_m) > 1e9:
            P_m = 0

        P_m_no_batt = P_m - self.Pb_old

        # TODO manage force_off non continuous

        if any(np.isnan(p_set)):
            if self.pars['use_internal_controller']:
                self.battery_controller.update_forecast(P_m, self.Pb_old)
                p_set, p_sol = self.battery_controller.solve_step()
            else:
                p_set = 0
                p_sol = 0

        # self.battery_controller.forecaster.predict(Pm)
        dt = self.dt

        n_steps = int((t_end - t_start) / dt) + 1
        y = np.zeros(n_steps+1)

        y[0] = self.state

        # initialize q hysteresis vector, rename parameters for speed up (in case more than one step is solved)
        p_in = np.zeros(n_steps+1)  # power in
        p_out = np.zeros(n_steps+1)  # power out
        p_real = np.zeros(n_steps+1)  # power realized

        if not(isinstance(p_set, (list, tuple, np.ndarray))):
            p_set = np.ones(n_steps)*p_set

        t0 = time()

        dt_int = int(dt)
        for i in range(0, n_steps):
            # compute the incoming power if needed
            p_in[i] = min(max(p_set[i], 0), self.pars['p_max_charge'])
            p_out[i] = min(max(-p_set[i], 0), self.pars['p_max_discharge'])

            if y[i] <= 0:
                p_out[i] = 0
            elif y[i] >= self.pars['E_max']:
                p_in[i] = 0

            if p_in[i] > 0:
                y[i+1] = y[i] + self.pars['eff_charge']*p_in[i]/3600*dt_int
                if y[i+1] > self.pars['E_max']:
                    y[i + 1] = self.pars['E_max']
                    p_real[i] = (y[i + 1]-y[i]) / self.pars['eff_charge']*3600/dt_int
                else:
                    p_real[i] = p_in[i]
            elif p_out[i] > 0:
                y[i + 1] = y[i] - p_out[i]/3600*dt_int/self.pars['eff_discharge']
                if y[i+1] < self.pars['E_min']:
                    y[i + 1] = self.pars['E_min']
                    p_real[i] = (y[i]-y[i + 1]) * self.pars['eff_discharge']*3600/dt_int
                else:
                    p_real[i] = -p_out[i]
            else:
                y[i + 1] = y[i]

        elapsed_time = time() - t0

        # save the state and time
        p = np.atleast_1d(p_in-p_out)
        q = np.sqrt(1-self.descriptor['pars']['cos_phi']**2)/self.descriptor['pars']['cos_phi'] * p

        p_all = np.zeros((n_steps, 3))
        q_all = np.zeros((n_steps, 3))

        for phase in self.phases:
            p_all[:, self.phases_num[phase]] = p[:-1] / self.num_phases
            q_all[:, self.phases_num[phase]] = q[:-1] / self.num_phases

        self.state = y[-1]
        self.time = t_end

        # write on pq_buffer
        self.write_pq_buffer(p_all, q_all, t_start, t_end)

        # write data on internal buffer
        buffer_dict = {'p_tot': p[:-1],
                       'q_tot': q[:-1],
                       'p_a': p_all[:, 0],
                       'p_b': p_all[:, 1],
                       'p_c': p_all[:, 2],
                       'q_a': q_all[:, 0],
                       'q_b': q_all[:, 1],
                       'q_c': q_all[:, 2],
                       # 'SOC': (self.state-self.pars['E_min'])/(self.pars['E_max']-self.pars['E_min']),
                       'SOC': self.state,
                       'SOC_controller': self.battery_controller.x_start.value[0] if self.pars['use_internal_controller'] else self.state,
                       'p_main': P_m,
                       'p_main_no_batt': P_m_no_batt,
                       'force_off': force_off
                       }

        # write in the current buffer
        dt = self.dt
        times = np.arange(t_start, t_end + dt, dt)
        self.increment_buffer(buffer_dict, times)

        if self.sim_pars['do_plots']:
            self.plots(y, p_real, p_sol)

        self.Pb_old = p[-2]
        if self.pars['use_internal_controller']:
            self.battery_controller.x_start.value[0] = self.state

        return p_real, self.state

    def propagate_datamanager(self):
        """ Propagate the datamanager instance to heating element """
        self.heating_element.data_manager = self.data_manager
        self.heating_element.propagate_datamanager()

    def plots(self, y, p_real, p_set):
        """
        Show the plots of energy, setpoint and measurement power

        :param y: Energy [kWh]
        :param p_real: Measured power [W]
        :param p_set: Setpoint [W]
        :type y: Numpy array
        :type p_real: Numpy array
        :type p_set: Numpy array
        """
        fig = plt.gcf()
        fig.clear()
        #fig = plt.figure()
        ax = fig.add_subplot(211)
        plt.hlines(self.pars['E_min'], 0, len(y), label='Emin')
        plt.hlines(self.pars['E_max'], 0, len(y), 'g', label='Emax')
        ax.plot(y, '-', label='E')
        plt.ylabel('E [kWh]')
        plt.xlabel('steps')

        plt.legend()
        ax.grid()

        ax = fig.add_subplot(212)
        plt.hlines(self.pars['p_max_charge'], 0, len(y), label='P_in_max')
        plt.hlines(-self.pars['p_max_discharge'], 0, len(y), 'g', label='P_out_max')
        ax.plot(p_set, '-', label='P_set')
        ax.plot(p_real, '-', label='P_real')
        plt.ylabel('P [W]')
        plt.xlabel('steps')
        plt.legend()
        ax.grid()
        plt.pause(0.001)
        fig.canvas.draw()
        fig.canvas.flush_events()

