import numpy as np
from collections import defaultdict
from scipy.sparse import csc_matrix

import krangpower as kp


# <editor-fold desc="Auxiliary functions and classes">
def _keychain(dictio: dict, keys: list, value):
    """Adds a value to a dictionary-like structure under a sequence of nested, keyed dicts.

    example:
    >>> x = {}
    >>> _keychain(x, ['foo', 3, 'bar'], 2.3)
    >>> x['foo'][3]['bar']
    2.3
    >>> _keychain(x, ['foo', 3, 'bar', 'boo'], 'overwrite_test')  #can't add other nested keys here!
    Traceback (most recent call last):
    ...
    ValueError: You tried to overwrite existing dictionary entry: 2.3
    >>> _keychain(x, ['foo', 3, 'lin'], 'bifurcation_test')  # it's ok to reuse existing keys, as long as they contain dicts
    >>> x['foo'][3]['lin']
    'bifurcation_test'
    """

    subdict = dictio
    for k in keys[0:-1]:
        try:
            if k not in subdict.keys():
                subdict[k] = {}
        except AttributeError:
            raise ValueError('You tried to overwrite existing dictionary entry: {}'.format(str(subdict)))
        subdict = subdict[k]

    try:
        subdict[keys[-1]] = value
    except TypeError:
        raise ValueError('You tried to overwrite existing dictionary entry: {}'.format(str(subdict)))


# --------------------------------------------------------------------------- #
# Graphviews for extracting data
# --------------------------------------------------------------------------- #
class PhasePowerView(kp.GraphView):
    def __init__(self, ckgr: kp.Krang):

        def nodepower(bus):
            pwr_dic = {}  # 0.0 * kp.UM.kW

            if bus.get('el', None) is not None:
                for el in bus['el']:
                    # here i am guaranteed that the element has one bus and one or more terminals
                    trm = el.NodeOrder().flatten()
                    pwr = el.Powers().flatten()
                    for t, p in zip(trm, pwr):
                        if t == 0:
                            pass
                        else:
                            try:
                                pwr_dic[t] += p
                            except KeyError:
                                pwr_dic[t] = p

                total_phases = np.max(list(pwr_dic.keys()))
                power_array = np.zeros((1, total_phases), dtype=np.complex)

                for phase, totpow in pwr_dic.items():
                    power_array[0, phase - 1] = totpow.magnitude

                return power_array * kp.UM.kW
            else:
                return np.zeros((1, 3), dtype=np.complex) * kp.UM.kW

        super().__init__(nodepower, None, ckgr)


# </editor-fold>


def losses(kr):
    return kr.brain.Circuit.Losses().magnitude


losses.indicization = []


def totpow(kr):
    return kr.brain.Circuit.TotalPower().magnitude


totpow.indicization = []


def V_node(kr):
    raw_v = kr.brain.Circuit.YNodeVArray()
    result = dict()
    for k, v in raw_v.items():
        node, phase = k.split('.')
        phase = 'p' + phase  # this is to prevent infuxdb's bug when receiving ints as strings
        _keychain(result, [node, phase], v.magnitude)
    return result


V_node.indicization = ['node', 'phase']


def vstar_meter(kr, m2g):
    mvolt = defaultdict(lambda: np.zeros(4, dtype=np.complex128))
    for meter, gen_names in m2g.items():
        for gen in gen_names:
            for node, voltage in zip(kr[gen].NodeOrder(), kr[gen].Voltages().magnitude):
                mvolt[meter][node] += voltage

        mvolt[meter] = mvolt[meter][1:]

    return dict(mvolt)


def vstar_meter_mag(kr, m2g):
    rslt = defaultdict(dict)
    mp = vstar_meter(kr, m2g)
    for meter, lol in mp.items():
        rslt[meter] = np.absolute(lol)
    return dict(rslt)


def vstar_unpack(kr, m2g):
    rslt = defaultdict(dict)
    mp = vstar_meter(kr, m2g)
    for meter, lol in mp.items():
        for phase_index, pq in enumerate(lol):
            rslt[meter]['p' + str(phase_index)] = pq

    return dict(rslt)


def I_inj(kr):
    raw_I = kr.brain.Circuit.YCurrents()
    result = dict()
    for k, v in raw_I.items():
        node, phase = k.split('.')
        phase = 'p' + phase
        _keychain(result, [node, phase], v.magnitude)

    return result
    # no = kr.brain.Circuit.AllNodeNames()
    # vdict = kr.brain.Circuit.YNodeVArray()
    # V = np.transpose(np.asarray([vdict[k].magnitude for k in no]))
    #
    # return dict(zip(no, kr.Ybus_noload() * V * kp.UM.A))


I_inj.indicization = ['node', 'phase']


def Sn(kr):
    true_power_pp = PhasePowerView(kr).get_node_dict(convert_to_unit='kW')
    result = dict()
    for node, v3 in true_power_pp.items():
        for phase in range(len(v3[0])):
            _keychain(result,
                      [node, 'p' + str(phase)],
                      v3[0][phase])
    return result


Sn.indicization = ['node', 'phase']


# note: NON STANDARD (FULLY INDICIZED) OUTPUT
def meter_power(kr, m2g):
    mpow = defaultdict(lambda: np.zeros(4, dtype=np.complex128))
    for meter, gen_names in m2g.items():
        for gen in gen_names:
            for node, power in zip(kr[gen].NodeOrder(), kr[gen].Powers().magnitude):
                mpow[meter][node] += power

        mpow[meter] = mpow[meter][1:]

    # mpow1 = {meter: sum(mpow[meter]) for meter in m2g.keys()}

    return dict(mpow)


def meter_power_unpack(kr, m2g):
    rslt = defaultdict(dict)
    mp = meter_power(kr, m2g)
    for meter, lol in mp.items():
        for phase_index, pq in enumerate(lol):
            rslt[meter]['p' + str(phase_index)] = pq

    return dict(rslt)


def all_meters_powers(kr, m2g):
    mp = meter_power(kr, m2g)
    e_pos = 0
    e_neg = 0
    for meter, lol in mp.items():
        e_tot = np.sum(lol.real)
        if e_tot >= 0:
            e_pos += e_tot
        else:
            e_neg -= e_tot
    return {'E_pos': e_pos, 'E_neg': e_neg}


def meter_voltage(kr, m2n):
    mvolt = {}
    for meter, busname in m2n.items():
        mvolt[meter] = kr[busname].Voltages()

    return mvolt


def meter_power_old(kr, nlist, bndic):
    pgr = kp.gv.BusTotPowerView(kr)
    nda = pgr.get_node_dict('kW')
    nds = {}
    for k, v in nda.items():
        if k in nlist:
            nds[bndic[k]] = v
    return nds


# note: NON STANDARD (FULLY INDICIZED) OUTPUT
# look below for the partialization!


def mtr2node(mm):
    result = {}
    meters = mm.config['meters']
    for m in meters:
        result[m['name']] = m['bus']
    return result


def retr_nlist(mm):
    result = []
    meters = mm.config['meters']
    for m in meters:
        result.append(m['bus'])
    return result


def retr_bus_node_dict(mm):
    bndic = {}
    for meter in mm.meters:
        bndic[meter['bus']] = meter['name']
    return bndic


def gens_of_meter(mm):
    m2g = {}
    for meter in mm.meters:
        mname = meter['name']
        m2g[mname] = mm.mtr_2_gen[mname].values()

    return m2g


meter_power.indicization = ['meter']
meter_power.partialization = {'m2g': gens_of_meter}
all_meters_powers.indicization = ['meter']
all_meters_powers.partialization = {'m2g': gens_of_meter}
meter_power_old.indicization = ['meter']
meter_power_old.partialization = {'nlist': retr_nlist, 'bndic': retr_bus_node_dict}
meter_voltage.indicization = ['meter']
meter_voltage.partialization = {'m2n': mtr2node}
meter_power_unpack.indicization = ['meter_id', 'phase']
meter_power_unpack.partialization = {'m2g': gens_of_meter}
vstar_meter.indicization = ['meter']
vstar_meter.partialization = {'m2g': gens_of_meter}
vstar_meter_mag.indicization = ['meter']
vstar_meter_mag.partialization = {'m2g': gens_of_meter}
vstar_unpack.indicization = ['meter_id', 'phase']
vstar_unpack.partialization = {'m2g': gens_of_meter}


# note: NON STANDARD (FULLY INDICIZED) OUTPUT
def traf_power(kr):
    rslt = {}
    an = kr.brain.get_all_names()
    trf = [x for x in an if x.split('.')[0] == 'transformer']

    for t in trf:
        terminals = kr[t].NodeOrder()
        out_power = kr[t].Powers().magnitude[1].tolist()  # 1 bc it's the output
        out_power_neut = [- x for i, x in enumerate(out_power) if terminals[1][i] != 0]
        rslt[t] = out_power_neut
    return rslt


# note: NON STANDARD (FULLY INDICIZED) OUTPUT
traf_power.indicization = ['transformer']


def traf_power_unpack(kr):
    rslt = defaultdict(dict)
    tp = traf_power(kr)
    for trafo, lol in tp.items():
        for phase_index, pq in enumerate(lol):
            rslt[trafo]['p' + str(phase_index)] = pq

    return dict(rslt)


traf_power_unpack.indicization = ['transformer', 'phase']


def I_edge(kr):
    edge_current_pp = kp.gv.EdgeCurrentView(kr).get_edge_dict(convert_to_unit='A')
    result = dict()
    for edge, v3 in edge_current_pp.items():
        for phase in range(len(v3[0])):
            # edge_repr = '-->'.join([str(x) for x in edge])
            _keychain(result,
                      [edge[0], edge[1], 'p' + str(phase)],
                      v3[0][phase])
    return result


I_edge.indicization = ['edge_start', 'edge_end', 'phase']


def allvmag(kr):
    return {k: v.magnitude for k, v in kr.brain.Circuit.AllBusVMag().items()}


def extrcurr(kr):
    pgr = kp.gv.BusTotCurrentView(kr)
    nda = pgr.get_node_dict('A')
    nds = {}
    for k, v in nda.items():
        for phase, single_v in enumerate(v):
            if phase == len(v):
                continue
            nds[k + '.' + str(phase + 1)] = single_v

    # nds = {k: v for k, v in nda.items() if k in loaded_buses}
    pass
    return nds


def extrpow(kr):
    pgr = kp.gv.BusTotPowerView(kr)
    nda = pgr.get_node_dict('kW')
    nds = {}
    for k, v in nda.items():
        for phase, single_v in enumerate(v):
            nds[k + '.' + str(phase + 1)] = single_v
    return nds


def ymatrix(kr):
    return csc_matrix(kr.brain.Circuit.SystemY().values)


def ycurrents(kr):
    return {k: v.magnitude for k, v in kr.brain.Circuit.YCurrents().items()}


def yvolts(kr):
    return {k: v.magnitude for k, v in kr.brain.Circuit.YNodeVArray().items()}
