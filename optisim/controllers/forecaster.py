class Forecaster:
    def __init__(self):
        pass

    def train(self, y):
        raise NotImplementedError("Must be overridden")

    def feed(self, y):
        raise NotImplementedError("Must be overridden")

    def predict(self, y):
        raise NotImplementedError("Must be overridden")
