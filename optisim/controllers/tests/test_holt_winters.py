import numpy as np
import matplotlib.pyplot as plt
from optisim.controllers.holt_winters import HoltWinters


sum_stp = 0
expend = 2.7
h = 96
steps = 0
while sum_stp != h:
    steps = np.asanyarray(np.logspace(0,expend,15),int)
    sum_stp = np.sum(steps)
    if sum_stp > h:
        expend = expend-0.00001
    else:
        expend = expend + 0.00001

#steps = list(np.ones(96, int))
alphas = []
betas = []
gammas = []
for i in range(len(steps)):
    if i < 3:
        alphas.append(0.1)
        gammas.append([0.5, 0.5])
    else:
        alphas.append(0.01)
        gammas.append([0.5, 0.5])

    betas.append(0.0)

pars = {'step_length': 1,
        'steps': steps,
        'alphas': alphas,
        'betas': betas,
        'gammas': gammas,
        'period': [96, 96*7],
        'method': 'add',
        'history_length': 0,
        'history_offset': 0}

hw = HoltWinters(pars)


# create sin with 96 steps period

ys = np.sin(2*np.pi/96*np.array(range(0, 96*7))) > 0
weekend = np.concatenate((np.zeros(96*5), np.ones(96*2)))
ys = ys + weekend

hw.train(np.tile(ys, 100))

ys = np.tile(ys, 100)

for i, y in enumerate(ys):
        y_fut = ys[i+1:i+97]
        k = 0
        y_real = np.zeros(len(steps))
        for j, step in enumerate(steps):
            y_real[j] = np.mean(y_fut[k:k+step])
            k += step

        y_hat = hw.predict(y)

        if i % 100 == 0:
            plt.cla()
            plt.plot(y_real)
            plt.plot(y_hat)
            plt.pause(0.001)

#plt.plot(ys)
#plt.show()