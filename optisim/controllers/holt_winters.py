from .forecaster import Forecaster
import numpy as np


class HoltWinters(Forecaster):

    def __init__(self, pars):

        self.step_length = pars['step_length']
        self.history_length = pars['history_length']
        self.history_offset = pars['history_offset']
        self.history_ix = 0
        steps = pars['steps']
        alphas = pars['alphas']
        betas = pars['betas']
        gammas = pars['gammas']
        period = pars['period']
        method = pars['method']

        if self.history_length > 0:
            self.history = np.zeros(period[-1] * self.history_length)
            self.collect_history = True
        else:
            self.history = None
            self.collect_history = False

        i = 1
        self.hw = []
        for step, alpha, beta, gamma in zip(steps, alphas, betas, gammas):
            horizon = np.array(range(i, i+step))
            i += step
            self.hw.append(_HoltWinters(alpha, beta, np.array(gamma), np.array(period), horizon, method))

            super().__init__()

    def train(self, y):
        # print('training started...')
        for i, hw in enumerate(self.hw):
            # print('training HW %d/%d' % (i+1, len(self.hw)))
            hw.train(y)

    def predict(self, y):
        # keep internal buffer to train
        if self.collect_history:
            self.history_ix += 1
            self.history = np.roll(self.history, -1)
            self.history[-1] = y
            if self.history_ix == (len(self.history) + self.history_offset):
                self.collect_history = False
                self.train(np.tile(self.history, 100))

        # print('predict')

        y_hat = np.zeros(len(self.hw))
        for i, hw in enumerate(self.hw):
            y_hat_s = hw.predict(y)
            y_hat[i] = np.mean(y_hat_s)
            #print(y_hat[i])
        return y_hat

class _HoltWinters:
    """
    Holt_Winters class
    Attributes:
    """

    def __init__(self, alpha=0.1, beta=1e-5, gamma=np.array([0.3]), period=np.array([96]),
                 horizon=np.array(range(1, 97)), method='add'):
        """
        Constructor
        :param alpha:
        :type alpha: float
        :param beta:
        :type bety: float
        :param gamma:
        :type gamma: numpy ndarray
        :param period:
        :type period: numpy ndarray
        :param horizon
        :type horizon: numpy ndarray
        :param method:
        :type method: str
        """

        assert 0 < period.size <= 2, "wrong period size (the class accepts 1 or 2 seasons)"
        assert period.size == gamma.size, "gamma and period must have the same length"
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        self.method = method
        self.period = period
        self.horizon = horizon

        self.am1 = 0
        self.bm1 = 0
        self.s1mp = np.zeros(self.period[0])
        if self.period.size == 2:
            self.s2mp = np.zeros(self.period[1])

    def train(self, y_history):
        """
        Fit
        :param y_history: y history
        :type y_history: numpy ndarray
        """
        if self.method == 'add':
            self._fit_add(y_history)

    def _fit_add(self, y_history):
        """
        Fit additive
        :param y_history: y history
        :type y_history: numpy ndarray
        """
        if self.period.size == 1:
            a = np.zeros(y_history.size)
            b = np.zeros(y_history.size)
            s = np.zeros(y_history.size)
            for i in range(max(self.period), y_history.size):
                a[i] = self.alpha * (y_history[i] - s[i - self.period[0]]) + (1 - self.alpha) * (
                            a[i - 1] + b[i - 1])
                b[i] = self.beta * (a[i] - a[i - 1]) + (1 - self.beta) * b[i - 1]
                # s[i] = self.gamma[0] * (y_history[i] - a[i-1] - b[i-1]) + (1 - self.gamma[0]) * s[i - self.period[0]]
                s[i] = self.gamma[0] * (y_history[i] - a[i]) + (1 - self.gamma[0]) * s[i - self.period[0]]
                # y_hat = np.zeros(self.horizon.size)
                # for j, h in enumerate(self.horizon):
                #    y_hat[j] = a[i] + h * b[i] + s[i - self.period[0] + (h - 1) % self.period[0]+1]
            self.am1 = a[-1]
            self.bm1 = b[-1]
            self.s1mp = s[-self.period[0]:]
        else:
            a = np.zeros(y_history.size)
            b = np.zeros(y_history.size)
            s1 = np.zeros(y_history.size)
            s2 = np.zeros(y_history.size)
            for i in range(max(self.period), y_history.size):
                a[i] = self.alpha * (y_history[i] - s1[i - self.period[0]] - s2[i - self.period[1]]) + (
                            1 - self.alpha) * (a[i - 1] + b[i - 1])
                b[i] = self.beta * (a[i] - a[i - 1]) + (1 - self.beta) * b[i - 1]
                s1[i] = self.gamma[0] * (y_history[i] - a[i] - s2[i - self.period[1]]) + (1 - self.gamma[0]) * \
                        s1[i - self.period[0]]
                s2[i] = self.gamma[1] * (y_history[i] - a[i] - s1[i - self.period[0]]) + (1 - self.gamma[1]) * \
                        s2[i - self.period[1]]
            self.am1 = a[-1]
            self.bm1 = b[-1]
            self.s1mp = s1[-self.period[0]:]
            self.s2mp = s2[-self.period[1]:]

    def predict(self, y):
        """
        Predict
        :param y: y
        :type y: numpy ndarray
        """
        if self.method == 'add':
            return self._predict_add(y)

    def _predict_add(self, y):
        """
        Predict additive
        :param y: y
        :type y: numpy ndarray
        """
        y_hat = np.zeros(self.horizon.size)
        if self.period.size == 1:
            a = self.alpha * (y - self.s1mp[0]) + (1 - self.alpha) * (self.am1 + self.bm1)
            b = self.beta * (a - self.am1) + (1 - self.beta) * self.bm1
            # s = self.gamma[0] * (y - self.am1 -self.bm1) + (1 - self.gamma[0]) * self.s1mp[0]
            s = self.gamma[0] * (y - a) + (1 - self.gamma[0]) * self.s1mp[0]
            self.s1mp = np.roll(self.s1mp, -1)
            self.s1mp[-1] = s
            self.am1 = a
            self.bm1 = b
            for i, h in enumerate(self.horizon):
                y_hat[i] = a + h * b + self.s1mp[(h - 1) % self.period[0]]
        else:
            a = self.alpha * (y - self.s1mp[0] - self.s2mp[0]) + (1 - self.alpha) * (self.am1 + self.bm1)
            b = self.beta * (a - self.am1) + (1 - self.beta) * self.bm1
            s1 = self.gamma[0] * (y - a - self.s2mp[0]) + (1 - self.gamma[0]) * self.s1mp[0]
            s2 = self.gamma[1] * (y - a - self.s1mp[0]) + (1 - self.gamma[1]) * self.s2mp[0]
            self.s1mp = np.roll(self.s1mp, -1)
            self.s1mp[-1] = s1
            self.s2mp = np.roll(self.s2mp, -1)
            self.s2mp[-1] = s2
            self.am1 = a
            self.bm1 = b
            for i, h in enumerate(self.horizon):
                y_hat[i] = a + h * b + self.s1mp[(h - 1) % self.period[0]] + self.s2mp[(h - 1) % self.period[1]]
        return y_hat