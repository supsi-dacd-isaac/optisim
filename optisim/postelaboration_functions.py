import json
from scipy import io
import numpy as np

_dateformat_influx = '%Y-%m-%dT%H:%M:%SZ'


def fake_value(V_node):
    # print(V_node)

    return V_node['valueRe']*1.2, \
           V_node['tags'], \
           V_node['time']


def I_inj(V_node, Ybus_mat_file, ynod_json_file):

    Y = io.loadmat(Ybus_mat_file)['Ybus']

    with open(ynod_json_file, 'r') as f:
        ynod = json.load(f)
    ynod = [x.lower() for x in ynod]

    unique_times = iter(V_node.index.unique())

    def create_vector(V_node, time):

        vl = np.zeros((len(ynod)), dtype=np.complex)

        now_df = V_node.iloc[V_node.index == time]

        for _, row in now_df.iterrows():
            fullbus = row.node + '.' + row.phase[1:]
            idx = ynod.index(fullbus)
            vl[idx] = getattr(row, 'valueRe') + 1j * getattr(row, 'valueIm')

        return vl

    for ut in unique_times:
        vees = create_vector(V_node, ut)
        iinj = Y * vees

        for idx, ivalue in enumerate(iinj):
            node, phase = ynod[idx].split('.')
            phase = 'p' + phase

            yield ivalue, \
                  {'node': node, 'phase': phase}, \
                  ut.strftime(_dateformat_influx)


I_inj.V_node = "SELECT * FROM {0} WHERE signal = 'V_node' and time >= '{1}' and time < '{2}' order by time"


def I_by_edge(I_edge):

    unique_times = iter(I_edge.index.unique())
    for ut in unique_times:
        now_df = I_edge.iloc[I_edge.index == ut]
        for _, row in now_df.iterrows():
            yield row['valueRe'] + 1j * row['valueIm'], \
                  {'edge': row['edge_start'] + '-->' + row['edge_end'],
                   'phase': row['phase']}, \
                  ut.strftime(_dateformat_influx)


I_by_edge.I_edge = "SELECT * FROM {0} WHERE signal = 'I_edge' and time >= '{1}' and time < '{2}' order by time"
