# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #
import glob
import json
from os import path


# --------------------------------------------------------------------------- #
# Classes
# --------------------------------------------------------------------------- #
class ConfigurationFitter:
    def __init__(self, config_file: str):
        """ Constructor
        :param config_file: file of the primary configuration path
        :type config_file: str
        :param logger: logger object
        :type logger: logger
        """
        self.config_file = config_file
        # initialize the fitted configuration with the one contained in primary config file
        self.fitted_config = json.loads(open(self.config_file).read())
        self.components_templates = None

    def set_fitted_config(self):
        """ Set the fitted configuration according to the template files and the customized parameters """
        # define the connections settings
        self.set_conns()

        # define properly the input output/section
        self.set_fitted_io()

        # define the fitted components
        self.set_fitted_components()

    def set_conns(self):
        """ Set the connections settings """
        conns_config = json.loads(open(self.fitted_config['simulation']['connectionsFile']).read())
        for key in conns_config.keys():
            self.fitted_config[key] = conns_config[key]

    def set_fitted_io(self):
        internal_id = self.fitted_config['simulation']['internalID'] \
            if 'internalID' in self.fitted_config['simulation'].keys() \
            else self.fitted_config['simulation']['ID']

        """ Set the fitted I/O sections """
        # cycle over the I/O instances
        for io in self.fitted_config['input_output']:
            # check if the I/O is related to the feedback between sim -> algo -> sim or to the data savings in InfluxDB

            # 'internal' case
            if io['type'] == 'internal':
                # append the simulation ID to the topic name
                io['topic'] = '%s_%s' % (io['topic'], internal_id)

                # check if a measurement header is defined for the I/O
                if io['measurement'] is not None:
                    # append the simulation ID to the measurement name where the data will be saved
                    io['measurement_base'] = io['measurement']
                    io['measurement'] = '%s_%s' % (io['measurement'], self.fitted_config["simulation"]["ID"])

            # 'external_to_db' case
            elif io['type'] == 'external_to_db':
                # check if a measurement header is defined for the I/O
                if io['measurement'] is not None:
                    # append the simulation ID to the measurement name where the data will be saved
                    io['measurement_base'] = io['measurement']
                    io['measurement'] = '%s_%s' % (io['measurement'], self.fitted_config["simulation"]["ID"])

            # the exchange "simulation_exchange" is just a placeholder for the real exchange, which has the name
            # of the simulaiton itself.

            if io['exchange'] == 'simulation_exchange':
                io['exchange'] = internal_id

    def set_fitted_components(self):
        """ Set the fitted components sections """
        # get data about the custom configuration
        custom_config = json.loads(open(self.config_file).read())

        # define templates pars
        self.__set_components_templates(input_folder=self.fitted_config['simulation']['templateComponentParsFolder'])

        # cycle over the meters/components to set the default parameters
        for meter in self.fitted_config['meters']:
            for component in meter['components']:
                # handle generic parameters
                self.set_default_pars(target_elem=component, src_elem=self.components_templates[component['type']])

                # handle heating parameters
                self.set_default_heating_pars(elem=component)

        # cycle over the meters/components to set the custom parameters
        for i in range(len(self.fitted_config['meters'])):
            for j in range(len(self.fitted_config['meters'][i]['components'])):
                # handle parameters
                self.set_default_pars(target_elem=self.fitted_config['meters'][i]['components'][j],
                                      src_elem=custom_config['meters'][i]['components'][j])

                # handle heating elements
                self.set_custom_heating_pars(fitted_elem=self.fitted_config['meters'][i]['components'][j],
                                             custom_elem=custom_config['meters'][i]['components'][j])

    def set_default_pars(self, target_elem: dict, src_elem: dict):
        """ Set the default heating parameters
        :param target_elem: component element to set
        :type target_elem: dict
        :param src_elem: source element
        :type src_elem: dict
        """
        # if a template is available, then initialize/update the pars dictionary
        if target_elem['type'] in src_elem.keys():
            if target_elem['pars'] is None:
                target_elem['pars'] = src_elem[target_elem['type']]['pars']
            else:
                target_elem['pars'].update(src_elem[target_elem['type']]['pars'])

    def set_default_heating_pars(self, elem: dict):
        """ Set the default heating parameters
        :param elem: component element
        :type elem: dict
        """
        # if a template is available, then initialize/update the pars dictionary
        if elem['type'] in self.components_templates.keys():
            if elem['pars'] is None:
                elem['pars'] = self.components_templates[elem['type']]['pars']
            else:
                elem['pars'].update(self.components_templates[elem['type']]['pars'])

        # iterates recursively searching all the 'heating_element' and 'dhw' components
        if 'heating_element' in elem.keys() and elem['heating_element'] is not None:
            return self.set_default_heating_pars(elem['heating_element'])
        elif 'dhw' in elem.keys() and elem['dhw'] is not None:
            return self.set_default_heating_pars(elem['dhw'])
        else:
            return None

    def set_custom_heating_pars(self, fitted_elem: dict, custom_elem: dict):
        """ Set the custom heating parameters
        :param fitted_elem: component element that has to be fitted
        :type fitted_elem: dict
        :param custom_elem: component element with customized parameters
        :type custom_elem: dict
        """
        # if a template is available, then apply the custom pars
        if fitted_elem['type'] in self.components_templates.keys():
            if custom_elem['pars'] is not None:
                fitted_elem['pars'].update(custom_elem['pars'])

        # iterates recursively searching all the 'heating_element' and 'dhw' components
        if 'heating_element' in fitted_elem.keys() and fitted_elem['heating_element'] is not None:
            return self.set_custom_heating_pars(fitted_elem['heating_element'], custom_elem['heating_element'])
        elif 'dhw' in fitted_elem.keys() and fitted_elem['dhw'] is not None:
            return self.set_custom_heating_pars(fitted_elem['dhw'], custom_elem['dhw'])
        else:
            return None

    def __set_components_templates(self, input_folder: str):
        """ Set the template pars data
        :param input_folder: folder where the json templates are located
        :type input_folder: str
        """
        self.components_templates = dict()
        for template_path in glob.glob('%s/*.json' % input_folder):
            (component_type, ext) = path.splitext(path.basename(template_path))
            self.components_templates[component_type] = json.loads(open(template_path).read())

