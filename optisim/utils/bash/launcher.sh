#!/usr/bin/env bash

help() {
  echo "Available commands:"
  echo ""
  echo "1) ODM: launch output_data_manager.py"
  echo "    Usage (CONSOLE mode):    ./launcher.sh -i CONF_FILE -c ODM"
  echo "    Example:                 ./launcher.sh -i opticonf/connections/conns_local_influx_local_rabbit.json -c ODM"
  echo ""
  echo "    Usage (BACKGROUND mode): ./launcher.sh -i CONF_FILE -c ODM -l LOG_FOLDER"
  echo "    Example:                 ./launcher.sh -i small_ckt.json -c ODM -l ~/logs"
  echo ""
  echo "3) SG: launch sim_grid.py"
  echo "    Usage (CONSOLE mode):    ./launcher.sh -i CONF_FILE -c SG"
  echo "    Example:                 ./launcher.sh -i opticonf/simulation_conf/small_ckt.json -c SG"
  echo ""
  echo "    Usage (BACKGROUND mode): ./launcher.sh -i CONF_FILE -s SG -l LOG_FOLDER"
  echo "    Example:                 ./launcher.sh -i opticonf/simulation_conf/small_ckt.json -s SG -l ~/logs"
  echo ""
  echo "5) KILL: kill all the python processes related to CONF_FILE file"
  echo "    Usage:                   ./launcher.sh -i CONF_FILE -c KILL"
  echo "    Example (ODM):           ./launcher.sh -i opticonf/connections/conns_local_influx_local_rabbit.json -c KILL"
  echo "    Example (SG):         ./launcher.sh -i opticonf/simulation_conf/small_ckt.json -c KILL"
  echo ""
}

launch_odm() {
  echo "$(date +"%Y-%m-%d %H:%M:%S") - ODM command (i.e. launch output_data_manager.py):"
  print_cfg_file $ODM_CFG_FILE

  if [ ${LOG_FLD} ]; then
      echo "Mode:               BACKGROUND"
      echo "Log file:           "$LOG_ODM
      echo ""
      nohup $OPTISIM_VENV_FLD $ODM_CMD -c $ODM_CFG_FILE -l $LOG_ODM >/dev/null 2>&1 &
      echo "$(date +"%Y-%m-%d %H:%M:%S") - ODM has been started"
      echo ""
  else
      echo "Mode:               CONSOLE"
      $OPTISIM_VENV_FLD $ODM_CMD -c $ODM_CFG_FILE
  fi
}

launch_sg() {
  echo "$(date +"%Y-%m-%d %H:%M:%S") - SG command (i.e. launch sim_grid.py):"
  print_cfg_file $CFG_FILE

  if [ ${LOG_FLD} ]; then
    echo "Mode:               BACKGROUND"
    echo "Log file:           "$LOG_SG
    echo ""
    nohup $OPTISIM_VENV_FLD $SG_CMD -c $INPUT_CFG_FILE -l $LOG_SG >/dev/null 2>&1 &
    echo "$(date +"%Y-%m-%d %H:%M:%S") - SG has been started"
    echo ""
  else
      echo "Mode:             CONSOLE"
      $OPTISIM_VENV_FLD $SG_CMD -c $INPUT_CFG_FILE
  fi
}

wait5s() {
  echo ""
  echo "$(date +"%Y-%m-%d %H:%M:%S") - Wait 5 seconds before starting..."
  sleep 5
  echo ""
}

print_cfg_file() {
  echo ""
  echo "Configuration file: "$1
}

kill() {
  echo ""
  echo "$(date +"%Y-%m-%d %H:%M:%S") - Send SIGTERM signal to all the Python processes related to $1 string"
  echo ""
  if [[ $(ps -ef | grep python | grep $1 | awk '{print $2}') ]]; then
    ps -ef | grep python | grep $1 | awk '{print $2}' | xargs kill -15
    echo "$(date +"%Y-%m-%d %H:%M:%S") - Simulation killed"
  else
    echo "$(date +"%Y-%m-%d %H:%M:%S") - Found 0 Python scripts related to $1 string, do nothing"
  fi
  echo ""
}

while getopts c:i:l: option
do
  case "${option}" in
      c) CMD=${OPTARG};;
      i) INPUT_CFG_FILE=${OPTARG};;
      l) LOG_FLD=${OPTARG};;
esac
done

if [ -z ${CMD} ]; then
  echo ""
  echo "$(date +"%Y-%m-%d %H:%M:%S") - ERROR: Command \"$CMD\" is not available"
  echo ""
  help
  exit
fi

# Set the folders to get data about connections configuration files
CFG_FILE="../../../../"$INPUT_CFG_FILE

# Check if you are in the proper folder
if [ ! -f "$CFG_FILE" ]; then
    echo ""
    echo "$(date +"%Y-%m-%d %H:%M:%S") - ERROR: Configuration file \"$CFG_FILE\" does not exist, are you in the folder of launcher.sh?"
    echo ""
    help
    exit
fi

if [ ${CMD} = 'KILL' ]; then
  kill $INPUT_CFG_FILE
  exit
fi

if [ ${CMD} = 'ODM' ]; then
    ODM_CFG_FILE=$INPUT_CFG_FILE
else
    ODM_CFG_FILE=$(cat $CFG_FILE | jq -r '.simulation.connectionsFile')
fi

# Go on the root folder supposing to be in the launcher folder
cd "../../../../"
WD=$(pwd)

# Set the project folders
OPTICONF_FLD=$WD"/opticonf/"
OPTISIM_FLD=$WD"/optisim/"

# Set the venv folder
OPTISIM_VENV_FLD=$OPTISIM_FLD"venv/bin/python"

# Set the scripts
ODM_CMD=$OPTISIM_FLD"output_data_manager.py"
SG_CMD=$OPTISIM_FLD"sim_grid.py"

# Define the file path of the configuration files
CFG_FILE=$WD/$INPUT_CFG_FILE
ODM_CFG_FILE=$WD/$ODM_CFG_FILE

# Set $PYTHONPATH env variable
export PYTHONPATH=$OPTISIM_FLD:$OPTIALGO_FLD:$OPTICONF_FLD:$OPTIALGO_FLD

# Set the log files
if [ ${LOG_FLD} ]; then
  INPUT_CFG_FILE_NO_EXT=$(basename -- "$INPUT_CFG_FILE")
  INPUT_CFG_FILE_NO_EXT="${INPUT_CFG_FILE_NO_EXT%.*}"
  SIM_LOG_FLD=$LOG_FLD"/"$INPUT_CFG_FILE_NO_EXT
  mkdir -p $SIM_LOG_FLD

  ODM_CFG_FILE_NO_EXT=$(basename -- "$ODM_CFG_FILE")
  ODM_CFG_FILE_NO_EXT="${ODM_CFG_FILE_NO_EXT%.*}"
  ODM_LOG_FLD=$LOG_FLD"odm/"$ODM_CFG_FILE_NO_EXT
  mkdir -p $ODM_LOG_FLD

  LOG_ODM=$ODM_LOG_FLD"/ODM.log"
  LOG_SG=$SIM_LOG_FLD"/SG.log"
fi

# ALL

# ODM
if [ ${CMD} = 'ODM' ]; then
  launch_odm

# SG
elif [ ${CMD} = 'SG' ]; then
  launch_sg

else
  echo ""
  echo "$(date +"%Y-%m-%d %H:%M:%S") - ERROR: Command \"$CMD\" is not available"
  echo ""
  help
fi



