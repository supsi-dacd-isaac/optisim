import logging
import sys


class _AnsiColorizer(object):
    """
    A colorizer is an object that loosely wraps around a stream, allowing
    callers to write text to the stream in a particular color.

    Colorizer classes must implement C{supported()} and C{write(text, color)}.
    """
    _colors = dict(black=30, red=31, green=32, yellow=33,
                   blue=34, magenta=35, cyan=36, white=37)

    def __init__(self, stream):
        self.stream = stream

    @classmethod
    def supported(cls, stream=sys.stdout):
        """
        A class method that returns True if the current platform supports
        coloring terminal output using this method. Returns False otherwise.
        """
        if not stream.isatty():
            return False  # auto color only on TTYs
        try:
            import curses
        except ImportError:
            return False
        else:
            try:
                try:
                    return curses.tigetnum("colors") > 2
                except curses.error:
                    curses.setupterm()
                    return curses.tigetnum("colors") > 2
            except:
                raise

    def write(self, text, color):
        """
        Write the given text to the stream in the given color.

        @param text: Text to be written to the stream.

        @param color: A string label for a color. e.g. 'red', 'white'.
        """
        color = self._colors[color]
        self.stream.write('\x1b[%s;1m%s\x1b[0m' % (color, text))


class ColorHandler(logging.StreamHandler):
    def __init__(self, stream=sys.stderr):
        super(ColorHandler, self).__init__(_AnsiColorizer(stream))

    def emit(self, record):
        msg_colors = {
            logging.DEBUG: "blue",
            logging.INFO: "green",
            logging.WARNING: "yellow",
            logging.ERROR: "red"
        }

        color = msg_colors.get(record.levelno, "blue")
        self.stream.write(self.format(record) + "\n", color)


def setup_logger(name,
                 entry_format='%(asctime)-15s::%(levelname)s::%(name)s::%(funcName)s::%(message)s',
                 level=logging.INFO,
                 filename=None):
    """Function setup as many loggers as you want"""

    formatter = logging.Formatter(entry_format)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.propagate = False

    if len(logger.handlers) > 0:
        raise 'You are setting up the same logger twice'

    if filename is not None:
        file_handler = logging.FileHandler(filename)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    else:
        stream_handler = ColorHandler()
        stream_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)

    return logger


def get_logger_level(level_desc):
    """Get the logger level given the description
    :param level_desc: description of the log level
    :type level_desc: str
    :return: Logger level
    :rtype: Logger.Level
    """
    if level_desc == 'DEBUG':
        return logging.DEBUG
    elif level_desc == 'INFO':
        return logging.INFO
    elif level_desc == 'WARNING':
        return logging.WARNING
    elif level_desc == 'ERROR':
        return logging.ERROR
    else:
        return logging.INFO
