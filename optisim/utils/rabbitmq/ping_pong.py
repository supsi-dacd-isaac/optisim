from datetime import datetime
from multiprocessing import Queue, Process, Event

from tqdm import tqdm

from thread_rabbit_consumer import thread_rabbit_consumer
from thread_rabbit_producer import thread_rabbit_producer

config = {'rabbit': {'host': 'localhost', 'port': 5672},
          'simulation': {'startDateTime': "2018-01-01 00:00"}}

topic1 = 'theping1'
topic2 = 'theping2'

_dateformat = '%Y-%m-%d %H:%M%z'
start_time = datetime.strptime(config['simulation']['startDateTime'] +'+0000', _dateformat)

logging_conf = dict(name='meter_manager',
                    entry_format='%(asctime)-15s::%(levelname)s::%(process)d::%(name)s::%(funcName)s::%(message)s',
                    level=40,
                    filename=None)


class P_ng:

    def __init__(self, my_topic, out_topic, count=False):
        self.out_queue = Queue(1)
        self.in_queue = Queue(1)
        self.cr = Event()
        self.my_topic = my_topic
        self.out_topic = out_topic

        self.prod = Process(target=thread_rabbit_producer, args=(self.out_queue, config, start_time, logging_conf))
        self.cons = Process(target=thread_rabbit_consumer, args=(self.my_topic, self.in_queue, self.cr, 0, logging_conf, config['rabbit']))
        self.player = None
        self.pbar = None
        self.prod.start()
        self.cons.start()
        self.count = count
        self.cr.wait()
        self.is_started = Event()

    def play(self):
        self.player = Process(target=self._ready, args=())
        self.player.start()
        self.is_started.set()

    def _ready(self):
        if self.count:
            self.pbar = tqdm(unit=' msg')
        while True:
            msg = self.in_queue.get(block=True)
            counter = msg['miao'] + 1
            if self.pbar is not None:
                self.pbar.update(1)
            self.out_queue.put({'function': 'send', 'args': [{'miao': counter}, 0, self.out_topic]})

    def serve(self):
        self.out_queue.put({'function': 'send', 'args': [{'miao': 1}, 0, self.out_topic]})
        self.play()


if __name__ == '__main__':
    ping = P_ng(topic1, topic2)
    pong = P_ng(topic2, topic1, count=True)
    print('Starting...')
    ping.play()
    ping.is_started.wait()

    pong.serve()
