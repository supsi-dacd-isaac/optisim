# mocks a simple RabbitConsumer

from .thread_rabbit_consumer import thread_rabbit_consumer
from multiprocessing import Process, Event, Queue


class RabbitConsumer:
    def __init__(self, topic, rabbit_config, timeout=None):
        self.oq = Queue(1)
        self.ready = Event()
        self.process = Process(target=thread_rabbit_consumer,
                               args=(topic,
                                     self.oq,
                                     self.ready,
                                     0,
                                     None,
                                     rabbit_config))
        self.timeout = timeout

    def __iter__(self):
        self.ready.wait()
        q_item = self.oq.get(block=True, timeout=self.timeout)
        yield q_item

    def poll(self):
        # compatibility
        self.ready.wait()
        return {}


