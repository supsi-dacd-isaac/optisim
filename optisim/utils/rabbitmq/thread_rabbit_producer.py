from ..optisim_logger import setup_logger
from .rabbit_sim_producer import RabbitSimulationProducer


def thread_rabbit_producer(input_queue, config, starting_time, logging_conf=None):

    """This function is intended to be the target of a Process.
    It sets up a RabbitSimulationProducer according to config and starting time.
    Then it takes messages (dicts) from the input_queue and uses the RabbitSimulationProducer
    to send them to broker."""

    logging_conf['name'] = 'thread_rabbit_producer'
    logger = setup_logger(**logging_conf)
    rabbit_producer = RabbitSimulationProducer(logger=logger, host=config['rabbit']['host'],
                                               port=int(config['rabbit']['port'],),
                                               user=config['rabbit']['user'],
                                               password=config['rabbit']['password'],
                                               virtual_host=config['rabbit']['virtual_host'])
    rabbit_producer.datetime_offset = starting_time
    while True:

        raw_msg = input_queue.get()
        exchange, topic, msg = raw_msg
        logger.debug('Received message function -> {}'.format(msg['function']))

        if msg['function'] == 'send':
            rabbit_producer.send_dict(exchange, topic, *msg['args'])
            logger.debug('Goes on topic -> {}'.format(topic))

        elif msg['function'] == 'flush':
            pass

        elif msg['function'] == 'kill':
            logger.info('rabbit producer killed')
            break

        else:
            raise ValueError('Producer instruction type unknown: {}'.format(msg['function']))
