import pika


class RabbitsBreeder:
    """
    Utility class for the management of a RabbitMQ instance
    """
    def __init__(self, cfg, logger):
        # Create the connection dictionary
        cred = pika.PlainCredentials(username=cfg['user'], password=cfg['password'])
        conn_params = pika.ConnectionParameters(host=cfg['host'], port=cfg['port'], credentials=cred,
                                                virtual_host=cfg['virtual_host'])

        # Define the main variables
        self.connection = pika.BlockingConnection(conn_params)
        self.logger = logger

    def purge_queues(self, queues):
        """
        Purge the given queues
        """
        # Check if the channel is open
        if self.connection.channel().is_open:
            # Purge the queues
            for queue in queues:
                try:
                    self.logger.info('Purging queue: %s' % queue)
                    self.connection.channel().queue_purge(queue=queue)
                except Exception as e:
                    self.logger.error('Unable to purge queue %s' % queue)
                    self.logger.error('Exception: %s' % str(e))

    def delete_queues(self, queues):
        """
        Delete the given queues
        """
        # Check if the channel is open
        if self.connection.channel().is_open:
            # Delete the queues
            for queue in queues:
                try:
                    self.logger.info('Deleting queue: %s' % queue)
                    self.connection.channel().queue_delete(queue=queue)
                except Exception as e:
                    self.logger.error('Unable to delete queue %s' % queue)
                    self.logger.error('Exception: %s' % str(e))

