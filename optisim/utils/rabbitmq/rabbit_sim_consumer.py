from ..optisim_logger import setup_logger
from multiprocessing import Queue, Process, Event
from .thread_rabbit_consumer import thread_rabbit_consumer
from .rabbit_breeder import RabbitsBreeder


class RabbitSimulationConsumer:
    def __init__(self, logging_conf, exchange, topic, rabbit_pars, auto_delete=False):
        logging_conf['name'] = 'RabbitSimulationConsumer_{}'.format(topic)
        self.logger = setup_logger(**logging_conf)
        self.topic = topic
        self.exchange = exchange
        self.rabbit_pars = rabbit_pars

        # Create a breeder of rabbits in order to delete the queues
        self.breeder = RabbitsBreeder(cfg=rabbit_pars, logger=self.logger)
        self.breeder.delete_queues(queues=[topic])

        self.gid = 0  # '%s_%s' % (broker_pars['group_id'], str(time.time()).replace('.', ''))
        self.q = Queue(1)
        self.assignment_acknowledge = Event()
        self.assignment_acknowledge.clear()
        self.cc = Process(target=thread_rabbit_consumer,
                          args=[self.exchange,
                                self.topic,
                                auto_delete,
                                self.q,
                                self.assignment_acknowledge,
                                self.gid,
                                logging_conf,
                                self.rabbit_pars])

        self.cc.daemon = True
        self.cc.start()

        self.assignment_acknowledge.wait()
        self.logger.info('rabbit consumer initialized')

    def listen(self, timeout=None):
        bv = self.q.get(timeout=timeout)
        return bv

    def exit(self):
        self.cc.terminate()

