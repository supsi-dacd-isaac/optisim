import json
import time
from datetime import datetime, timezone, timedelta

import pika

RABBIT_HEARTBEAT = 600

class RabbitSimulationProducer:
    def __init__(self, logger=None, host='localhost', user='guest', password='guest', virtual_host='/', port=5672, ttl=None):

        self.cred = pika.PlainCredentials(username=user, password=password)
        self.host = host
        self.user = user
        self.password = password
        self.virtual_host = virtual_host
        self.port = port
        self.ttl = ttl
        self.init_pika_channel()
        if logger is not None:
            self.logger = logger
        else:
            import logging
            null_logger = logging.getLogger('RSPdummy')
            null_handler = logging.NullHandler()
            null_logger.addHandler(null_handler)
            self.logger = null_logger

        self._starting_time = datetime(1970, 1, 1)
        self.declared_exchanges = []

    def init_pika_channel(self):
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self.host, port=self.port, credentials=self.cred,
                                      virtual_host=self.virtual_host, heartbeat=RABBIT_HEARTBEAT))
        self.channel = self.connection.channel()

    @property
    def datetime_offset(self):
        return self._starting_time

    @datetime_offset.setter
    def datetime_offset(self, new_start_time):
        self._starting_time = new_start_time

    def send_dict(self, exchange, topic, payload_dict: dict, case, simulation_dblhour=None, sync=False, etype='direct'):

        # self.channel.exchange_declare(exchange, exchange_type=etype)
        # this operation should be unnecessary
        # self.logger.debug('Rabbit producer worker declared/checked exchange {}'.format(topic))
        msg2send = self._wrap_data(payload_dict, case, simulation_dblhour)
        try:
            self.channel.basic_publish(exchange=exchange,
                                       body=msg2send,
                                       routing_key=topic,
                                       properties=pika.BasicProperties(expiration=self.ttl))
        except Exception as e:
            self.logger.error(str(e))
            self.logger.error('try to reinit pika channel')
            self.init_pika_channel()
            self.channel.basic_publish(exchange=exchange,
                                       body=msg2send,
                                       routing_key=topic,
                                       properties=pika.BasicProperties(expiration=self.ttl))
            self.logger.error('reinitialization successful')

        self.logger.debug('Sent message %s' % msg2send)

    def _wrap_data(self, payload: dict, case, simulation_dblhour):
        if simulation_dblhour is not None:
            sim_time = self._starting_time + timedelta(hours=simulation_dblhour)
            sim_time = int(sim_time.replace(tzinfo=timezone.utc).timestamp() * 1e6)
        else:
            sim_time = None
        data = dict(case=case, unix_ts=int(time.time()), unix_simulation_time=sim_time)
        data.update(payload)

        return json.dumps(data)
