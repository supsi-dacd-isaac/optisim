import json
from multiprocessing import Event
from multiprocessing.queues import Queue
from queue import Full as FullException

import pika
import logging

from ..optisim_logger import setup_logger

RABBIT_HEARTBEAT = 600

def thread_rabbit_consumer(exchange,
                           topic,
                           auto_delete,
                           output_queue: Queue,
                           consumer_ready: Event,
                           group_id,
                           logging_conf,
                           rabbit_pars):

    """This function is intended to be the target of a Process.
    It sets up a RabbitConsumer according to broker_pars, then subscribes to topic with the provided group_id.
    Then it consumes messages (byte streams representing json-formatted strings) from broker, decodes them, json-loads
    them and puts them in the output_queue. """
    # logging setup
    if logging_conf is None:
        null_logger = logging.getLogger('CCPdummy')
        null_handler = logging.NullHandler()
        null_logger.addHandler(null_handler)
        logger = null_logger
    else:
        logging_conf['name'] += '.{}.continuous_consumer_process'.format(topic)
        logger = setup_logger(**logging_conf)

    # message reaction callback
    def on_message(_ch, _method, _properties, body):
        decoded_msg = body.decode('utf-8')
        content = json.loads(decoded_msg)

        try:
            output_queue.put(content, block=True)
            # block=False bc we WANT to raise an exception if the queue is full
        except FullException:
            raise FullException('A message arrived while the last one was not consumed')

        logger.debug('Received message: %s' % decoded_msg)

    # setup connexun
    cred = pika.PlainCredentials(username=rabbit_pars['user'], password=rabbit_pars['password'])
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=rabbit_pars['host'], 
                                  port=rabbit_pars['port'], 
                                  credentials=cred,
                                  virtual_host=rabbit_pars['virtual_host'],
                                  heartbeat=RABBIT_HEARTBEAT))
    channel = connection.channel()
    etype = 'direct'
    channel.exchange_declare(exchange=exchange, exchange_type=etype, auto_delete=auto_delete)
    # print(topic)
    # logger.debug('Consumer declared exchange {} ({})'.format(topic, etype))

    _result = channel.queue_declare(topic, exclusive=False)
    # queue_name = result.method.queue
    channel.queue_bind(exchange=exchange, queue=topic)
    logger.debug('Consumer bound to queue {}'.format(topic))

    # logger.info('continuous consumer process configured')

    channel.basic_consume(
        queue=topic, on_message_callback=on_message, auto_ack=True)
    consumer_ready.set()
    channel.start_consuming()


