import json
from itertools import tee
from os import path

import networkx as nx

import krangpower as kp


def _pairwiseback(iterable):
    a, b = tee(reversed(iterable))
    next(b, None)
    return zip(a, b)


def _o2k(optisim_conf_filename):
    with open(optisim_conf_filename, 'r') as cf:
        raw_cf = json.load(cf)
    circuit_folder = raw_cf['krang']["dss_files_folder"]
    circuit_file = raw_cf['krang']["circuit_file"]
    krang_file = path.join(circuit_folder, circuit_file)
    return krang_file


def op_2_kr(optisim_conf_filename):
    """returns the Krang used as circuit in an optisim main configuration file."""
    with open(optisim_conf_filename, 'r') as cf:
        raw_cf = json.load(cf)

    circuit_folder = raw_cf['krang']["dss_files_folder"]
    circuit_file = raw_cf['krang']["circuit_file"]
    krang_file = path.join(circuit_folder, circuit_file)
    ckt = kp.Krang.from_json(krang_file)
    return ckt


def circuit_buses(ckt):
    buses_in_circuit = ckt.brain.Circuit.AllBusNames()
    return buses_in_circuit


def terminal_buses(ckt):
    g = ckt.graph()
    tnodes = [x for x in g.nodes if nx.degree(g, x) == 1]
    return tnodes


def meter2traf(optisim_conf_filename):
    ckt = op_2_kr(optisim_conf_filename)
    b2t = node2traf(ckt)

    with open(optisim_conf_filename, 'r') as cf:
        raw_cf = json.load(cf)

    m2b = {}
    for meterdata in raw_cf['meters']:
        m2b[meterdata['name']] = meterdata['bus']

    m2t = {}
    for metername, bus in m2b.items():
        try:
            m2t[metername] = b2t[bus]
        except KeyError:
            m2t[metername] = None

    return m2t


def meter2traf_direct(ckt, optisim_conf_filename):
    b2t = node2traf(ckt)

    with open(optisim_conf_filename, 'r') as cf:
        raw_cf = json.load(cf)

    m2b = {}
    for meterdata in raw_cf['meters']:
        m2b[meterdata['name']] = meterdata['bus']

    m2t = {}
    for metername, bus in m2b.items():
        try:
            m2t[metername] = b2t[bus]
        except KeyError:
            m2t[metername] = None

    return m2t


def node2traf(ckt):
    g = ckt.graph()
    source_bus, *bnames = ckt.brain.Circuit.AllBusNames()  # the first of AllBusNames is the source
    trafi_bus = ckt.Transformers['buses'].to_dict()

    b2t = {}

    for b in bnames:  # for each bus...
        bus_paths = nx.all_simple_paths(g, source_bus, b)  # we find the paths connecting it to source...
        # if the topology is a tree, there will be only one!
        b2t[b] = []
        for bus_path in bus_paths:
            found = False
            for b2, b1 in _pairwiseback(bus_path):  # we traverse it in reverse, pair by pair...
                for trafo, buses in trafi_bus.items():
                    if b2 in buses and b1 in buses:
                        # as soon as we find bus couple with one or more valid trafs, we append it (them) and break path
                        # NB: the only case where >1 buses is if there are two trafs between these same buses!
                        b2t[b].append(trafo)
                        found = True
                if found:
                    break

        if len(b2t[b]) == 1:
            b2t[b] = b2t[b][0]
        elif len(b2t[b]) == 0:
            b2t[b] = None

    return b2t


if __name__ == '__main__':
    conf_file = 'opticonf/simulation_conf/idsia_battery_test_2_meters_idsia_influx_idsia_rabbit.json'
    print(circuit_buses(op_2_kr(conf_file)))
    print(terminal_buses(op_2_kr(conf_file)))
    print(node2traf(op_2_kr(conf_file)))
    print(meter2traf(conf_file))

    # a minimal, multi-transformer circuit for on-the-fly test
    gee = kp.Krang()
    gee['sourcebus', '1'] << kp.Transformer().aka('tr1')
    gee['1', '2'] << kp.Line().aka('l1')
    gee['2', 'b3'] << kp.Transformer().aka('tra2b')
    gee['2.1.3', 'c4.1.3'] << kp.Transformer().aka('tra2cp1')
    gee['2.2', 'c4.2'] << kp.Transformer().aka('tra2cp2')
    gee['b3', 'b5'] << kp.Line().aka('lb1')
    gee['b5', 'b6'] << kp.Line().aka('lb2')
    gee['c4.1', 'c7.1'] << kp.Line().aka('lc1')
    print(node2traf(gee))
