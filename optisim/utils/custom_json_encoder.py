import json
import numpy as np


class PintCpxEnco(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, np.ndarray):
            return o.tolist()
        if hasattr(o, 'magnitude'):
            o = o.magnitude
            return np.real(o), np.imag(o)
        else:
            return np.real(o), np.imag(o)
