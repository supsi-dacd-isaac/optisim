import json
import glob
from os import path
from optisim.utils.configuration_fitter import ConfigurationFitter


class PeekConf(ConfigurationFitter):
    def __init__(self, config_file, components_defaults_folder):
        super().__init__(config_file)
        self.components_templates = dict()
        self._avail_types = []

        for template_path in glob.glob('%s/*.json' % components_defaults_folder):
            (component_type, ext) = path.splitext(path.basename(template_path))
            self._avail_types.append(component_type)
            self.components_templates[component_type] = json.loads(open(template_path).read())

        self.set_fitted_components()

    @property
    def available_component_types(self):
        return self._avail_types

    def _peekdic(self, cpt, element_type: str, pars: list = ()):

        if cpt['type'] == element_type:
            rd1 = {item: cpt[item] for item in pars if item in cpt.keys()}
            rd2 = {par: cpt['pars'][par] for par in pars if par in cpt['pars'].keys()}
            if 'algo_pars' in cpt.keys():
                rd3 = {par: cpt['algo_pars'][par] for par in pars if par in cpt['algo_pars'].keys()}
            else:
                rd3 = {}
            rd = {}
            rd.update(rd1)
            rd.update(rd2)
            rd.update(rd3)
            yield rd

        elif 'heating_element' in cpt.keys():
            for subel in self._peekdic(cpt['heating_element'], element_type, pars):
                yield subel

        elif 'dhw' in cpt.keys():
            for subel in self._peekdic(cpt['dhw'], element_type, pars):
                yield subel

    def peek(self, element_type: str, pars: list = ()):
        # cycle over the meters/components to set the custom parameters

        for meter in self.fitted_config['meters']:
            for component in meter['components']:
                for f in self._peekdic(component, element_type, pars):
                    f['meter_name'] = meter['name']
                    yield f


if __name__ == '__main__':
    f = PeekConf(r'optisim\conf\algo_test_standalone_boiler.json',
                 'optisim\conf\components_defaults')

    print(f.available_component_types)

    els1 = f.peek('dhw',   # type of element
                  ['Qnom', 'profile', 'name'],   # items to look for inside component or component['pars']
                  )

    # els2 = f.peek('heat_pump',   # type of element
    #               ['Q_nom', 'type'],   # items to look for inside component or component['pars']
    #               )
    #
    # els3 = f.peek('pv',   # type of element
    #               ['Pac', 'profile'],   # items to look for inside component or component['pars']
    #               )

    for el in els1:
        print(el)

    # for el in els2:
    #     print(el)
    #
    # for el in els3:
    #     print(el)
