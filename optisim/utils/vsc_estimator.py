import numpy as np
from sklearn.linear_model import RidgeCV, LinearRegression
#from multiprocessing import Process, Manager
import pandas as pd

class VSCEstimator:
    def __init__(self,pars=None):
        if pars is None:
            pars = {'r':'Ridge','max_obs':1440*10,'min_obs':100}
        self.pars = pars
        self.r = self._init_regressor(pars['r'])
        self.fitted = False
        self.meter_list = None
        self.P_buffer = None
        self.Q_buffer = None
        self.V_buffer = None
        self.n_meters = []
        self.meter_trafo_dep = None

    def update_measurements(self,raw_data):
        """
        Increase the buffer of P,Q,V matrices
        :param data: dictionary containing [[P1,Q1,V1],[P2,Q2,V2],[P3,Q3,V3]], respectively the active and reactive
        power and the voltage of each phase.
        :return:
        """
        power_dict = raw_data['meter_power']
        voltage_dict = raw_data['meter_voltage']
        meter_list_p = list(power_dict.keys())
        meter_list_v = list(voltage_dict.keys())

        assert np.all([m in meter_list_p for m in meter_list_v]) and np.all([m in meter_list_v for m in meter_list_p])\
            , 'meters in meter_voltage dict do not coincide with meters in meter_power dict'

        if self.meter_list is None:
            self.meter_list = meter_list_v
            self.n_meters = len(self.meter_list)
        else:
            assert np.all([m in self.meter_list for m in meter_list_v]), 'Some meters are not recognized'
            assert np.all([m in meter_list_v for m in self.meter_list]), 'Some meters are missing'

        # assuming data is a dictionary, each entry containing a nested array of P,Q,V for each phase,
        # retrieve these observations for all the meters
        P,Q,V = [[],[],[]]
        for m in self.meter_list:
            P_m = np.array(power_dict[m])[:, 0]
            Q_m = np.array(power_dict[m])[:, 1]
            V_complex = np.array(power_dict[m])[:,0]+1j*np.array(power_dict[m])[:,1]    # convert to complex number
            V_m = np.abs(V_complex)                                                     # take the absolute value
            P.append(P_m.ravel())
            Q.append(Q_m.ravel())
            V.append(V_m.ravel())
        P,Q,V = [np.hstack(P),np.hstack(Q),np.hstack(V)]


        # increase the buffer
        if self.P_buffer is None:
            self.P_buffer = np.copy(P)
        else:
            self.P_buffer = np.vstack([self.P_buffer,P])
        if self.Q_buffer is None:
            self.Q_buffer = np.copy(Q)
        else:
            self.Q_buffer = np.vstack([self.Q_buffer,Q])
        if self.V_buffer is None:
            self.V_buffer = np.copy(V)
        else:
            self.V_buffer = np.vstack([self.V_buffer,V])

        # trim the buffer if number of samples is bigger than 'max_obs'
        if self.P_buffer.shape[0]>self.pars['max_obs']:
            self.P_buffer = self.P_buffer[-self.pars['max_obs']:,:]
        if self.Q_buffer.shape[0]>self.pars['max_obs']:
            self.Q_buffer = self.Q_buffer[-self.pars['max_obs']:,:]
        if self.V_buffer.shape[0]>self.pars['max_obs']:
            self.V_buffer = self.V_buffer[-self.pars['max_obs']:,:]

    def estimate_vsc(self):
        """
        Estimate the voltage sensitivity coefficients by means of some kind of regression
        :return: k_mat, k_pd, kpis:
                k_mat: np.narray, size (6n,3n), where n is the number of meters
                k_pd: pd.DataFrame, such that k_pd.loc['meter_1']['meter_2'] is a (6,3) matrix with sensitivity
                coefficients linking (P,Q) of meter_1 to the 3 phases voltages of 'meter_2'
                kpis: dict of KPIs for the current identification
        """
        if len(self.P_buffer) < self.pars['min_obs']:
            k_mat = np.zeros((6*self.n_meters, 3*self.n_meters))
            kpis = {}
        else:
            x = np.hstack([self.P_buffer,self.Q_buffer])
            y = self.V_buffer
            self.r.fit(np.diff(x,axis=0),np.diff(y,axis=0))
            k_mat = self.r.coef_.T
            kpis = self._get_kpis(k_mat)

        k_pd = pd.DataFrame({},index=self.meter_list,columns=self.meter_list)
        for i,m in enumerate(self.meter_list):
            wp = np.arange(3) + 3 * i
            wq = np.arange(3) + 3 * i + int(len(k_mat)/2)
            # extract the coefficients relative to meter m
            k_m = np.vstack([k_mat[wp, :],k_mat[wq, :]])
            k_pd.loc[m] = [k_m[:,np.arange(3)+3*j] for j in range(len(self.meter_list)) ]



        return k_mat, k_pd, kpis

    @staticmethod
    def _init_regressor(r_name):
        if r_name == 'Ridge':
            r = RidgeCV(alphas=10**np.linspace(-2,8,11))
        elif r_name == 'wls':
            r = WLS()
        else:
            ValueError('Regressor name for voltage sensitivity coefficients is {}, but is not present '
                       'in the available cases'.format(r_name))
        return r

    def _get_kpis(self, k: np.ndarray) -> dict:
        """
        Compute KPIs for the identified coefficients
        :param k: identified voltage sensitivity coefficients np.ndarray (6n,3n), where n is the number of meters
        :return: dict of KPIs
        """
        x = np.diff(np.hstack([self.P_buffer, self.Q_buffer]),axis=0)
        v = np.diff(self.V_buffer,axis=0)
        v_hat = x @ k
        kpis = {}
        kpis['rmse'] = np.mean((v-v_hat)**2)**0.5
        rmse_0 = np.mean((v) ** 2) ** 0.5
        kpis['rmse_norm'] = kpis['rmse']/rmse_0
        kpis['mape'] = np.mean(np.abs(v - v_hat))

        return kpis

class WLS:
    def __init__(self):
        self.coef_ = []

    def fit(self, x, y):
        self.coef_ = self.weighted_lin_squares(x, y)

    def predict(self, x):
        return np.dot(x, self.coef_)

    def weighted_lin_squares(self, x, y):
        n = x.shape[0]
        Sigma_inv = np.linalg.pinv(
            np.eye(n) + np.diag(-0.5 * np.ones(n - 1), -1) + np.diag(-0.5 * np.ones(n - 1), 1))
        coef_ = np.linalg.pinv(x.T @ Sigma_inv @ x) @ x.T @ (Sigma_inv @ y)
        return coef_