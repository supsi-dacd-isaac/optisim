import inspect
from datetime import timezone, datetime
import logging
import numpy as np
import pandas as pd
from influxdb import InfluxDBClient, DataFrameClient
from typing import Optional

class InputDataManager:
    """
    Class for data management. Data get downloaded from a remote db. Intended use: global variables such as
    irradiation and external temperature are downloaded once and kept in RAM, while data required for the simulation
    of each agents are queried on the fly.
    """
    def __init__(self, master_descriptor: dict, logger, sim_steps: dict):
        """
        Constructor
        :param master_descriptor: dict with the whole structure of the simulation.
        :type dictionary
        :param logger:
        :type Logger
        :param sim_steps:
        :type dictionary
        """
        # define the main global variables
        self.random_state = np.random.RandomState()
        self.str_frmt = '%Y-%m-%d %H:%M:%S'
        self.sim_steps = sim_steps
        if logger is None:
            self.logger = logging.getLogger('input_data_manager_logger')
        else:
            self.logger = logger
        self.master_descriptor = master_descriptor
        self.master_descriptor['influxdb']['inputMeasurement'] = next((l for l in master_descriptor['input_output'] if l['name'] == 'input_data'), None)['measurement']
        self.dataset = None

        start_sim_dt = datetime.strptime(master_descriptor['simulation']['startDateTime'], '%Y-%m-%d %H:%M')
        self.simulation_start_ts = int(start_sim_dt.replace(tzinfo=timezone.utc).timestamp())

        end_sim_dt = datetime.strptime(master_descriptor['simulation']['endDateTime'], '%Y-%m-%d %H:%M')
        self.simulation_end_ts = int(end_sim_dt.replace(tzinfo=timezone.utc).timestamp())

        # create the InfluxDB client
        self.if_client = InfluxDBClient(host=master_descriptor['influxdb']['host'],
                                        port=master_descriptor['influxdb']['port'],
                                        username=master_descriptor['influxdb']['user'],
                                        password=master_descriptor['influxdb']['password'],
                                        database=master_descriptor['influxdb']['dbname'])

        self.if_df_client = DataFrameClient(host=master_descriptor['influxdb']['host'],
                                        port=master_descriptor['influxdb']['port'],
                                        username=master_descriptor['influxdb']['user'],
                                        password=master_descriptor['influxdb']['password'],
                                        database=master_descriptor['influxdb']['dbname'])

        # set dt
        self.dt = self.master_descriptor['simulation']['step']

        # load meteo data
        #self.meteo_dataset_ts = self._get_meteo_dataset()
        self.meteo_dataset_ts = None
        self.t_unix = None

        # instantiate dict with simulation data
        self.simulation_dataset_ts = dict()

    def set_t_unix(self):
        self.t_unix = to_array(self.meteo_dataset_ts)

        self.logger.info('Meteo dataset successfully created')

    def get_simulation_start_ts(self):
        return self.simulation_start_ts

    def get_simulation_end_ts(self):
        return self.simulation_end_ts

    def get_t_unix(self):
        return self.t_unix

    def get_meteo_dataset(self, time_start, time_end):
        filt_idx = (self.meteo_dataset_ts.index >= time_start) & (self.meteo_dataset_ts.index <= time_end)
        return self.meteo_dataset_ts[filt_idx]

    def get_dt(self):
        return self.dt

    def get_master_descriptor(self):
        return self.master_descriptor

    def create_meteo_dataset(self):
        """ Get the meteo data from InfluxDB
        :param step_start: starting step
        :type  int
        :param step_end: ending step
        :type int
        :param interval: time resolution (sec)
        :type int
        :return: Result dataset
        :rtype: Pandas Dataframe
        """
        data, query = self.download_data(measurement=self.master_descriptor['influxdb']['inputMeasurement'],
                                         tags=self.master_descriptor['meteo']['tags'],
                                         fields=self.master_descriptor['meteo']['fields'],
                                         t_start=self.sim_steps['start'],
                                         t_end=self.sim_steps['end']+86400-1,
                                         interval='%ss' % self.master_descriptor['simulation']['step'],
                                         aggr_fun=self.master_descriptor['influxdb']['aggrFun'])
        return data.round(1)

    def get_simulation_dataset(self,
                               step_start: int,
                               step_end: int,
                               component_name: str,
                               fields: dict,
                               tags: dict,
                               interval: int,
                               aggr_fun: str):
        """ Get data from the dataset
        :param step_start: starting step
        :type  int
        :param step_end: ending step
        :type int
        :param component_name: name of the component
        :type string
        :param fields: fields
        :type list
        :param tags: tags
        :type dict
        :param interval: time resolution (sec)
        :type int
        :param aggr_fun: InfluxDb aggregation function (e.g. mean, sum)
        :type string
        :return: Result dataset
        :rtype: Pandas Dataframe
        """
        # calculate the data key
        data_key = '%s_%s' % (component_name, tags['name'])

        # check if the dataset is empty
        if data_key not in self.simulation_dataset_ts.keys():
            self.logger.info('Dataset empty')

            # get the requested dataset from InfluxDB and store it in the dictionary
            new_data_ts, query = self.download_data(measurement=self.master_descriptor['influxdb']['inputMeasurement'],
                                                    tags=tags, fields=fields,
                                                    t_start=step_start,
                                                    t_end=step_start + self.master_descriptor['simulation']['stepReadWriteDB'] + self.random_state.randint(0, round(86400/interval))*interval,
                                                    interval='%ss' % interval, aggr_fun=aggr_fun)

            # initialize the dataset
            self.simulation_dataset_ts[data_key] = new_data_ts

        else:
            # if data not in the buffer => free the buffer and load from the DB teh new data
            requested_data = self.simulation_dataset_ts[data_key][(self.simulation_dataset_ts[data_key].index >= step_start + self.simulation_start_ts) &
                                                                  (self.simulation_dataset_ts[data_key].index <= step_end + self.simulation_start_ts)]

            #  check if the dataset is not available
            if len(requested_data) < ((step_end-step_start) / self.simulation_start_ts + 1):
                self.logger.debug('Requested data not available in the dataset')

                # get the requested dataset from InfluxDB and store it in the dictionary
                new_data_ts, query = self.download_data(measurement=self.master_descriptor['influxdb']['inputMeasurement'],
                                                        tags=tags, fields=fields,
                                                        t_start=step_start,
                                                        t_end=step_start + self.master_descriptor['simulation']['stepReadWriteDB'] + self.random_state.randint(0, round(3600/interval))*interval,
                                                        interval='%ss' % interval, aggr_fun=aggr_fun)

                self.simulation_dataset_ts[data_key] = new_data_ts
                # # concatenate the datasets
                # self.simulation_dataset_ts[data_key] = pd.concat([self.simulation_dataset_ts[data_key], new_data_ts])
                #
                # # delete data older than 2 days
                # starting_index_to_consider = len(self.simulation_dataset_ts[data_key]) - 2 * round((86400 / interval) + 0.5)
                # self.simulation_dataset_ts[data_key] = self.simulation_dataset_ts[data_key][starting_index_to_consider:]
            else:
                self.logger.debug('Requested data available in the dataset')

        # return the requested data
        return self.simulation_dataset_ts[data_key][(self.simulation_dataset_ts[data_key].index >= step_start + self.simulation_start_ts) &
                                                    (self.simulation_dataset_ts[data_key].index <= step_end + self.simulation_start_ts)]

    @staticmethod
    def __called_by__():
        stack = inspect.stack()
        calling_class = stack[1][0].f_locals['self'].__class__
        return calling_class

    def download_data(self,
                      measurement: str,
                      tags: dict,
                      fields: list,
                      t_start: str,
                      t_end: str,
                      interval: str='900s',
                      aggr_fun: str='mean'):
        """ get data from the dataset
        :param measurement: measurement where the data are stored
        :type  string
        :param tags: dictionary with tags to proper perform the data request
        :type dict
        :param fields: fields array
        :type list
        :param t_start: time start
        :type string
        :param t_end: time end
        :type string
        :param interval: time grouping in seconds
        :type string
        :param aggr_fun: InfluxDB aggregation function (e.g. mean, sum)
        :type string
        :return: Result dataset, query
        :rtype: Pandas Dataframe, string"""
        query = 'SELECT '
        # add the fields
        if aggr_fun is None:
            for field in fields:
                query += '"%s", ' % (field)
        else:
            for field in fields:
                query += '%s(\"%s\") AS \"%s\", ' % (aggr_fun, field, field)

        # add the measurement
        query = '%s FROM \"%s\" WHERE ' % (query[:-2], measurement)

        # add the tags filters
        for key, value in tags.items():
            if isinstance(value, list):
                query += '('
                for v in value:
                    query += '\"%s\"=\'%s\' OR ' % (key, v)
                query = '%s) AND ' % query[:-4]
            else:
                query += '\"%s\"=\'%s\' AND ' % (key, value)
        query = '%s AND ' % query[:-5]

        # add the time filters
        query += 'time>=%i AND time<%i ' % ((int(t_start)+self.simulation_start_ts)*1e9,
                                                 (int(t_end)+self.simulation_start_ts)*1e9)
        # add the grouping
        if aggr_fun is not None:
            query += 'GROUP BY time(%s)' % interval

        self.logger.debug('Query: %s' % query)
        # get the values from DB
        res = self.if_client.query(query, epoch='s')
        self.logger.debug('result received for query')
        cols = res.raw['series'][0]['columns']
        values_np = np.array(res.raw['series'][0]['values'])

        dict_data = dict()
        for i in range(0, len(cols)):
            tmp_vals = values_np[:len(values_np), [i]]
            dict_data[cols[i]] = tmp_vals.flatten().tolist()
        res_df = pd.DataFrame.from_dict(dict_data)
        res_df['time'] = res_df['time'].astype('int')
        res_df = res_df.set_index('time')

        # return result and query string
        return res_df, query

    def download_df_data(self,
                      measurement: str,
                      tags: dict,
                      fields: list,
                      t_start: str,
                      t_end: str,
                      interval: str='900s',
                      aggr_fun: str='mean',
                      aggr_tag=None):
        """ get data from the dataset
        :param measurement: measurement where the data are stored
        :type  string
        :param tags: dictionary with tags to proper perform the data request
        :type dict
        :param fields: fields array
        :type list
        :param t_start: time start
        :type string
        :param t_end: time end
        :type string
        :param interval: time grouping in seconds
        :type string
        :param aggr_fun: InfluxDB aggregation function (e.g. mean, sum)
        :type string
        :return: Result dataset, query
        :rtype: Pandas Dataframe, string"""
        query = 'SELECT '
        # add the fields
        if aggr_fun is None:
            for field in fields:
                query += '"%s", ' % (field)
        else:
            for field in fields:
                query += '%s(\"%s\") AS \"%s\", ' % (aggr_fun, field, field)

        # add the measurement
        query = '%s FROM \"%s\" WHERE (' % (query[:-2], measurement)

        # add the tags filters
        for key, value in tags.items():
            query += '\"%s\"=\'%s\' AND ' % (key, value)
        query = '%s) AND ' % query[:-5]

        # add the time filters
        query += 'time>=%i AND time<=%i ' % ((int(t_start)+self.simulation_start_ts)*1e9,
                                                 (int(t_end)+self.simulation_start_ts)*1e9)
        # add the grouping
        if aggr_fun is not None:
            query += 'GROUP BY time(%s)' % interval

        if aggr_tag is not None:
            if aggr_fun is None:
                query = query + 'GROUP BY "%s"' % aggr_tag
            else:
                query = query + ', "%s"' % aggr_tag

        self.logger.debug('Query: %s' % query)
        # get the values from DB
        res = self.if_df_client.query(query, epoch='s')

        if aggr_tag is not None:
            res_df = pd.DataFrame({})
            for k, df in res.items():
                key = k[1][0][1]
                df.loc[:,'location'] = key
                res_df = res_df.combine_first(df)
        else:
            res_df = res[measurement]
        res_df.index = res_df.index.astype(int)
        return res_df, query

    def get_force_offs(self,
                       measurement: str,
                       meter_id: str,
                       t_start: int,
                       t_end: int,
                       interval: str = '900s',
                       aggr_fun: str = 'mean',
                       aggr_tag=None):
        """ get data from the dataset
        :param measurement: measurement where the data are stored
        :type  string
        :param tags: dictionary with tags to proper perform the data request
        :type dict
        :param fields: fields array
        :type list
        :param t_start: time start
        :type string
        :param t_end: time end
        :type string
        :param interval: time grouping in seconds
        :type string
        :param aggr_fun: InfluxDB aggregation function (e.g. mean, sum)
        :type string
        :return: Result dataset, query
        :rtype: Pandas Dataframe, string"""

        # get list of components for which force_off is saved (I know it's dangerous)
        fo_list = []
        query = 'SHOW TAG VALUES FROM {} WITH KEY="component_name" WHERE "meter_id"=\'{}\''.format(measurement, meter_id)
        res = self.if_client.query(query, epoch='s')
        for re in res:
            for r in re:
                query = 'SHOW TAG VALUES FROM {} WITH KEY="signal" WHERE "signal"=\'force_off\' AND ' \
                        '"component_name"=\'{}\' AND "meter_id"=\'{}\''.format(measurement, r['value'], meter_id)
                fo_res = self.if_client.query(query, epoch='s')
                if len(fo_res) > 0:
                    fo_list.append(r['value'])

        # download force_offs
        force_off_df = pd.DataFrame()
        for fo in fo_list:
            tags = {'signal': 'force_off', 'component_name': fo, 'meter_id': meter_id}
            data, _ = self.download_df_data(measurement, tags, ['value'], t_start, t_end, aggr_fun=None)
            force_off_df[fo] = data['value']
        force_off_df.index = force_off_df.index * 1000

        return force_off_df



CACHE = {}


def to_array(meteo_dataset):

    hsh = hash(tuple(pd.util.hash_pandas_object(meteo_dataset).values))

    try:
        return CACHE[hsh]
    except KeyError:
        arr = np.array([float(x) for x in meteo_dataset.index]).reshape(-1, 1)
        CACHE[hsh] = arr
        return arr
