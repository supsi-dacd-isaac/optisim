# --------------------------------------------------------------------------- #
# Classes
# --------------------------------------------------------------------------- #
import copy
import json

import numpy as np
import pandas as pd

from influxdb import DataFrameClient

# --------------------------------------------------------------------------- #
# Auxiliary record creation function
# --------------------------------------------------------------------------- #
def create_record(measurement: str, fields: dict, tags: dict, utc_timestamp: int):
    """ Create a LINE protocol string
    :param measurement: name of the measurement
    :type  string
    :param fields: InfluxDB fields
    :type dict
    :param tags: InfluxDB tags
    :type dict
    :param utc_timestamp: UTC timestamp
    :type int
    :return: string in LINE protocol
    :rtype: str
    """

    str_result = '{0},{1} {2} {3}'.format(
        measurement,
        ','.join(['='.join([str(k), str(v)]) for k, v in tags.items()]),
        ','.join(['='.join([str(k), str(v)]) for k, v in fields.items()]),
        int(float(utc_timestamp))
    )
    return str_result


# --------------------------------------------------------------------------- #
# Classes
# --------------------------------------------------------------------------- #
class ResultsHandler:
    """
    Class handling data received by broker topics. Firstly, the datasets are stored in an internal buffer and
    then saved in an InfluxDB instance
    """
    def __init__(self, config, influx_client, logger, id):
        """ Constructor
        :param config: configuration dictionary
        :type dict
        :param influx_client: InfluxDB client
        :type InfluxDBClient
        :param logger: logger instance
        :type logger
        :param id: identifier
        :type int
        """
        self.config = config
        self.influx_client = influx_client
        self.influx_df_client = DataFrameClient(influx_client)
        self.logger = logger
        self.id = id
        self.PHASES_CODES = {'1': 'a', '2': 'b', '3': 'c'}

        # the function create record is added as static method
        self.create_record = create_record

    def insert_models_values(self, str_data, current_points, measurement, tags, used_measurements):
        """ Insert data related to the component models
        :param str_data: JSON string data
        :type  string
        :param current_points: current InfluxDB points
        :type array of dict
        :param tags: InfluxDB tags
        :type dict
        :return: updated InfluxDB points
        :rtype: array of dict
        :return: UNIX timestamp pf the last value
        :rtype: int
        """
        values = json.loads(str_data)

        # cycling over the signals
        signals = values.keys()
        for signal in signals:
            tags['signal'] = signal
            # cycling over the timestamps
            for ts in values[signal].keys():
                # append the current point to the list
                try:
                    if values[signal][ts] is None:
                        continue
                    point = self.create_record(measurement, dict(value=float(values[signal][ts])), tags, ts)
                except Exception as e:
                    self.logger.error('EXCEPTION: %s' % str(e))
                    raise

                current_points.append(copy.deepcopy(point))
                # send data to InfluxDB if necessary
                # current_points = self.send_data_to_influxdb(points_to_send=current_points,
                #                                             max_data_length=int(self.config['influxdb']['maxLinesPerInsert']))
        return current_points

    def insert_forecast_values(self, str_data, current_points, measurement, tags, used_measurements):
        """ Insert data related to the component models
        :param str_data: JSON string data
        :type  string
        :param current_points: current InfluxDB points
        :type array of dict
        :param tags: InfluxDB tags
        :type dict
        :return: updated InfluxDB points
        :rtype: array of dict
        :return: UNIX timestamp pf the last value
        :rtype: int
        """
        values = json.loads(str_data)

        # cycling over the signals
        signals = values.keys()
        for signal in signals:
            # cycling over the timestamps
            for ts in values[signal].keys():
                tags['signal'] = signal
                for ix, val in enumerate(values[signal][ts]):
                    tags['step'] = 'step_{:05d}'.format(ix+1)
                    # append the current point to the list
                    try:
                        point = self.create_record(measurement, dict(value=float(val)), tags, ts)
                    except Exception as e:
                        self.logger.error('EXCEPTION: %s' % str(e))
                        raise

                    current_points.append(copy.deepcopy(point))
                # send data to InfluxDB if necessary
                # current_points = self.send_data_to_influxdb(points_to_send=current_points,
                #                                             max_data_length=int(self.config['influxdb']['maxLinesPerInsert']))
        return current_points

    def insert_agent_internal_values(self, str_data, current_points, measurement, tags, used_measurements):
        """ Insert data related to the component models
        :param str_data: JSON string data
        :type  string
        :param current_points: current InfluxDB points
        :type array of dict
        :param tags: InfluxDB tags
        :type dict
        :return: updated InfluxDB points
        :rtype: array of dict
        :return: UNIX timestamp pf the last value
        :rtype: int
        """
        values = pd.read_json(str_data).astype(float)
        points = self.influx_df_client._convert_dataframe_to_lines(values, measurement, global_tags=tags, time_precision='u')
        current_points += (copy.deepcopy(points))
        return current_points

    def insert_disaggregation_values(self, data, current_points, measurement):
        """ Insert data related to the component models
        :param data: data to insert
        :type  dict
        :param current_points: current InfluxDB points
        :type array of dict
        :param measurement: InfluxDB measurement
        :type str
        :return: updated InfluxDB points
        :rtype: array of dict
        :return: UNIX timestamp pf the last value
        :rtype: int
        """
        # cycle over the meters
        for meter in data['disaggregation'].keys():
            # cycle over the components
            for component in data['disaggregation'][meter].keys():
                dict_sigs = data['disaggregation'][meter][component]
                dict_sigs = json.loads(dict_sigs)
                # cycle over the signals
                for signal in dict_sigs.keys():
                    for ts in dict_sigs[signal]:
                        # define tags and fields dictionaries
                        tags = dict(meter=meter, component=component, signal=signal)
                        fields = dict(value=float(dict_sigs[signal][ts]))

                        # append the current point to the list
                        try:
                            point = self.create_record(measurement, fields, tags, int(float(ts)*1e3))
                        except Exception as e:
                            self.logger.error('EXCEPTION: %s' % str(e))
                            raise

                        current_points.append(copy.deepcopy(point))
                        # send data to InfluxDB if necessary
                        # current_points = self.send_data_to_influxdb(points_to_send=current_points,
                        #                                             max_data_length=int(self.config['influxdb']['maxLinesPerInsert']))
        return current_points

    def insert_meter_pv_profile_values(self, data, current_points, measurement):
        """ Insert data related to the component models
        :param data: data to insert
        :type  dict
        :param current_points: current InfluxDB points
        :type array of dict
        :param measurement: InfluxDB measurement
        :type str
        :return: updated InfluxDB points
        :rtype: array of dict
        :return: UNIX timestamp pf the last value
        :rtype: int
        """
        # cycle over the meters
        for meter in data['meter_pv_profile'].keys():
            dict_sigs = data['meter_pv_profile'][meter]
            dict_sigs = json.loads(dict_sigs)
            # cycle over the signals
            for signal in dict_sigs.keys():
                for ts in dict_sigs[signal]:
                    # define tags and fields dictionaries
                    tags = dict(meter=meter, signal=signal)
                    fields = dict(value=float(dict_sigs[signal][ts]))

                    # append the current point to the list
                    try:
                        point = self.create_record(measurement, fields, tags, int(float(ts)*1e3))
                    except Exception as e:
                        self.logger.error('EXCEPTION: %s' % str(e))
                        raise

                    current_points.append(copy.deepcopy(point))
                    # send data to InfluxDB if necessary
                    # current_points = self.send_data_to_influxdb(points_to_send=current_points,
                    #                                             max_data_length=int(self.config['influxdb']['maxLinesPerInsert']))
        return current_points

    def insert_meter_net_power_values(self, data, current_points, measurement):
        """ Insert data related to the component models
        :param data: data to insert
        :type  dict
        :param current_points: current InfluxDB points
        :type array of dict
        :param measurement: InfluxDB measurement
        :type str
        :return: updated InfluxDB points
        :rtype: array of dict
        :return: UNIX timestamp pf the last value
        :rtype: int
        """
        # cycle over the meters
        for meter in data['meter_net_power'].keys():
            dict_sigs = data['meter_net_power'][meter]
            dict_sigs = json.loads(dict_sigs)
            # cycle over the signals
            for signal in dict_sigs.keys():
                for ts in dict_sigs[signal]:
                    # define tags and fields dictionaries
                    tags = dict(meter=meter, signal=signal)
                    fields = dict(value=float(dict_sigs[signal][ts]))

                    # append the current point to the list
                    try:
                        point = self.create_record(measurement, fields, tags, int(float(ts)*1e3))
                    except Exception as e:
                        self.logger.error('EXCEPTION: %s' % str(e))
                        raise

                    current_points.append(copy.deepcopy(point))
                    # send data to InfluxDB if necessary
                    # current_points = self.send_data_to_influxdb(points_to_send=current_points,
                    #                                             max_data_length=int(self.config['influxdb']['maxLinesPerInsert']))
        return current_points

    @staticmethod
    def flatten_number(content):
        content = np.atleast_1d(content)
        if len(content) == 1 and not np.iscomplex(content):
            return dict(value=np.float(content))
        elif len(content) == 2:
            cpx = np.complex128(content[0] + 1j * content[1])
            return dict(valueMag=np.abs(cpx),
                        valueIm=np.imag(cpx),
                        valueRe=np.real(cpx))
        elif np.iscomplex(content):
            return dict(valueMag=np.abs(content),
                        valueIm=np.imag(content),
                        valueRe=np.real(content))
        else:
            raise ValueError('results_handler does not know how to handle a list of len > 2')

    def _recursive_explore(self,
                           dict_structure,
                           measurement: str,
                           timestamp: int,
                           tag_names: list or tuple,
                           initial_tags: list):
        
        """This function 'flattens' the dictionary structures that, by design, are returned by the results_function.
        It returns a list of points, one for each final data value contained in the dictionary, in order to be loaded
        in influx.
        
        EXAMPLE INPUT:
        measurement = 'current'
        timestamp = 1523556654
        dict_structure[<tag1_value>][<tag2_value>][<tag3_value>] = <fn_result>
             for instance:
            dict_structure['node_1']['node_2'] = 23.4467 + 0.5j
            dict_structure['node_1']['node_3'] = 27.795 + 2.65j
            dict_structure['node_2']['node_4'] = 19.9645 + 10.54j
        ...
        
        RETURNS:
        ["current, node_start=node_1, node_end=node_2, valueRe=23.4467, valueIm=0.5, 1523556654"
        ...

        ]

        """

        # ---------- INNER FUNCTION DEFINITION ---------
        # ---------- INNER FUNCTION DEFINITION ---------

        def recursive_explore_inner(dict_structure,
                                    measurement: str,
                                    timestamp: int,
                                    tag_names: list or tuple,
                                    current_tags: list,
                                    points_bag: list):

            nonlocal self

            if len(current_tags) < len(tag_names):
                for k in dict_structure.keys():
                    recursive_explore_inner(dict_structure[k],
                                            measurement,
                                            timestamp,
                                            tag_names,
                                            current_tags + [k],
                                            points_bag)

            elif len(current_tags) == len(tag_names):

                final_content = dict_structure  # since we reached the bottom, now dict structure is the real data.
                # we create the point and append it to the bag.
                point = self.create_record(measurement,
                                           self.flatten_number(final_content),
                                           dict(zip(tag_names, current_tags)),
                                           int(timestamp))
                points_bag.append(point)

            else:
                raise IndexError('Something went horribly wrong')

        # ---------- INNER FUNCTION DEFINITION ---------
        # ---------- INNER FUNCTION DEFINITION ---------

        points_bag = []
        # recursive exploration does not return anything. We populate points_bag passed "by reference"
        recursive_explore_inner(dict_structure, measurement, timestamp, tag_names, initial_tags, points_bag)
        return points_bag

    def insert_simulation_values(self, data, current_points, measurement, timestamp, index_dict):

        # send data to InfluxDB if necessary
        # current_points = self.send_data_to_influxdb(points_to_send=current_points,
        #                                             max_data_length=int(self.config['influxdb']['maxLinesPerInsert']))

        for signal in data.keys():

            # sig is just the first index, but we have to unpack it here in order to pass indicization[sig] below
            new_points = self._recursive_explore(data[signal],
                                                 measurement,
                                                 timestamp,
                                                 ['signal'] + index_dict[signal],
                                                 [signal])

            current_points += [copy.deepcopy(x) for x in new_points]

        return current_points

    def send_data_to_influxdb(self, points_to_send, max_data_length):
        """ Send data to InfluxDB server
        :param points_to_send: points to send
        :type array of dict
        :param max_data_length: Maximum length of the dataset (if > then all is sent to teh server)
        :type int
        :return: updated InfluxDB points
        :rtype: array of dict
        """
        # check if there are enough data in the batch, in case send all to InfluxDB
        if len(points_to_send) >= max_data_length:

            # Send data to InfluxDB
            try:
                self.influx_client.write_points(points=points_to_send,
                                                time_precision='u',
                                                protocol='line',
                                                batch_size=int(max_data_length * 1.25))

                self.logger.info('Worker N. %02i -> Inserted %i points in InfluxDB' % (self.id, len(points_to_send)))

                # free the dataset
                points_to_send = []
            except Exception as e:
                self.logger.error('Worker N. %02i failed to insert points in InfluxDB with error %s' % (self.id, str(e)))
            
        return points_to_send
