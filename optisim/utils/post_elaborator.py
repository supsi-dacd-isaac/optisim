import sys
import json
from datetime import datetime, MAXYEAR, timedelta
from importlib.util import spec_from_file_location, module_from_spec
from inspect import signature
from time import sleep
import argparse
from threading import Event

import numpy as np
from influxdb import InfluxDBClient, DataFrameClient

from optisim.utils import ConfigurationFitter

_dateformat = '%Y-%m-%d %H:%M%z'
_dateformat_influx = '%Y-%m-%dT%H:%M:%SZ'
_DEFAULT_CHUNK = 10000


class PostElaborator:
    def __init__(self, config, logger, max_chunk_size=_DEFAULT_CHUNK):

        # the following stuff is COPIED from meter_manager, because it has to perfectly mirror what the simulation
        # does/did. It'd be nice to wrap all this stuff in a function, and call it, in order to have no code duplication

        self.logger = logger
        # loading configuration
        # cf = ConfigurationFitter(config_file=config_file)
        # cf.set_fitted_config()
        self.config = config  # cf.fitted_config
        self.mcs = max_chunk_size
        self.functions = {}
        self.fixed_inputs = {}
        self.correspondencies = {}
        self.stop = Event()

        # extracting generic parameters
        self.start_time = datetime.strptime(self.config['simulation']['startDateTime'] + '+0000', _dateformat)
        self.end_time = datetime.strptime(self.config['simulation']['endDateTime'] + '+0000', _dateformat)

        # here we dyn-load the result function module in order to have their names and indicization available
        functions_file = self.config['krang']['results_functions_file']
        rf_modulespec = spec_from_file_location('results_functions', functions_file)
        rf_module = module_from_spec(rf_modulespec)
        rf_modulespec.loader.exec_module(rf_module)
        self.resfun_indicization = {}
        for name in dir(rf_module):
            member = getattr(rf_module, name)
            if hasattr(member, 'indicization'):
                self.resfun_indicization[name] = member.indicization
            else:
                continue

        # raise an influxdb client
        try:
            self.idb_client = InfluxDBClient(host=self.config['influxdb']['host'],
                                             port=int(self.config['influxdb']['port']),
                                             username=self.config['influxdb']['user'],
                                             password=self.config['influxdb']['password'],
                                             database=self.config['influxdb']['dbname'])
            self.pdf_client = DataFrameClient(host=self.config['influxdb']['host'],
                                              port=int(self.config['influxdb']['port']),
                                              username=self.config['influxdb']['user'],
                                              password=self.config['influxdb']['password'],
                                              database=self.config['influxdb']['dbname'])
        except Exception as e:
            self.logger.error('EXCEPTION: %s' % str(e))
            sys.exit(-1)

        # we reconstruct the correct measurement (for now it's just the one related to grid solution persistence)
        self.measurement = \
            next((l for l in self.config['input_output'] if l['name'] == "grid_2_db"), None)['measurement']
        self.logger.info('The measurement selected was {}'.format(self.measurement))
        self.pcr = None

    @property
    def default_dateformat(self):
        return _dateformat_influx

    def _pull_function_inputs(self, fun: callable, start_time, end_time, fixed_parameters):
        sig = signature(fun)
        pulled_inputs = {}

        # we verify that there is at least one "dominant" signal that contains all the tags of the others, so to ensure
        # that there is always a way to group tagged signals unambiguously.
        # (Actually I suspect that the sufficient condition for unambiguity is weaker than "there must be a 'dominant'
        # tag". If you want to write a perfectly general version of this function, you are most welcome to do so.)

        free_parameters = [param for param in sig.parameters if param in self.resfun_indicization.keys()]

        self.logger.debug('Extracting free inputs for function "{}": [{}]'
                         .format(fun.__name__, ', '.join(free_parameters)))

        for param in sig.parameters:  # param is, f.e., 'I_edge'

            if param in fixed_parameters.keys():
                continue
                # important: putting this option first has the additional benefit of allowing you to override the
                # signal retrieval with a fixed_parameter item with the name of the signal itself

            else:  # param in self.resfun_indicization.keys():
                param_query = getattr(fun, param)  # we expect the function to have....
                pulled_inputs[param] = self.pdf_client.query(param_query.format(self.measurement, start_time, end_time))

        return pulled_inputs

    def _calculate_points(self, fun, start_time, end_time, fixed_parameters: dict = None):

        # correspondency == 'perfect' means that all the signals must be perfectly mappable to the dominant; there's no
        #    one point less or more, neither in tag nor in timestamp.
        # correspondency == 'loose' means that only those points for which all the signals can provide the same tag
        #    and timestamp as the dominant will be computed, the others will be SKIPPED
        # correspondency == 'intersection' means that the signals must provide all the points that the dominant has in
        #    terms of tags and timestamp; but they can have more, that won't be utilized

        if fixed_parameters is None:
            fixed_parameters = {}

        # pull the inputs
        inputs = self._pull_function_inputs(fun, start_time, end_time, fixed_parameters)

        for i12 in inputs.values():
            if not i12:
                return None  # we return an iterable for coherence

        # inputs is a dict where the values are the exact query results that the function asked for, as dataframes.
        return fun(**{k: x.get(self.measurement) for k, x in inputs.items()}, **fixed_parameters)

        # if there is a correspondency problem, it's the function that has to manage it.
        # the function will return a generator that gives you all the points (tuples) complete with tags and time.

    def _run_chunk(self, fun, start_time, end_time, fixed_parameters):

        self.logger.info('Executing chunk: {} --> {}'
                         .format(start_time, end_time))

        pts = self._calculate_points(fun, start_time, end_time, fixed_parameters)

        if not pts:
            return False

        signame = fun.__name__
        records = []

        for point in pts:  # we iterate the points returned by the function

            value, tags, pttime = point

            if value is None:
                continue

            rec = self._create_record(self.measurement,
                                      dict(valueRe=np.real(value),
                                           valueIm=np.imag(value),
                                           valueMag=np.abs(value),
                                           ),
                                      dict(signal=signame,
                                           **dict(tags)
                                           ),
                                      int(datetime.strptime(pttime, _dateformat_influx).timestamp())
                                      )
            records.append(rec)

        self.logger.info('Writing {} points for function {} on InfluxDB.'.format(len(records), signame))
        self.idb_client.write_points(points=records, time_precision='s', protocol='line')
        return True

    def run(self, chunk_duration, start_time, end_time=None, wait=0, date_format=_dateformat_influx, fn_list=()):

        try:
            chunk_duration = timedelta(**chunk_duration)
        except TypeError:
            pass

        start_datetime = datetime.strptime(start_time, date_format)
        now_datetime = start_datetime
        ok = True

        if end_time is None:
            end_time = datetime(year=MAXYEAR, month=12, day=31)
        else:
            end_time = datetime.strptime(end_time, date_format)

        if not fn_list:
            fn_list = [k for k in self.functions.keys()]

        while not (now_datetime >= end_time or self.stop.is_set()):

            after_datetime = min(now_datetime + chunk_duration, end_time)

            for fun_name in fn_list:
                fun_conf = self.functions[fun_name]
                ok = self._run_chunk(fun_conf,
                                     now_datetime.strftime(_dateformat_influx),
                                     after_datetime.strftime(_dateformat_influx),
                                     self.fixed_inputs[fun_name])

            if not ok:
                if wait > 0:
                    self.logger.info('The last chunk was empty. Rerun scheduled in {}s....'.format(str(wait)))
                    now_datetime = now_datetime - chunk_duration
                    # in wait mode it's probable that the last chunk was only partially filled, so our
                    # rerun starts one step back
                    sleep(wait)
                else:
                    # if no wait, we just break
                    break
            else:
                # if we were ok, we update the time and regularly go on
                now_datetime = after_datetime

        return True

    def autorun(self, pe_config_file):

        pcr = PeConfReader(pe_config_file)

        fnlist = pcr['functions']
        post_functions_file = pcr['file']
        rf_modulespec = spec_from_file_location('postelaboration_functions', post_functions_file)
        pf_module = module_from_spec(rf_modulespec)
        rf_modulespec.loader.exec_module(pf_module)

        for i in fnlist:

            q_function = getattr(pf_module, i)

            self.functions[q_function.__name__] = q_function
            self.fixed_inputs[q_function.__name__] = \
                pcr['functions_configuration'][q_function.__name__]['fixed_inputs']

        st = self.config['simulation']['startDateTime'] + '+0000'
        ed = self.config['simulation']['endDateTime'] + '+0000'
        chunk_duration = pcr['chunk_duration']
        wait_period = pcr['wait']

        self.run(chunk_duration, st, ed, wait_period, date_format=_dateformat)

    @staticmethod
    def _create_record(measurement: str, fields: dict, tags: dict, utc_timestamp: int):
        """ Create a LINE protocol string
        :param measurement: name of the measurement
        :type  string
        :param fields: InfluxDB fields
        :type dict
        :param tags: InfluxDB tags
        :type dict
        :param utc_timestamp: UTC timestamp
        :type int
        :return: string in LINE protocol
        :rtype: str
        """

        str_result = '{0},{1} {2} {3}'.format(
            measurement,
            ','.join(['='.join([str(k), str(v)]) for k, v in tags.items()]),
            ','.join(['='.join([str(k), str(v)]) for k, v in fields.items()]),
            int(float(utc_timestamp))
        )
        return str_result


# if __name__ == '__main__':
#     import logging
#     ligir = logging.getLogger()
#     ligir.level = logging.INFO
#     ligir.addHandler(logging.StreamHandler())
#
#     pe = PostElaborator('/home/bombolo/PycharmProjects/optisim/conf/sim_test_parallel_2meters.json', ligir, 10000)
#
#     pe.configure_function('fake_value', {'cao': 5})
#     pe.run(timedelta(minutes=500), '2018-01-01T01:00:00Z')
#
#     # pe.run_chunk(fake_barb_value, '2018-01-01T01:00:00Z', '2018-01-01T02:30:00Z', {'cao': 3})

class PeConfReader:
    def __init__(self, file_name):
        with open(file_name, 'r') as peconffile:
            self.content = json.load(peconffile)

    def __getitem__(self, item):
        return self.content.__getitem__(item)


if __name__ == "__main__":
    import logging

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c', help='main configuration file')
    arg_parser.add_argument('-p', help='postelaboration configuration file')
    arg_parser.add_argument('-l', help='log file')

    # --------------------------------------------------------------------------- #
    # Set configuration
    # --------------------------------------------------------------------------- #
    args = arg_parser.parse_args()
    cf = ConfigurationFitter(config_file=args.c)
    cf.set_fitted_config()
    main_config = cf.fitted_config

    # --------------------------------------------------------------------------- #
    # Set logging object
    # --------------------------------------------------------------------------- #
    if not args.l:
        log_file = None
    else:
        log_file = args.l

    logger = logging.getLogger()
    logging.basicConfig(format='%(asctime)-15s::%(levelname)s::%(funcName)s::%(message)s', level=logging.INFO,
                        filename=log_file)

    pe = PostElaborator(main_config, logger)

    pe.autorun(args.p)
