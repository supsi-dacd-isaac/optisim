from influxdb import InfluxDBClient
from influxdb import DataFrameClient
import pandas as pd


class DbInterface:

    def __init__(self, host='localhost', port=8086, user='xxx', password='xxx', dbname='xxx', chunk_size=5000):
        self.client = InfluxDBClient(host, port, user, password, dbname)
        self.chunk_size = chunk_size
        self.df_client = DataFrameClient(host, port, user, password, dbname)

    def load_from_csv(self, file_path, measurement, tags, time_shift=None):
        df = pd.read_csv(file_path, header=0, parse_dates=[0], index_col=0)
        if time_shift is not None:
            df.index = df.index + time_shift
        self.df_client.write_points(df, measurement, tags, batch_size=self.chunk_size)

        # # write chunks of chunk_size points
        # i = 0
        # while True:
        #
        #     if i+self.chunk_size >= len(df):
        #         self.df_client.write_points(df.iloc[i:len(df)], measurement, tags)
        #         break
        #     else:
        #         self.df_client.write_points(df.iloc[i:i+self.chunk_size], measurement, tags, batch_size=self.chunk_size)
        #         i += self.chunk_size

    def get_data(self, measurement, tags, fields, t_start, t_end, interval, aggr_fun='mean', all_signals=False):

        if all_signals:
            measurement_string = '/^' + measurement + '$/'
            osg = ', "signal"'
        else:
            measurement_string = '"' + measurement + '"'
            osg = ''

        _fields_base_string = '{aggr_fun}("{field}") AS "{field}"'
        fields_string = ', '.join([_fields_base_string.format(aggr_fun=aggr_fun, field=field) for field in fields])
        tags_string = ' AND '.join(['"{0}" = \'{1}\''.format(k, v) for k, v in tags.items()])

        query = \
            'SELECT {fields} FROM {measurement} WHERE({tags} ) '\
            'AND time >= {start_time}ms AND time <= {end_time}ms '\
            'GROUP BY time({interval}){optional_signal_group} fill(null)'\
            .format(

                fields=fields_string,
                tags=tags_string,
                measurement=measurement_string,
                start_time=str(int(t_start*1000)),
                end_time=str(int(t_end*1000)),
                interval=interval,
                optional_signal_group=osg

            )

        # query_c = 'SELECT '
        # for field in fields:
        #     query_c += aggr_fun + '("' + field + '")' + ' AS "' + field + '", '
        # query_c = query_c[:-2]
        # if all_signals:
        #     query_c += ' FROM /^' + measurement + '$/'
        # else:
        #     query_c += ' FROM "' + measurement + '"'
        # query_c += ' WHERE('
        # for key, value in tags.items():
        #     query_c += '"' + key + '" = \'' + value + '\' AND '
        # query_c = query_c[:-4]
        # query_c += ')'
        # query_c += ' AND time >= ' + str(int(t_start*1000)) + 'ms AND time <= ' + str(int(1000*t_end)) + 'ms'
        # query_c += ' GROUP BY time(' + interval + ')'
        # if all_signals:
        #     query_c += ', "signal"'
        # query_c += ' fill(null)'
        # print(query_c)
        #
        # assert query == query_c

        return self.df_client.query(query)

    def get_tag_values(self, measurements, key):

        query = \
            'SHOW TAG VALUES FROM {measurements} WITH KEY = {key}'.format(
                measurements=', '.join(measurements),
                key=key
            )

        # query_c = 'SHOW TAG VALUES '
        # query_c += 'FROM '
        # for measurement in measurements:
        #     query_c += measurement + ', '
        # query_c = query_c[:-2]
        # query_c += ' WITH KEY = ' + key
        # print(query_c)
        #
        # assert query_c == query_c

        return self.df_client.query(query)

    def get_data_signals(self, measurement, tags, fields, t_start, t_end, interval, signals, dev_key_name, logger,
                         aggr_fun='mean', all_signals=False):

        if all_signals:
            measurement_string = '/^' + measurement + '$/'
            osg = ', "signal"'
        else:
            measurement_string = '"' + measurement + '"'
            osg = ''

        _fields_base_string = '{aggr_fun}({field}) AS "{field}"'
        fields_string = ', '.join([_fields_base_string.format(aggr_fun=aggr_fun, field=field) for field in fields])
        tags_string = ' AND '.join(['"{0}"=\'{1}\''.format(k, v) for k, v in tags.items])
        signals_string = ' OR '.join(['"signal" = \'{}\''.format(s) for s in signals])

        query = \
            'SELECT {fields} FROM {measurement} WHERE(({tags}) AND ({signals})) '\
            'AND TIME >= {start_time}ms AND TIME <= {end_time}ms '\
            'GROUP BY time({interval}), "{dkn}"{optional_signal_group} fill(null)'\
            .format(

                fields=fields_string,
                tags=tags_string,
                signals=signals_string,
                measurement=measurement_string,
                start_time=str(int(t_start*1000)),
                end_time=str(int(t_end*1000)),
                interval=interval,
                dkn=dev_key_name,
                optional_signal_group=osg

            )
        #
        # query_c = 'SELECT '
        # for field in fields:
        #     query_c += aggr_fun + '("' + field + '") ' + ' AS "' + field + '", '
        # query_c = query_c[:-2]
        # if all_signals:
        #     query_c += ' FROM /^' + measurement + '$/'
        # else:
        #     query_c += ' FROM "' + measurement + '"'
        # query_c += ' WHERE (('
        # for key, values in tags.items():
        #     for value in values:
        #         query_c += '"' + key + '" = \'' + value + '\' OR '
        # query_c = query_c[:-3]
        # query_c += ') AND ('
        # for signal in signals:
        #     query_c += '"signal" = \'' + signal + '\' OR '
        # query_c = query_c[:-3]
        # query_c += '))'
        # query_c += ' AND time >= ' + str(int(t_start*1000)) + 'ms AND time <= ' + str(int(1000*t_end)) + 'ms'
        # query_c += ' GROUP BY time(' + interval + ')'
        # if len(signals) > 1:
        #     query_c += ', "' + dev_key_name + '"'
        # if all_signals or len(signals) > 1:
        #     query_c += ', "signal"'
        # query_c += ' fill(null)'
        # logger.info(query_c)
        #
        # assert query == query_c

        return self.df_client.query(query)
