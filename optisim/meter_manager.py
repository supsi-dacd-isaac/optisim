import json
import logging
import math
import time
import weakref
from datetime import datetime
from functools import partial
from importlib.util import spec_from_file_location, module_from_spec
from multiprocessing import Process, Manager, Semaphore
from queue import Empty

import numpy as np
import pandas as pd

import krangpower as kp
from .agents import meter_process
from .utils.configuration_fitter import ConfigurationFitter
from .utils.custom_json_encoder import PintCpxEnco
from .utils.input_data_manager import InputDataManager
from .utils.optisim_logger import setup_logger
from .utils.rabbitmq.rabbit_sim_consumer import RabbitSimulationConsumer
from .utils.rabbitmq.thread_rabbit_producer import thread_rabbit_producer

_dateformat = '%Y-%m-%d %H:%M%z'
_generator_namer = lambda mtr, term: mtr + '_' + str(term)


def process_internal_controller(meter_idx, meter_name, components_profiles, force_off_profiles, q_activation, q_force_off, sim_config, logging_conf):
    logging_conf['name'] = logging_conf['name'] + '.' + meter_name
    logger = setup_logger(**logging_conf)

    while True:
        res = q_activation.get()
        if isinstance(res, str) and res == 'kill':
            logger.info('killed internal controller process {}'.format(meter_idx))
            break

        logger.debug('Internal controller for meter %s has been activated, sim_time -> %i' % (meter_name, res['sim_time']))

        df_meter_force_offs = pd.DataFrame(np.zeros(res['num_steps']), index=res['idx'].astype(np.int64), columns=['force_off'])
        df_meter_force_offs.index.name = 'time'

        # Check if controller profiles have to be used
        if sim_config['internalController']['profilesUsage'] is True:
            for k_comp in components_profiles:

                if components_profiles[k_comp]['type'] in sim_config['internalController']['componentsProfiles'].keys():

                    # Set the dataframe for the specific component
                    # todo this part should be changed to implement the multi-step feature
                    cmd = force_off_profiles[components_profiles[k_comp]['type']][int(res['sim_time'])]
                    df_comp = pd.DataFrame(np.ones(res['num_steps']) * cmd, index=res['idx'].astype(np.int64),
                                           columns=[k_comp])

                    # Concatenate the component dataframe with the meter one
                    df_meter_force_offs = pd.concat([df_meter_force_offs, df_comp], axis=1)

        q_force_off.put(df_meter_force_offs)


class MeterManager:

    def __init__(self,
                 config_file,
                 log_file,
                 krg,
                 concurrent_processes=2,
                 logging_level=logging.WARNING):

        # loading configuration
        cf = ConfigurationFitter(config_file=config_file)
        cf.set_fitted_config()
        self.config = cf.fitted_config
        self.cfile = config_file

        self.logging_conf = dict(name='meter_manager',
                                 entry_format='%(asctime)-15s::%(levelname)s::%(process)d::%(name)s::%(funcName)s::%(message)s',
                                 level=logging_level,
                                 filename=log_file)

        self.logger = setup_logger(**self.logging_conf)
        self.use_algo = self.config['algoVersions']['use_algo']
        self.meters = self.config['meters']
        self.first_gone = False
        self.mtr_2_gen = {}
        self.latest_results = None
        self.current_index = 0
        self.buffer_steps = 0
        self.load_states_from_db = self.config['simulation']['load_states_from_db'] if 'load_states_from_db' in self.config['simulation'] else False
        self.load_states_from_db_measurement = self.config['simulation']['load_states_from_db_measurement'] if 'load_states_from_db_measurement' in self.config['simulation'] else self.config['simulation']['ID']

        if krg is not None:
            self.krg_ref = weakref.proxy(krg)
        else:
            self.krg_ref = None

        # todo this is just to match fus signature inside evalsolve
        self.name = 'meter_manager'

        # extracting generic parameters
        self.start_time = datetime.strptime(self.config['simulation']['startDateTime'] +'+0000', _dateformat)
        self.end_time = datetime.strptime(self.config['simulation']['endDateTime'] +'+0000', _dateformat)
        self.sim_hours = ((self.end_time - self.start_time).total_seconds() * kp.UM.s).to('hour')
        self.start_sim = int(self.start_time.timestamp())
        self.end_sim = int(self.end_time.timestamp())
        self.sim_time = self.start_sim
        self.sim_step = self.config['simulation']['step']

        self.steps_per_day = int(86400 / self.config['simulation']['step'])

        # avoid creating useless threads
        concurrent_processes = min(concurrent_processes, len(self.meters))

        # manager for getting queues (conserved as attribute for debug)
        self._manager = Manager()

        # queue for producer
        self.q_prod = self._manager.Queue(1)
        self.w_prod = Process(target=thread_rabbit_producer,
                              args=[self.q_prod, self.config, self.start_time, self.logging_conf])
        self.w_prod.daemon = True
        self.w_prod.start()
        # sleep(3)

        # force off queues, one for each meter
        self.q_force_off = {}
        for m in self.meters:
            self.q_force_off[m['name']] = self._manager.Queue()

        # meters names must be unique
        mns = [m['name'] for m in self.meters]
        if not len(mns) == len(set(mns)):
            raise ValueError('The meter names are not unique!')

        self.q_out = self._manager.Queue()

        self.semaphore = Semaphore(concurrent_processes)

        # Meter process spawning and starting
        self.logger.info('Spawning the meters...')
        self.w_meters = []

        sim_hours = ((self.end_time - self.start_time).total_seconds() * kp.UM.s).to('hour')

        self.data_manager = InputDataManager(master_descriptor=self.config, logger=self.logger,
                                              sim_steps=dict(start=0, end=math.ceil(sim_hours.magnitude) * 3600))
        meteo_dataset = self.data_manager.create_meteo_dataset()

        # Load force off profiles from database if necessary
        if 'internalController' in self.config['simulation'].keys() and \
                self.config['simulation']['internalController']['usage'] is True:
            self.set_internal_controllers()

        # Define the steps to simulate
        # todo currently it works only with numSteps = 1
        if 'numSteps' in self.config['simulation'].keys():
            self.num_steps = self.config['simulation']['numSteps']
        else:
            self.num_steps = 1

        # Attach to krang
        self._attach_to_krang(krg)
        if krg is not None:
            krg.snap()

        tot_n_meters = len(self.meters)
        for ix, meter in enumerate(self.meters):
            self.w_meters.append(Process(target=meter_process,
                                         args=[ix, meter['name'], meteo_dataset,
                                               self.semaphore,
                                               self.q_force_off[meter['name']],
                                               self.q_out,
                                               self.q_prod,
                                               self.config,
                                               self.logging_conf]))
            self.logger.info('Process for meter {} (#{}/{}) created'.format(meter['name'], str(ix+1), str(tot_n_meters)))

        self.logger.info('Starting the meters...')
        for w_meter in self.w_meters:
            w_meter.daemon = True
            w_meter.start()
        self.logger.info('Meters up and running...')

        # initialization acknowledgement on the q_outs
        for ix in range(len(self.meters)):
            res = self.q_out.get()
            if res['msg'] != 'init ok':
                raise ValueError('meter %i did not initialize correctly' % res['ix'])
            else:
                self.logger.info('ix:{} meter:{} initialized ok'.format(res['ix'], res['name']))
        self.logger.info('initialized acknowledgement on the q_outs...')

        # extraction of the list of quantities to retrieve
        self.krangresults = self.config['krang']['results']
        self.functions_file = self.config['krang']['results_functions_file']
        rf_modulespec = spec_from_file_location('results_functions', self.functions_file)
        self.rf_module = module_from_spec(rf_modulespec)
        rf_modulespec.loader.exec_module(self.rf_module)

        self.qtyes = list()
        self.qtyes_indicization = {}

        for i in range(0, len(self.krangresults)):

            # PULLS OUT ALL THE FUNCTIONS REQUIRED IN KRANGRESULT

            for q in self.krangresults[i]['selection']:
                q_function = getattr(self.rf_module, q)
                try:
                    assert hasattr(q_function, 'indicization')
                except AssertionError:
                    raise AttributeError('Every result-evaluation function has to have an attribute "indicization", '
                                         'a list of tag_names that reflects the dictionary structure of the result.')
                # here we also pull the indicization property, that is required to be explicitly set as an attribute
                # of the function
                self.qtyes_indicization[q] = q_function.indicization

                # if the original function is to be partialized with fixed data computable by a function, we do it
                if hasattr(q_function, 'partialization'):

                    fixed_args = {arg_name: arg_function(self)  # partialization function has to take mm as parameter
                                  for arg_name, arg_function in q_function.partialization.items()}
                    part_q_function = partial(q_function, **fixed_args)
                    part_q_function.__name__ = q_function.__name__
                    self.qtyes.append(part_q_function)
                else:
                    self.qtyes.append(q_function)

            # COMPLETES KRANGRESULTS WITH INFO COMING FROM THE CONNECTION SECTION, USEFUL FOR ROUTING

            self.krangresults[i]['topic'] = \
                next((l for l in self.config['input_output'] if l['name'] == self.krangresults[i]['output_name']), None)['topic']
            self.krangresults[i]['exchange'] = \
                next((l for l in self.config['input_output'] if l['name'] == self.krangresults[i]['output_name']), None)['exchange']
            # if self.krangresults[i]['exchange'] == 'simulation_exchange':  # generic name
            #     self.krangresults[i]['exchange'] = self.config['simulation']['ID']
            self.krangresults[i]['measurement'] = \
                next((l for l in self.config['input_output'] if l['name'] == self.krangresults[i]['output_name']), None)['measurement']

        self.qtyes = list(set(self.qtyes))  # ensures that values are unique

        self.logger.info('Starting consumer...')
        self._init_consumer()
        self.logger.info('Consumer started')

        if self.config['simulation'].get('check_output_data_manager', True):
            self.logger.info('Checking output data manager...')
            self.check_output_data_manager()
        else:
            self.logger.info('Skipping output data manager check')

        self.logger.info('MeterManager initialized')

        if self.use_algo:
            self.check_sim_algo()
        self.logger.info('MeterManager attached')

    def _init_consumer(self):
        self.algo_decisions_topic = next((l for l in self.config['input_output'] if l['name'] == 'algo_decisions'), None)['topic']
        self.odm_ping_topic = next((l for l in self.config['input_output'] if l['type'] == 'odmping'), None)['topic']
        self.odm_pong_topic = next((l for l in self.config['input_output'] if l['type'] == 'odmpong'), None)['topic']
        self.algo_decisions_topic = next((l for l in self.config['input_output'] if l['name'] == 'algo_decisions'), None)['topic']
        self.algo_2_topic = next((l for l in self.config['input_output'] if l['name'] == '2_algo'), None)['topic']
        self.output_data_manager_topic = next((l for l in self.config['input_output'] if l['type'] == 'external_to_db'), None)[
                                                    'topic']

        if 'internalController' not in self.config['simulation'].keys() or \
                self.config['simulation']['internalController']['usage'] is False:
            self.algo_consumer = RabbitSimulationConsumer(exchange=self.config['simulation']['internalID'] if 'internalID' in self.config['simulation'].keys() else self.config['simulation']['ID'],
                                                          topic=self.algo_decisions_topic,
                                                          auto_delete=True,
                                                          rabbit_pars=self.config['rabbit'],
                                                          logging_conf=self.logging_conf)

            self.pong_consumer = RabbitSimulationConsumer(exchange='output_exchange',
                                                          topic=self.odm_pong_topic,
                                                          rabbit_pars=self.config['rabbit'],
                                                          logging_conf=self.logging_conf)

    def _attach_to_krang(self, krg):

        meters_lists = {}
        for meter in self.meters:
            mname = meter['name']
            meters_lists[mname] = []
            mbus = meter['bus']

            true_phases = meter['krang_pars']['phases']
            meter['krang_pars']['phases'] = 1

            gen = kp.Generator(**meter['krang_pars'])
            sk = meter['simulation_kwargs']

            self.mtr_2_gen[mname] = {}
            for term in range(1, 4):
                # define the meter decision models
                genname = _generator_namer(mname, term)
                try:
                    krg[mbus + '.' + str(term), ] << gen.aka(genname)
                except Exception:
                    pass

                # memorization of the generators by meter's name and terminal,
                # in order to be able to correctly assign the powers decided by algo
                # at update time
                self.mtr_2_gen[mname][term] = genname

        # todo find an alternative
        if krg is not None:
            krg._ai_list.append(self)

    def sim_meters_only(self):
        n_steps = min(1440, int((self.end_sim - self.sim_time) / self.sim_step))
        self.test_fill(n_steps)
        self.latest_results = self._reap_meters()
        lengths = np.asarray([x.shape[0] for x in self.latest_results.values()])
        if not np.all(lengths == lengths[0]):
            # all instruction dataframes must be of equal length
            raise ValueError('The results coming from the Meters did not all have the same number of steps')
        self.buffer_steps = lengths[0]
        self.current_index = 0

    def activate_controllers(self):
        # Define the dataframe index related to the specific point
        # todo this part has to be changed to implement the multi-step feature
        idx = (self.sim_time + self.sim_step * np.arange(self.num_steps)) * 1e3

        # Cycle over the meters queues
        for key_meter in self.q_force_off.keys():
            # Activate the internal controller of the meter
            self.q_controller_activation[key_meter].put({'inputs': None, 'sim_time': int(self.sim_time),
                                                         'idx': idx, 'num_steps': self.num_steps})

        # Update the simulation time
        self.sim_time += self.sim_step * self.num_steps

    def run_step_internal_controller(self):

        # Get the latest results
        self.latest_results = self._reap_meters()

        # todo here the part related to the Krangpower simulation step should be implemented

        if len(self.latest_results) == len(self.config['meters']):
            # Send commands
            self.activate_controllers()

            # Code taken from fus() method
            lengths = np.asarray([x.shape[0] for x in self.latest_results.values()])
            if not np.all(lengths == lengths[0]):
                # all instruction dataframes must be of equal length
                raise ValueError('The results coming from the Meters did not all have the same number of steps')
            self.buffer_steps = lengths[0]
            self.current_index = 1

            # The following part (implemented in fus() seems to be useless)
            # self.current_index = 0
            #
            # update_string = ''
            #
            # for metername, df_instr in self.latest_results.items():
            #     for phase, genname in self.mtr_2_gen[metername].items():
            #         p = df_instr['P' + str(phase)].iloc[self.current_index]
            #         q = df_instr['Q' + str(phase)].iloc[self.current_index]
            #
            #         update_string += 'edit generator.' + genname + ' kw=' + str(-p) + ' kvar=' + str(-q) + '\n'
            # self.current_index += 1

        return self.sim_time

    def fus(self, krg, name):

        # we request new results from the algo only when the last batch is over
        if self.current_index >= self.buffer_steps - 1:

            if self.first_gone and self.use_algo:  # after the first, decision-less step is gone:
                self.logger.info('awaiting instructions')
                # import time
                idsia_instructions = self.algo_consumer.listen()  # we listen for messages from the algo
                self.logger.info('received instructions')
                # self.logger.info('received instructions: ' + json.dumps(idsia_instructions))
                if idsia_instructions['case'] == 'decisions':  # we ensure that the case is the one regarding decision
                    for meter_name, instr_df_json in idsia_instructions['decisions_content'].items():
                        instr_df = pd.DataFrame(json.loads(instr_df_json), dtype=np.float)
                        instr_df.index = instr_df.index.map(int)
                        self.q_force_off[meter_name].put(instr_df)

                        # print(pandas.DataFrame.from_dict(instr_df_json))
                        # for m in self.meters_lists[meter]:
                        # m.update_instructions(instr_df_json)  # meter by meter, we broadcast the instructions received
            elif self.use_algo and self.load_states_from_db:
                self.load_force_offs()
                self.first_gone = True
            else:
                # if the first is not gone, we perform a test fill in order to be able to feed the first informations
                # to the algos
                self.test_fill()
                self.first_gone = True

            self.latest_results = self._reap_meters()

            lengths = np.asarray([x.shape[0] for x in self.latest_results.values()])
            if not np.all(lengths == lengths[0]):
                # all instruction dataframes must be of equal length
                raise ValueError('The results coming from the Meters did not all have the same number of steps')
            self.buffer_steps = lengths[0]
            self.current_index = 0

        update_string = ''

        for metername, df_instr in self.latest_results.items():
            for phase, genname in self.mtr_2_gen[metername].items():

                p = df_instr['P' + str(phase)].iloc[self.current_index]
                q = df_instr['Q' + str(phase)].iloc[self.current_index]

                update_string += 'edit generator.' + genname + ' kw=' + str(-p) + ' kvar=' + str(-q) + '\n'

        self.current_index += 1
        return update_string

    def send_info_to_message_broker(self, results, krg: kp.Krang):
        """sends the infos to algos"""

        results = json.loads(json.dumps(results, cls=PintCpxEnco))

        dblhour_af = krg.brain.Solution.DblHour().magnitude
        dblhour_bf = krg.brain.Solution.DblHour().magnitude - krg.get('stepsize')['stepsize'].to('hour').magnitude

        for er in self.krangresults:

            res = {}
            res.update({'indicization_dict': {k: v for k, v in self.qtyes_indicization.items() if k in er['selection']}})
            res.update({'measurement': er['measurement']})
            res.update({'payload': {}})

            # todo eliminate the need for [-1]
            # this -1 is because these are results from evalsolve, so they are lists (because evalsolve gives you lists
            # of len==n, where n is the number of points solved)
            # in general, when using optisim you are going to want a step-by-step solve, but it'd be nice to
            # the general case where you are taking meter decisions every n steps solved by krang
            res['payload'].update({k: v[-1] for k, v in results.items() if k in er['selection']})
            content = {'function': 'send', 'args': [res, 'simulation', dblhour_af if er['send_time'] == 'after' else dblhour_bf]}
            self.q_prod.put((er['exchange'], er['topic'], content))
            self.logger.info('sent message to broker %s:%s' % (er['exchange'], er['topic']))
            # self.logger.info('sent message to broker %s:%s, message: %s' % (er['exchange'], er['topic'], content))

        # flush the messages related to the current step
        # self.q_prod.put({'function': 'flush'})

    def send_krang_surrogate(self, t, step_size):
        """sends the infos to algos"""
        dblhour_af = (t + step_size) / 3600
        dblhour_bf = t / 3600

        for er in self.krangresults:

            res = {}
            res.update({'indicization_dict': {k: v for k, v in self.qtyes_indicization.items() if k in er['selection']}})
            res.update({'measurement': er['measurement']})
            res.update({'payload': {}})

            # todo eliminate the need for [-1]
            # this -1 is because these are results from evalsolve, so they are lists (because evalsolve gives you lists
            # of len==n, where n is the number of points solved)
            # in general, when using optisim you are going to want a step-by-step solve, but it'd be nice to
            # the general case where you are taking meter decisions every n steps solved by krang
            # res['payload'].update({k: v[-1] for k, v in {} if k in er['selection']})
            content = {'function': 'send', 'args': [res, 'simulation', dblhour_af if er['send_time'] == 'after' else dblhour_bf]}
            self.q_prod.put((er['exchange'], er['topic'], content))
            self.logger.info('sent message to broker %s:%s' % (er['exchange'], er['topic']))
            # self.logger.info('sent message to broker %s:%s, message: %s' % (er['exchange'], er['topic'], content))

        # flush the messages related to the current step
        # self.q_prod.put({'function': 'flush'})

    def check_output_data_manager(self, num_checks=100):

        # send the ping command to output_data_manager
        i = 0
        fake_dblhour = 0.0
        while True:
            i += 1
            self.logger.info('checking on output_data_manager %d/%d' % (i, num_checks))
            self.q_prod.put(('output_exchange',
                             self.odm_ping_topic,
                             {'function': 'send',
                              'args': [{'type': 'ping'},
                                        'ctrl',
                                        fake_dblhour,
                                        True
                                       ]
                              }))

            pong_data = self.pong_consumer.listen(timeout=2.0)  # wait for acknowledgement
            if pong_data == 'TIMEOUT':
                self.logger.warning('problems with algo ack: TIMEOUT')
            elif pong_data['payload_dict']['msg'] == 'ok':
                break
            else:
                self.logger.warning('problems with algo ack: %s' % pong_data)
            if i == num_checks:
                raise ValueError('output_data_manager is not responding')

        self.logger.info('Output data manager was checked and answered correctly')

    def check_sim_algo(self, num_checks=100):
        # circuit send and acknowledge
        if hasattr(self.krg_ref, 'brain'):
            dblhour = self.krg_ref.brain.Solution.DblHour().magnitude
        else:
            dblhour = 0
            time.sleep(5)
        i = 0
        while True:
            i += 1
            self.logger.info('checking on algo %d/%d' % (i, num_checks))
            if hasattr(self.krg_ref, 'brain'):
                self.q_prod.put(
                    (self.config['simulation']['internalID'] if 'internalID' in self.config['simulation'].keys() else self.config['simulation']['ID'],
                     self.algo_2_topic,
                     {'function': 'send',
                      'args':
                          [{"circuit": self.krg_ref.make_json_dict(),
                            "meter_2_traf": None},
                            'circuit_acknowledge',
                            dblhour,
                            True
                           ]
                      }))
            else:
                self.q_prod.put(
                    (self.config['simulation']['internalID'] if 'internalID' in self.config['simulation'].keys() else
                     self.config['simulation']['ID'],
                     self.algo_2_topic,
                     {'function': 'send',
                      'args':
                          [{"circuit": None,
                            "meter_2_traf": None},
                           'circuit_acknowledge',
                           dblhour,
                           True
                           ]
                      }))

            ack = self.algo_consumer.listen()  # wait for acknowledgement
            if ack == 'TIMEOUT':
                self.logger.warning('problems with algo ack: TIMEOUT')
            elif ack['msg'] == 'ok':
                self.logger.info('sim_algo is up and running!')
                break
            else:
                self.logger.warning('problems with algo ack: %s' % ack)
            if i == num_checks:
                raise ValueError('algo is not responding')

    def _reap_meters(self):
        results = {}
        for n in [m['name'] for m in self.meters]:
            results[n] = []

        # there is only one q_out for all the meters. So n messages have to arrive on this queue.
        for _i, _n in enumerate([m['name'] for m in self.meters]):
            msg = self.q_out.get(block=True)

            if len(results[msg['name']]) == 0:
                results[msg['name']] = msg['res']
            else:
                results[msg['name']] = results[msg['name']].append(msg['res'])

        return results

    def send_kill(self):

        # todo this is a tacconata
        # we are relying on krangresults to exist. The clean thing to do would be to create a new type of message
        # to be sent to the algos, that explicitly represents the "passage" (instead of relying on the fact that the
        # "simulation" message is the last, as we are doing now); when we want to kill, this type of message
        # will be configured to contain a "kill" information

        hr = self.krangresults[1]  # we piggyback the results, so that the interface works
        kill_msg = {'function': 'send', 'args': [{}, 'kill', None]}
        self.q_prod.put((hr['exchange'], hr['topic'], kill_msg))

    def kill(self):
        for meter in self.meters:
            self.q_force_off[meter['name']].put('kill')
            self.q_controller_activation[meter['name']].put('kill')
        # for w_meter in self.w_meters:
        #     self.logger.info('killing meter {}'.format(w_meter.name))
        #     w_meter.terminate()

        self.logger.info('killing w_prod')
        kill_msg = {'function': 'kill'}
        self.q_prod.put((None, None, kill_msg))
        # self.w_prod.terminate()
        if hasattr(self, 'algo_consumer'):
            self.logger.info('killing algo consumer')
            self.algo_consumer.exit()

    def test_fill(self, num_steps=1):
        """This function executes a test filling of the listening queues of the meters, in order to be able
        to do a test-reap"""
        df = pd.DataFrame(np.zeros(num_steps),
                          index=((self.sim_time+self.sim_step*np.arange(num_steps)) * 1000.0).astype(np.int64),
                          columns=['force_off'])
        df.index.name = 'time'
        # df.index = df.index.map(int)
        for q in self.q_force_off.values():
            q.put(df)
        self.sim_time += self.sim_step * num_steps

    def load_force_offs(self, num_steps=1):
        """This function executes a test filling of the listening queues of the meters, in order to be able
        to do a test-reap"""
        meters_with_force_off = list()
        for k, q in self.q_force_off.items():
            # print('loading force_off for meter {}'.format(k))
            states_measurement_base = next((l for l in self.data_manager.get_master_descriptor()['input_output'] if
                  l['name'] == 'states_2_db'),
                 None)['measurement_base']
            force_offs = self.data_manager.get_force_offs('{}_{}'.format(states_measurement_base,
                                                                         self.load_states_from_db_measurement), k,
                                                          t_start=0,
                                                          t_end=0)

            if len(force_offs) == 0:
                force_offs = pd.DataFrame(np.zeros(num_steps),
                             index=((self.sim_time + self.sim_step * np.arange(num_steps)) * 1000.0).astype(np.int64),
                             columns=['force_off'])
            q.put(force_offs)


        # df = pd.DataFrame(np.zeros(num_steps),
        #                   index=((self.sim_time+self.sim_step*np.arange(num_steps)) * 1000.0).astype(np.int64),
        #                   columns=['force_off'])
        # df.index.name = 'time'
        # # df.index = df.index.map(int)
        # for q in self.q_force_off.values():
        #     q.put(df)
        # self.sim_time += self.sim_step * num_steps

    def set_internal_controllers(self):
        """
        Load force_off profiles from DB according to the configuration files. This function is used when a control is
        performed by an internal controller based on static profiles.
        :return:
        """
        # Get the profiles to load for the different components
        self.profiles_names = dict()
        for meter in self.config['meters']:
            self.profiles_names[meter['name']] = dict()
            for component in meter['components']:
                comp_name, profile_name, comp_type = self.get_profile_name(component, meter['name'])

                if profile_name is not None:
                    self.profiles_names[meter['name']][comp_name] = {'type': comp_type, 'profile': profile_name}

        self.q_controller_activation = dict()
        self.proc_internal_controllers = dict()

        if self.config['simulation']['internalController']['profilesUsage'] is True:

            force_off_profiles = dict()
            #download profiles
            profile_names = np.unique(
                np.concatenate([list(dev_type.values()) if type(dev_type) is dict else [dev_type] for dev_type in
                                self.config['simulation']['internalController'][
                                    'componentsProfiles'].values()]).ravel())
            for profile_name in profile_names:
                data, query = self.data_manager.download_data(
                    measurement=self.data_manager.master_descriptor['influxdb']['inputMeasurement'],
                    tags={'type': 'ForceOff', 'name': profile_name},
                    fields=['ForceOff'],
                    t_start=self.data_manager.sim_steps['start'],
                    t_end=self.data_manager.sim_steps['end'] + 86400 - 1,
                    interval='%ss' % self.data_manager.master_descriptor['simulation']['step'],
                    aggr_fun='max')
                data = data.fillna(0)
                data = data.round(1)
                force_off_profiles[profile_name] = dict()
                for idx in data.index:
                    force_off_profiles[profile_name][idx] = int(data['ForceOff'][idx])

        idx = 0
        for meter in self.config['meters']:
            k_meter = meter['name']
            devices = [device for device in self.profiles_names[k_meter].values()]
            meter_force_off_profiles = {device['type']: force_off_profiles[device['profile']] for device in devices}
            self.q_controller_activation[k_meter] = self._manager.Queue()
            self.proc_internal_controllers[k_meter] = Process(target=process_internal_controller,
                                                              args=[idx, k_meter, self.profiles_names[k_meter],
                                                                    meter_force_off_profiles,
                                                                    self.q_controller_activation[k_meter],
                                                                    self.q_force_off[k_meter],
                                                                    self.config['simulation'], self.logging_conf])

            idx += 1

        self.logger.info('Starting the internal controllers...')
        for k_proc in self.proc_internal_controllers.keys():
            self.proc_internal_controllers[k_proc].daemon = True
            self.proc_internal_controllers[k_proc].start()
        self.logger.info('Controllers up and running...')

    def get_profile_name(self, data, meter_name):
        if data['type'] in self.config['simulation']['internalController']['componentsProfiles'].keys():
            if type(self.config['simulation']['internalController']['componentsProfiles'][data['type']]) is dict:
                profile_key = meter_name if meter_name in self.config['simulation']['internalController']['componentsProfiles'][data['type']] else 'default'
                return data['name'], self.config['simulation']['internalController']['componentsProfiles'][data['type']][profile_key], data['type']
            else:
                return data['name'], self.config['simulation']['internalController']['componentsProfiles'][data['type']], data['type']
        else:
            if 'heating_element' in data:
                comp_name, profile_name, comp_type = self.get_profile_name(data['heating_element'], meter_name)
                return comp_name, profile_name, comp_type
            else:
                return None, None, None
