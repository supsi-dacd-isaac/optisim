import argparse
import logging
import multiprocessing as mp
import signal
import sys
from datetime import datetime, timedelta
from time import time, sleep

import numpy as np

import krangpower as kp
from optisim import MeterManager
from optisim.utils import ConfigurationFitter, setup_logger, get_logger_level
from optisim.utils.rabbitmq.rabbit_breeder import RabbitsBreeder

np.random.seed(0)


# --------------------------------------------------------------------------- #
# Functions
# --------------------------------------------------------------------------- #
def signal_handler(sig_num, frame):
    logger.warning('Received signal ', str(signal.Signals(sig_num).name))

    # # Delete all the queues used in the simulation
    breeder = RabbitsBreeder(cfg=config['rabbit'], logger=logger)
    breeder.purge_queues(queues=np.unique(topics))
    sys.exit(sig_num)


# --------------------------------------------------------------------------- #
# Main
# --------------------------------------------------------------------------- #
if __name__ == "__main__":

    kp.set_log_level(30)
    um = kp.UM

    # --------------------------------------------------------------------------- #
    # Arguments
    # --------------------------------------------------------------------------- #

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c', help='configuration file')
    arg_parser.add_argument('-l', help='log file')
    args = arg_parser.parse_args()
    cf = ConfigurationFitter(config_file=args.c)
    cf.set_fitted_config()
    config = cf.fitted_config
    step_size = config['simulation']['step'] * um.s

    if not args.l:
        log_file = None
    else:
        log_file = args.l

    str_formatter = '%(asctime)-15s::%(levelname)s::%(process)d::%(name)s::%(funcName)s::%(message)s'
    logger_level = get_logger_level(level_desc=config['simulation']['log']['level'])
    logger = setup_logger(name='sim_grid', filename=log_file, level=logger_level, entry_format=str_formatter)

    logger.info('Starting simulation of ' + args.c)
    broker_logger = logging.getLogger('broker')
    handler = logging.StreamHandler()
    formatter = logging.Formatter(str_formatter)
    handler.setFormatter(formatter)
    broker_logger.addHandler(handler)
    broker_logger.setLevel(logging.INFO)

    if log_file is not None:
        file_handler = logging.FileHandler(filename=log_file)
        file_handler.setFormatter(formatter)
        broker_logger.addHandler(file_handler)

    # Define the signals handler
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    # Times and durations
    dateformat = '%Y-%m-%d %H:%M'
    inidate = config['simulation']['startDateTime']
    start_time = datetime.strptime(config['simulation']['startDateTime'], dateformat)
    end_time = datetime.strptime(config['simulation']['endDateTime'], dateformat)
    sim_seconds = (end_time - start_time).total_seconds()

    topics = []
    for io in config['input_output']:
        if io['topic'] is not None:
            topics.append(io['topic'])

    # Check if Krangpower must be used
    if 'krangUsage' in config['simulation'].keys() and config['simulation']['krangUsage'] is False:
        # Power flow simulation is not needed in the simulation -> Krangpower is not necessary
        logger.info('Power flow simulation will not be performed')
        circuit = None
    else:
        # Power flow simulation is needed in the simulation -> Krangpower is necessary
        # Instantiate the Krangpower circuit
        logger.info('Power flow simulation will be performed')
        circuit = kp.Krang.from_json('%s/%s' % (config['krang']['dss_files_folder'],
                                                config['krang']['circuit_file']),
                                     override_log=config['krang']['override_log'] if 'override_log' in config['krang'].keys() else False)

        # Set the main circuit parameters
        circuit.set(stepsize=step_size.to('s').magnitude, number=1)
        circuit.set(time=[0.0, 0.0])

    # --------------------------------------------------------------------------- #
    # Instantiate the meter manager
    # --------------------------------------------------------------------------- #
    num_cpu = mp.cpu_count()
    logger.info('Starting meter manager with {} cores'.format(num_cpu))
    gg = time()
    mm_log_level = config['simulation']['log']['meter_manager_level'] if 'meter_manager_level' in config['simulation']['log'] else logging.WARNING
    mm = MeterManager(args.c, log_file, circuit, num_cpu, mm_log_level)
    logger.info('loading time: %ds' % (time()-gg))

    dt_tot = np.full(288*7, np.nan)
    dt_init = time()

    step_seconds = config['simulation']['step']
    curr_sim_seconds = 0

    if 'internalController' in config['simulation'].keys() and \
            config['simulation']['internalController']['usage'] is True:
        # Activate the controller for the first step
        mm.activate_controllers()

    try:
        # Launch the simulation iterations
        while curr_sim_seconds < sim_seconds:
            t1 = time()

            if circuit is not None:
                # Launch the simulation step (models/components and Krang power flow) and send models data on the queues
                rdck = circuit.evalsolve(*mm.qtyes, as_df=False)
                # Send the Krang results to the broker
                mm.send_info_to_message_broker(rdck, circuit)
            else:
                # Launch the simulation step for models/components data and send them on the queues
                if 'internalController' in config['simulation'].keys() and \
                        config['simulation']['internalController']['usage'] is True:
                    # Usage of a controller internal to sim_grid
                    mm.run_step_internal_controller()
                else:
                    # Usage of a controller external to sim_grid, based on Rabbit broker
                    mm.fus(None, None)
                    mm.send_krang_surrogate(curr_sim_seconds, step_seconds)

            dt = time() - t1
            dt_from_init = time() - dt_init

            dt_tot = np.roll(dt_tot, 1)
            dt_tot[0] = dt

            logger.info('Step: %s, Simulated %.1f hours (%.1f days) in %.1f minutes' %
                        (
                            start_time + timedelta(seconds=curr_sim_seconds),
                            curr_sim_seconds / 3600,
                            curr_sim_seconds / 86400, dt_from_init / 60
                        ))

            average_step_time = np.nanmean(dt_tot)
            remaining_steps = (sim_seconds - curr_sim_seconds) / step_seconds
            remaining_time = timedelta(seconds=remaining_steps * average_step_time)
            logger.info('Loop time: %.0fms (average %.0fms), remaining time: %s, expected finish time: %s' % (
                dt * 1000, average_step_time * 1000, str(remaining_time), str(datetime.now() + remaining_time)))

            curr_sim_seconds += step_seconds
    except Exception as e:
        logger.error('Simulation failed with error: {}'.format(e))

    # just to be on the safe side
    try:
        sleep(5)
        mm.send_kill()
    except Exception as e:
        logger.error('send kill failed with error: {}'.format(e))

    # prevents suicide before carrying out the send_kill
    try:
        sleep(10)
        mm.kill()
    except Exception as e:
        logger.error('kill failed with error: {}'.format(e))

    breeder = RabbitsBreeder(cfg=config['rabbit'], logger=logger)
    breeder.purge_queues(queues=np.unique(topics))

    logger.info('Simulation ended')

